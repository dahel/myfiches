module.exports = function(grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        relSrc: 'build/release',
        devSrc: 'build/dev',

        uglify: {
            target: {
                options: {
                    mangle: true
                },
                files: [{
                    expand: true,
                    cwd: 'build/release/',
                    src: ['**/*.js'],
                    dest: 'build/release/'
                }]
            }
        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1
            },
            target: {
                files: {
                    '<%= dest %>/css/all.css': ['.tmp/css/all.css']
                }
            }
        },

        copy: {
            fonts: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['fonts/bench/**'],
                    dest: '<%= dest %>/'
                }]
            },

            js: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/',
                        src: [
                            'app/collections/*.js',
                            'app/models/*.js',
                            'app/views/**/*.js',
                            'app/Page.js',
                            'app/Pane.js',
                            'lang/**',
                            'utils/**'
                        ],
                        dest: '<%= dest %>/',
                        include: ['libs/require.js']
                    }
                ]
            },

            css: {
                files: [
                    {
                        expand: false,
                        src: ['.tmp/css/all.css'],
                        dest: '<%= dest %>/css/all.css'
                    },
                    {
                        expand: false,
                        src: ['css/ie_below_9.css'],
                        dest: '<%= dest %>/css/ie_below_9.css'
                    }
                ]
            },

            templates: {
                files: [{
                    expand: true,
                    cwd: 'src/templates',
                    src: ['**/*.html'],
                    dest: '<%= dest %>/templates/'
                }]
            },

            index: {
                  files: [{
                      expand: true,
                      cwd: 'src/',
                      src: ['index-rel.html'],
                      dest: '<%= dest %>/',
                      rename: function (dest, src) {
                          if (src.indexOf('index-rel.html') > -1 ) {
                              return dest + src.replace('index-rel.html', 'index.html');
                          }
                          return dest + src;
                      }
                  }]
            },

            sprites: {
                files: [{
                    expand: true,
                    cwd: '.tmp/sprites',
                    src: ['*.png'],
                    dest: '<%= dest %>/images'
//                    dest: 'build/develop/images'
                }]
            },

            logoImage: {
                expand: true,
                cwd: 'src/images/no-sprites',
                src: ['*.png'],
                dest: '<%= dest %>//images'
//                dest: '<%= dest %>/images',
            }

        },

        clean: {
            before: ['<%= dest %>/'],
            after: ['<%= tmp %>']
        },

        requirejs: {
            develop: {
                options: {
                    findNestedDependencies: true,
                    removeCombined: true,
                    optimize : 'none',
                    baseUrl: "src",
                    name: 'main',
                    mainConfigFile: "src/main.js",
                    out: "<%= dest %>/main.js",
                    include: ["libs/require.js", 'main.js'],

                    paths: {
                        'lang': 'empty:'
                    }
                }
            },

            release: {
                options: {
                    findNestedDependencies: true,
                    removeCombined: true,
                    baseUrl: "src",
                    name: 'main',
                    mainConfigFile: "src/main.js",
                    out: "<%= dest %>/main.js",
                    include: ["libs/require.js"],
                    paths: {
                        'lang': 'empty:'
                    }
                }
            }
        },

        concat: {
            scss: {
                src: 'src/scss/*.scss',
                dest: '<%= tmp %>/scss/all.scss'
            }
        },

        compass: {
            build: {
                options: {
                    basePath: 'src',
                    cssDir: '../<%= tmp %>/css',
                    sassDir: '../<%= tmp %>/scss',
                    imagesDir: 'images',
                    generatedImagesDir: '../<%= tmp %>/sprites/',
                    httpGeneratedImagesPath: "../images",
                    relativeAssets: false,
                    force: false
                }
            }
        },

		'ftp-deploy': {
			build: {
				auth: {
					host: 'ftp.htmlcenter.pl',
					port: 21,
					authKey: 'seba'

				},
				src: 'build/develop/',
				dest: '/public_html/myfiches',
				exclusions: []
			}
		},

        watch: {
            scripts: {
                files: ['src/**/*.js'],
                tasks: ['refreshAppJs'],
                options: {
                    spawn: false,
                    livereload: true
                }
            },

            css: {
                files: ['src/scss/**.scss', 'src/images/**/*.png'],
                tasks: ['recompileScss'],
                options: {
                    livereload: true
                }
            },

            templates: {
                files: 'src/templates/**/*.html',
                tasks: ['refreshTemplates'],
                options: {
                    livereload: true
                }
            },

            index: {
                files: 'src/*.html',
                tasks: ['refreshIndex'],
                options: {
                    livereload: true
                }
            },

			grunt: {
				files: 'Gruntfile.js',
				tasks: ['build:develop:nowatch'],
				options: {
					livereload: true
				}
			}
        }

    });

    // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-ftp-deploy');

    // custom tasks
    grunt.registerTask('generateImageList', function () {
        var done = this.async();
        var fs = require('fs');

        function getFiles(dir,files_){
            files_ = files_ || [];
            if (typeof files_ === 'undefined') files_=[];
            var files = fs.readdirSync(dir);
            for(var i in files){
                if (!files.hasOwnProperty(i)) continue;
                var name = dir+'/'+files[i];
                if (fs.statSync(name).isDirectory()){
                    getFiles(name,files_);
                } else {
                    files_.push(name.replace('.tmp/sprites/', ''));
                }
            }
            return files_;
        }
        var files = getFiles('.tmp/sprites');
        var content = 'var _imgList_ = [\n';

        for(var i = 0, l = files.length; i < l; i++) {
                content += ('\'' + files[i] + '\',\n');
        }
        content = content.substring(0, content.length - 2);
        content += ']';
        fs.writeFile(grunt.config.get('dest') + "/images/_imgList_.js", content, function(err) {
//        fs.writeFile("_imgList_.js", content, function(err) {
            if(err) {
                done();
            } else {
                done();
            }
        });
    });

    grunt.registerTask('build:before:develop', ['prepare:develop', 'clean:before']);
    grunt.registerTask('build:before:release', ['prepare:release', 'clean:before']);
    grunt.registerTask('build:after', ['clean:after']);
    grunt.registerTask('prepare:release', function () {
        grunt.config.set('dest', 'build/release');
        grunt.config.set('tmp', '.tmp');
    });
    grunt.registerTask('prepare:develop', function () {
        grunt.config.set('dest', 'build/develop');
        grunt.config.set('tmp', '.tmp');
    });

    // partiall task for watcher
    grunt.registerTask('recompileScss', ['prepare:develop', 'concat:scss', 'compass', 'copy:css', 'copy:sprites']);
    grunt.registerTask('refreshAppJs', ['prepare:develop', 'requirejs:develop', 'copy:js']);
    grunt.registerTask('refreshTemplates', ['prepare:develop', 'copy:templates']);
    grunt.registerTask('refreshIndex', ['prepare:develop', 'copy:index']);
    //

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
//    grunt.registerTask('build', ['clean', 'uglify, ', 'copy']);
//    grunt.registerTask('build:develop', ['build:before:develop', 'concat:scss', 'compass', 'requirejs:develop', 'copy', 'copy:sprites', 'generateImageList', 'copy:css', 'watch']);
    grunt.registerTask('build:develop', ['build:before:develop', 'concat:scss', 'compass', 'requirejs:develop', 'copy', 'copy:sprites', 'generateImageList', 'copy:css', 'watch']);
    grunt.registerTask('build:develop:nowatch', ['build:before:develop', 'concat:scss', 'compass', 'requirejs:develop', 'copy', 'copy:sprites', 'generateImageList', 'copy:css']);

    grunt.registerTask('build:release', ['build:before:release', 'concat:scss', 'compass', 'requirejs:develop', 'copy', 'copy:sprites', 'generateImageList', 'uglify', 'cssmin'])

//    grunt.registerTask('test', ['build:before', 'concat:scss']);
    grunt.registerTask('test', ['build:before', 'concat:scss', 'compass', 'requirejs:develop', 'copy', 'generateImageList']);




grunt.registerTask('com', ['compass']);

};