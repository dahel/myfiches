<?php
require '../../ff/Slim/Slim.php';
require '../../ff/register/register.php';
require '../../ff/login/login.php';
require '../../ff/login/android_login.php';
require '../../ff/authorize/authorize.php';
require_once '../../ff/dao/dao.php';



\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// USER & SETTINGS ACTIONS  /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$app->post('/register', function() use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	register($input);
});

$app->post('/login', function() use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);

	login($input);
});



//$app->get('/logout', authorize(), function() {
//	logout();
//});

$app->get('/stats', authorize(), function() {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getStats($username);
});

$app->post('/settings', authorize(), function() use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->setSettings($input, $username);
});

$app->get('/settings', authorize(), function() {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getSettings($username);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// COMMON WORDS GROUPS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//utworzenie nowego zestawu
$app->post('/wordsGroups', authorize(), function() use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->createWordsGroup($input, $username);
});


// pobranie info o zestawie
$app->get('/wordsGroups/:id', authorize(), function($id) use ($app) {
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao = Dao::getInstance();
	$dao->getWordsGroup($id, $username);
});

// pobranie zestawów słówek z listy wszystkich
$app->get('/wordsGroups', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getWordsGroups($request, $username);
});

//zaktualizowanie danych o zestawie (aktualnie wyłącznie zmiana nazwy)
$app->put('/wordsGroups/:id', authorize(), function($groupId) use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$dao->updateGroupName($groupId, $input);
});

//zaktualizowanie danych o zestawie (ocena użytkownika)
$app->patch('/wordsGroup/:id', authorize(), function($groupId) use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->addGroupMark($groupId, $input, $username);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// OPERACJE NA SŁÓWKACH /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// dodanie nowego słówka
$app->post('/wordsGroups/:id/words', authorize(), function($groupId) use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->addWord($groupId, $input, $username);
});

//pobranie słówek z zestawu
$app->get('/wordsGroups/:id/words', authorize(), function($id) use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$dao->getGroupWords($id, $request);
});

// edycja słówka
$app->put('/wordsGroups/:groupId/words/:wordId', authorize(), function($groupId, $wordId) use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->editWord($groupId, $wordId, $input, $username);
});

$app->get('/time', function() use ($app) {
	echo time();
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// LEARNING WORDS GROUPS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// rozpoczęcie nauki danego zestawu
$app->post('/learningWordsGroups/:id', authorize(), function($id) use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->startGroupLearning($id, $username);
});


// pobranie zestawów słówek których użytkownik aktualnie się uczy
$app->get('/learningWordsGroups', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getLearningWordsGroups($request, $username);
});

// przerwanie nauki danego zestawu
$app->delete('/learningWordsGroups/:id', authorize(), function($id) use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->stopGroupLearning($id, $username);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// USER WORDS GROUPS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// pobranie zestawów słówek stworzonych przez użytkownika
$app->get('/userWordsGroups', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getUserWordsGroups($request, $username);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// SYNCHRONIZATION /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$app->post('/synchronize/:timestamp', authorize(), function($timestamp) use ($app) {
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getSynchronizationData($username, $timestamp, $input);
});


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////// LEARN POINTS /////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// pobranie ilości punktów nauki
$app->get('/learnPoints', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->getLearnPoints($username);
});

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



// pobranie kategorii
$app->get('/categories', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$dao->getCategories();
});

$app->get('/startBonus', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->checkIfStartBonusUsed($username);
});

$app->post('/startBonus', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$dao->useStartBonus($username);
});

// zgloszenie naruszenia regulaminu przez zestawie
$app->post('/report/:id', authorize(), function($id) use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao->report($input, $id, $username);
});


$app->post('/userDataRecovery', function() use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao->recoverUserData($input);
});

$app->post('/changePassword', function() use ($app) {
	$dao = Dao::getInstance();
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao->changeUserPassword($input);
});

$app->post('/contact', authorize(), function() use ($app) {
	$dao = Dao::getInstance();
	$headers = apache_request_headers();
	$username = $headers['User-Name'];
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body);
	$dao->handleUserContact($username, $input);
});


$app->get('/tmpaction', function() use ($app) {
	$dao = Dao::getInstance();
	$dao->tmpaction();
});



///// old ////////////////////////
///// old ////////////////////////
///// old ////////////////////////

function authorizeUser() {
	return authorize();
}

function addCost($data) {
	$dao = Dao::getInstance();
	$dao->addCost($data);
}

function getCategories() {
	$dao = Dao::getInstance();
	$dao->getCategories();
}

function saveNewCategory($input) {
	$dao = Dao::getInstance();
	$dao->saveNewCategory($input);
}

function updateCategory($input) {
	$dao = Dao::getInstance();
	$dao->updateCategory($input);
}

function deleteCategory($id) {
	$dao = Dao::getInstance();
	$dao->deleteCategory($id);
}

function getCosts($month) {
	$dao = Dao::getInstance();
	$dao->getCosts($month);
}

function deleteCost($id) {
	$dao = Dao::getInstance();
	$dao->deleteCost($id);
}

function updateCost($input) {
	$dao = Dao::getInstance();
	$dao->updateCost($input);
}



$app->post('/login', function() use ($app) {
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    login($input);
});

$app->post('/cost', authorize(), function() use ($app) {
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    addCost($input);
});

$app->delete('/cost/:id', authorize(), function($id){
    deleteCost($id);
});

$app->put('/cost/:id', authorize(), function() use ($app) {
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    updateCost($input);
});

//$app->put('/cost/:id', authorize(), function() use ($app) {
//    $request = $app->request();
//    $body = $request->getBody();
//    $input = json_decode($body);
//    updateCost($input);
//});
//
//$app->delete('/cost/:id', authorize(), function($id) {
//    deleteCost($id);
//});

$app->get('/costs/:month', authorize(), function($month) {
    getCosts($month);
});

$app->post('/category', authorize(), function() use ($app) {
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    saveNewCategory($input);
});

$app->put('/category/:id', authorize(), function() use ($app) {
    $request = $app->request();
    $body = $request->getBody();
    $input = json_decode($body);
    updateCategory($input);
});

$app->delete('/category/:id', authorize(), function($id) {
    deleteCategory($id);
});

$app->get('/categories', authorize(), function() use ($app) {
    getCategories();
});




$app ->run();
?>