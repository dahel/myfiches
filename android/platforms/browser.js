define([
	'config',
	'logger',
	'async',
	'app/views/components/loading'
], function (
	config,
	Logger,
	async,
	Loading
) {

	var logger = Logger.getOne('platform:BROWSER'),
		loadingInited = false,
		notifyInited = false,
		notify,
		loading;

	function errorHandler(error) {
		logger.error("Error: ", error)
	}

	function initNotify() {
		notify = document.createElement('div');
		notify.style.padding = "10px";
		notify.style.background = "black";
		notify.style.border = "solid 1px white";
		notify.style.color = "white";
		notify.style.position = "absolute";
		notify.style.left = "30%";
		notify.style.top = "45%";
		document.body.appendChild(notify);
	}

	function initLoading() {
		loading = document.createElement('div');
		loading.style.width = "100%";
		loading.style.height = "100%";
		loading.style.background = "black";
		loading.style.opacity = "0.7";
		loading.style.textAlign = "center";
		loading.style.display = "none";
		loading.style.position = "absolute";
		loading.style.top = "0";
		loading.style.left = "0";
		loading.style.fontSize = "25px";
		loading.style.lineHeight = "400px";
		loading.style.color = "rgb(24, 232, 0)";
		loading.innerHTML = "Loading";
		document.body.appendChild(loading);
	}

	return {
		getUserData: function (callback) {
			async.parallel({
				username: function (callback) {
					callback(null, localStorage.getItem('username'));
				},
				loggedIn: function (callback) {
					callback(null, localStorage.getItem('loggedIn') === 'true');
				},
				created: function (callback) {
					callback(null, parseInt(localStorage.getItem('created'), 10));
				},
				token: function (callback) {
					callback(null, localStorage.getItem('androidToken'));
				},
				lastSyncTimestamp: function (callback) {
					callback(null, localStorage.getItem('lastSyncTimestamp'));
//					callback(null, 1);
				},
				learningStarted: function (callback) {
					callback(null, parseInt(localStorage.getItem('learningStarted')));
				},
				displayedExpressions: function (callback) {
					callback(null, parseInt(localStorage.getItem('displayedExpressions')) || 0);
				},
				learnPoints: function (callback) {
					callback(null, parseInt(localStorage.getItem('learnPoints')) || 0);
				}
			}, function (error, result) {
				if (error) {
					errorHandler(error);
				} else {
					userData = result;
					logger.log('getUserData()', userData);
					callback(userData);
				}
			});
		},

		getSettings: function (callback) {
			logger.log('getSettings()');
            var self = this;
			async.parallel({
				nativeLang: function (callback) {
					callback(null, localStorage.getItem('nativeLang'));
				},
				learningLang: function (callback) {
					callback(null, localStorage.getItem('learningLang'));
				},
				appLang: function (callback) {
                    if (!localStorage.getItem('appLang') || localStorage.getItem('appLang') === 'undefined') {
                        self.getPrefferedLanguage(callback);
                    } else {
                        callback(null, localStorage.getItem('appLang'));
                    }
				},
				adverts: function (callback) {
					callback(null, localStorage.getItem('adverts'));
				},
				learnInReverse: function (callback) {
					callback(null, localStorage.getItem('learnInReverse') === 'true');
				}
			}, function (error, settings) {
				if (error) {
					errorHandler(error);
				} else {
					logger.log('getSettings()', settings);
					callback(settings);
				}
			});
		},

		saveUserData: function (modelAttributes, callback) {
			logger.log('saveUserData()', modelAttributes);
			async.parallel({
				username: function (callback) {
					callback(null, localStorage.setItem('username', modelAttributes.username));
				},
				token: function (callback) {
					callback(null, localStorage.setItem('androidToken', modelAttributes.token));
				},
				loggedIn: function (callback) {
					callback(null, localStorage.setItem('loggedIn', modelAttributes.loggedIn));
				},
				created: function (callback) {
					callback(null, localStorage.setItem('created', modelAttributes.created));
				},
				lastSyncTimestamp: function (callback) {
					callback(null, localStorage.setItem('lastSyncTimestamp', modelAttributes.lastSyncTimestamp));
				},
				learningStarted: function (callback) {
					callback(null, localStorage.setItem('learningStarted', modelAttributes.learningStarted));
				},
				displayedExpressions: function (callback) {
					callback(null, localStorage.setItem('displayedExpressions', modelAttributes.displayedExpressions));
				},
				learnPoints: function (callback) {
					callback(null, localStorage.setItem('learnPoints', modelAttributes.learnPoints));
				}
			}, function (error, result) {
				if (error) {
					errorHandler(error);
				} else {
					userData = result;
					callback(userData);
				}
			});
		},

		saveSettings: function (settings, callback) {
			logger.log('saveSettings()', settings);
			async.parallel({
				nativeLang: function (callback) {
					callback(null, localStorage.setItem('nativeLang', settings.nativeLang));
				},
				learningLang: function (callback) {
					callback(null, localStorage.setItem('learningLang', settings.learningLang));
				},
				appLang: function (callback) {
					callback(null, localStorage.setItem('appLang', settings.appLang));
				},
				adverts: function (callback) {
					callback(null, localStorage.setItem('adverts', settings.adverts));
				},
				learnInReverse: function (callback) {
					callback(null, localStorage.setItem('learnInReverse', settings.learnInReverse));
				}
			}, function (error, result) {
				if (error) {
					errorHandler(error);
				} else {
					callback(settings);
				}
			});
		},

		init: function (callback) {
			logger.log('init()');
			callback();
		},

		showLoading: function (data) {
			logger.log('showLoading()');
			if (!loadingInited) {
			    initLoading();
				loadingInited = true;
			}
			loading.style.display = "block";
		},

		hideLoading: function () {
			logger.log('hideLoading()');
			loading.style.display = "none";
		},

		showSpinner: function (title, message, cancelCallback) {
			Loading.show('fullscreen');
		},

		hideSpinner: function () {
			Loading.hide();
		},

		notify: function (message) {
			logger.log('notify()', message);
			if (!notifyInited) {
			    initNotify();
			}
			notify.innerHTML = message;
			notify.style.display = "block";
			setTimeout(function () {
				notify.style.display = "none";
			}, 2000);
		},

		//		confirmPopup.show({
//			title: 'some title',
//			message: lang.WANNA_RESTART_LEARNING,
//			leftButton: {
//			label: 'label',
//			callback: function () {}
//			},
//		})
		confirm: function (options) {
			logger.log('confirm()', options);
			require(['app/views/components/confirmPopup'], function (confirmPopup) {
				confirmPopup.show({
					title: options.title,
					message: options.message,
					yesLabel: options.leftButton.label,
					noLabel: options.rightButton.label,
					yesCallback: options.leftButton.callback,
					noCallback: options.rightButton.callback
				})
			});
		},

		openDatabase: function (name, version, description) {
			logger.log('openDatabase()', name, version, description);
			return openDatabase(name, version, description, 2 * 1024 * 1024);
		},

		getDeviceUid: function () {
			logger.log('getDeviceUid()');
			return 'browser-device';
		},

        getPrefferedLanguage: function (successCallback, errorCallback) {
            successCallback(null, 'pol');
        }
	}
});