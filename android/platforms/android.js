define([
	'config',
	'logger',
	'async',
	'app/views/components/loading'
], function (
	config,
	Logger,
	async,
	loading
) {

	var logger = Logger.getOne('platform:ANDROID'),

		action = '';

	logger.log('Loading android platform...');

	function errorHandler(error, method) {
		logger.error("Error: " + action, error);
	}

	function warnHandler(error) {
		logger.warn("Error: " + action, error)
	}



	return {
		getUserData: function (finalCallback) {
			logger.log('getUserData()', finalCallback);
			var userData = null;
			action = 'getUserData';

			async.parallel({
				username: function (callback) {
					sharedpreferences.getString('username', function (username) {
						callback(null, username);
					}, function () {
						errorHandler();
						callback(null);
					});
				},
				token: function (callback) {
					sharedpreferences.getString('token', function (token) {
						callback(null, token);
					}, function () {
						errorHandler();
						callback(null);
					});
				},
				loggedIn: function (callback) {
					sharedpreferences.getBoolean('loggedIn', function (loggedIn) {
						callback(null, loggedIn);
					}, function () {
						errorHandler();
						callback(null, 0);
					});
				},
				created: function (callback) {
					sharedpreferences.getInt('created', function (created) {
						logger.log('retrieving \'created\' value:', created);
						callback(null, created);
					}, function () {
						errorHandler();
						callback(null, 0);
					});
				},
				lastSyncTimestamp: function (callback) {
					sharedpreferences.getInt('lastSyncTimestamp', function (lastSyncTimestamp) {
						callback(null, lastSyncTimestamp);
					}, function () {
						errorHandler();
						callback(null, 0);
					});
				},
				learningStarted: function (callback) {
					sharedpreferences.getInt('learningStarted', function (learningStarted) {
						callback(null, learningStarted);
					}, function () {
						errorHandler();
						callback(null, 0);
					});
				},
				displayedExpressions: function (callback) {
					sharedpreferences.getInt('displayedExpressions', function (displayedExpressions) {
						callback(null, displayedExpressions || 0);
					}, function () {
						errorHandler();
						callback(null, 0);
					});
				},
				learnPoints: function (callback) {
					sharedpreferences.getString('learnPoints', function (learnPoints) {
						callback(null, parseInt(learnPoints, 10));
					}, function () {
						errorHandler();
						callback(null);
					});
				}
			}, function (error, result) {
				if (error) {
					errorHandler(error);
					finalCallback({});
				} else {
					userData = result;
					logger.log('getUserData()', userData);
					finalCallback(userData);
				}
			});
		},

		getSettings: function (finalCallback) {
			logger.log('getSettings()');
			var userData = null,
				self = this;
			action = 'getSettings';

			async.parallel({
				nativeLang: function (callback) {
					sharedpreferences.getString('nativeLang', function (nativeLang) {
						callback(null, nativeLang);
					}, function () {
						errorHandler();
						callback(null);
					});
				},
				learningLang: function (callback) {
					sharedpreferences.getString('learningLang', function (learningLang) {
						callback(null, learningLang);
					}, function () {
						errorHandler();
						callback(null);
					});
				},
				appLang: function (callback) {
					sharedpreferences.getString('appLang', function (appLang) {
                        if (!appLang || appLang === null || appLang === 'null') {
                            self.getPrefferedLanguage(callback);
                        } else {
							callback(null, appLang);
						}
					}, function () {

						errorHandler();
						self.getPrefferedLanguage(callback);
					});
				},
				adverts: function (callback) {
					sharedpreferences.getString('adverts', function (adverts) {
						callback(null, adverts);
					}, function () {
						errorHandler();
						callback(null);
					});
				},
				learnInReverse: function (callback) {
					sharedpreferences.getBoolean('learnInReverse', function (learnInReverse) {
						callback(null, learnInReverse);
					}, function (err) {
						errorHandler(err);
						callback(false);
					});
				}
			}, function (error, settings) {
				if (error) {
					errorHandler(error);
					finalCallback({});
				} else {
					logger.log('getSettings()', settings);
					finalCallback(settings);
				}
			});
		},

		saveUserData: function (modelAttributes, finalCallback) {
			logger.log('saveUserData()', modelAttributes);
			action = 'saveUserData';
			async.parallel({
				username: function (callback) {
					sharedpreferences.putString('username', modelAttributes.username, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				token: function (callback) {
					sharedpreferences.putString('token', modelAttributes.token, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				loggedIn: function (callback) {
					sharedpreferences.putBoolean('loggedIn', !!modelAttributes.loggedIn, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				created: function (callback) {
					logger.log('setting \'created\' to ', modelAttributes.created);
					sharedpreferences.putInt('created', modelAttributes.created, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				lastSyncTimestamp: function (callback) {
					sharedpreferences.putInt('lastSyncTimestamp', modelAttributes.lastSyncTimestamp, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				learningStarted: function (callback) {
					sharedpreferences.putInt('learningStarted', modelAttributes.learningStarted, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				displayedExpressions: function (callback) {
					sharedpreferences.putInt('displayedExpressions', modelAttributes.displayedExpressions, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				nativeLang: function (callback) {
					sharedpreferences.putString('nativeLang', modelAttributes.nativeLang, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				learningLang: function (callback) {
					sharedpreferences.putString('learningLang', modelAttributes.learningLang, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				learnPoints: function (callback) {
					sharedpreferences.putString('learnPoints', modelAttributes.learnPoints, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				}
			}, function (error, result) {
				if (error) {
					finalCallback({});
				} else {
					userData = result;
					finalCallback(userData);
				}
			});
		},

		saveSettings: function (settings, finalCallback) {
			logger.log('saveSettings()', settings);
			action = 'saveSettings';
			async.parallel({
				nativeLang: function (callback) {
					sharedpreferences.putString('nativeLang', settings.nativeLang, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				learningLang: function (callback) {
					sharedpreferences.putString('learningLang', settings.learningLang, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				appLang: function (callback) {
					sharedpreferences.putString('appLang', settings.appLang, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				adverts: function (callback) {
					sharedpreferences.putString('adverts', settings.adverts, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(null);
					});
				},
				learnInReverse: function (callback) {
					sharedpreferences.putBoolean('learnInReverse', settings.learnInReverse, function () {
						callback(null);
					}, function (err) {
						errorHandler(err);
						callback(false);
					});
				}
			}, function (error, settings) {
				if (error) {
					finalCallback({});
				} else {
					finalCallback(settings);
				}
			});
		},

		hideStatusBar: function () {
			logger.log('hideStatusBar()');
			StatusBar.hide();
		},

		init: function (callback) {
			logger.log('init()', callback);
			logger.log('Initing shared preferences...', config.SHARED_PREFERENCES_FILENAME);
			sharedpreferences.getSharedPreferences(
				config.SHARED_PREFERENCES_FILENAME,
				"MODE_PRIVATE",
				function(a){
					logger.log('sharedPreferencesInited',a);
					callback();
				},
				function(a){logger.error('cannot init sharedPreferences',a)});

			this.hideStatusBar();
		},

		showLoading: function (data) {
			logger.log('showLoading()', data);
			var validData = {};
			if (data && data.timeout) {
				validData.timeout = data.timeout;
			}
			if (data && data.overlay) {
				validData.overlay = data.overlay;
			}
			spinnerplugin.hide();
			spinnerplugin.show(data);
		},

		showSpinner: function (title, message, cancelCallback) {
			logger.log('showSpinner()', title, message);
			loading.show('fullscreen');
			//window.plugins.spinnerDialog.show(title, message, cancelCallback);
		},

		hideSpinner: function () {
			logger.log('hideSpinner()');
			//window.plugins.spinnerDialog.hide();
			loading.hide();
		},

		hideLoading: function () {
			logger.log('hideLoading()');
			spinnerplugin.hide();
		},

		notify: function (message) {
			logger.log('notify()', message);
			plugins.toast.showShortCenter(message);
		},

//		confirmPopup.show({
//			title: 'some title',
//			message: lang.WANNA_RESTART_LEARNING,
//			leftButton: {
//			label: 'label',
//			callback: function () {}
//			},
//		})
		confirm: function (options) {
			logger.log('confirm()', options);
			navigator.notification.confirm(options.message, function (result) {
				if (result === 1 && typeof options.rightButton.callback === 'function') {
				    options.rightButton.callback();
				} else if (result === 2 && options.leftButton.callback === 'function') {
					options.leftButton.callback();
				}
			}, options.title, [options.rightButton.label, options.leftButton.label]);
		},

		openDatabase: function (name, version, description) {
			logger.log('openDatabase()', name, version, description);
			return sqlitePlugin.openDatabase(name, version, description, -1);
		},

		getDeviceUid: function () {
			logger.log('getDeviceUid()');
			return device.uuid;
		},

        getPrefferedLanguage: function (successCallback) {
            logger.error('NO IMPLEMENTATION, MOCK DATA');
			if (navigator.language === 'pl-PL' || navigator.language === 'pl-pl') {
				successCallback(null, 'pol');
			} else {
				successCallback(null, 'eng');
			}
        }
	}
});