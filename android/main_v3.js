// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
	urlArgs: 'bust=' +  (new Date()).getTime(),
	baseUrl: './',

	paths: {
		jquery: 'libs/jquery/jquery',
		backbone: 'libs/backbone/backbone',
		underscore: 'libs/underscore/underscore',
		text: 'libs/text',
		async: 'libs/async/async',
		logger: 'libs/logger',
		weinre: 'http://192.168.0.10:8585/target/target-script-min.js#anonymous',
		fastclick: 'libs/fastclick/fastclick',

		// overthrow
		'overthrow-detect': 'libs/overthrow/overthrow-detect',
		'overthrow-polyfill': 'libs/overthrow/overthrow-polyfill',
		'overthrow-toss': 'libs/overthrow/overthrow-toss',
		'overthrow-init': 'libs/overthrow/overthrow-init',

		//pages

		App: 'app/app',

		router: 'app/router/router',
		config: 'app/config',

		tpl: 'libs/doT/doT',

		components: 'app/views/components/',
		models: 'app/models/',
		collections: 'app/collections/',
		pages: 'app/views/pages/'

	},
	//Remember: only use shim config for non-AMD scripts,
	//scripts that do not already call define(). The shim
	//config will not work correctly if used on AMD scripts,
	//in particular, the exports and init config will not
	//be triggered, and the deps config will be confusing
	//for those cases.
	shim: {
		'backbone': {
			//These script dependencies should be loaded before loading
			//backbone.js
			deps: ['underscore', 'jquery'],
			//Once loaded, use the global 'Backbone' as the
			//module value.
			exports: 'Backbone'
		},
		'underscore': {
			exports: '_'
		},
		async: {
			exports: 'async'
		},
		'overthrow-detect': {},
		'overthrow-polyfill': {},
		'overthrow-toss': {},
		'overthrow-init': {}
	}
});

function init(platformName, androidVersion) {
	require(['async', 'logger'], function (async, Logger) {
		var logger = Logger.getOne('main');

		async.series([
            function (callback) {
                logger.log('app initialization, step 1 of 7, LOADING STYLESHEETS');
				logger.log('_android_version_:', androidVersion);
				if (androidVersion < 4) {
				    callback();
					return;
				}
                var cssToLoad = [
                    'common',
                    'wordsGroupsList',
                    'main',
                    'login',
                    'group',
                    'learn',
                    'browseList',
                    'mark',
                    'report',
                    'stats',
                    'settings',
                    'search',
                    'groupSearch'
                    ],
                    length = cssToLoad.length,
                    current = 0,
                    headElement = document.getElementsByTagName('head')[0];

                (function loadNext() {
                    if (cssToLoad[current]) {
                        logger.log('loading: ' + 'css/' + cssToLoad[current] + '.css');
                        var link = document.createElement('link');
                        link.setAttribute('rel', 'stylesheet');
                        link.setAttribute('type', 'text/css');
                        link.setAttribute('href', 'css/' + cssToLoad[current] + '.css');

                        link.addEventListener('load', function () {
                            current++;
                            loadNext();
                        }, false);

                        link.addEventListener('error', function () {
                            logger.error('ERROR while loading:', 'img/' + 'css/' + cssToLoad[current] + '.css');
                            current++;
                            loadNext();
                        }, false);
                        headElement.appendChild(link);
                    } else {
                        logger.log('CSS LOADING FINISHED');
                        callback();
                    }
                }());
            },
            function (callback) {
                logger.log('app initialization, step 2 of 7, LOADING IMAGES');
                var imagesToLoad = _imgList_,
                    current = 0,
                    headElement = document.getElementsByTagName('head')[0];

                (function loadNext() {
                    if (imagesToLoad[current]) {
                        logger.log('loading: ' + 'img/' + imagesToLoad[current]);
                        var img = new Image();
                        img.addEventListener('load', function () {
                            current++;
                            loadNext();
                        }, false);
                        img.addEventListener('error', function () {
                            logger.error('ERROR while loading:', 'img/' + imagesToLoad[current]);
                            current++;
                            loadNext();
                        }, false);
                        img.src = 'img/' + imagesToLoad[current];
                    } else {
                        logger.log('IMAGES LOADING FINISHED');
                        callback();
                    }
                }());
                //callback();
            },
			function (callback) {
				logger.log('app initialization, step 3 of 7, READING PLATFORM');
				require.config({
					paths: {
						platform: 'platforms/' + platformName
					}
				});
				callback();
			},

			function (callback) {
				logger.log('app initialization, step 4 of 7, INITIALIZING PLATFORM');
				require(['platform'], function (platform) {
					platform.init(function () {
						callback();
					});
				});
			},

			function (callback) {
				logger.log('app initialization, step 5 of 7, READING SETTINGS');
				require(['app/models/Settings'], function (Settings) {
					Settings.read({
						success: callback
					});
				});
			},

            function (callback) {
                logger.log('app initialization, step 6 of 7, PREPARING LANGUAGE PACKAGE');
                require(['app/models/Settings'], function (Settings) {
                    require.config({
                        paths: {
                            lang: 'lang/' + Settings.get('appLang')
                        }
                    });
                    callback();
                });
            }
		], function () {
			logger.log('app initialization, step 7 of 7, INITIALIZING APPLICATION');
			require(['App'], function (app) {
				app.initialize(platformName);
			});
		})
	});
}

var _platform = navigator.userAgent.match(/Android/) ? 'android' : 'browser';

document.addEventListener("deviceready", function () {
	var _android_version_ = parseInt(device.version[0]);
	init(_platform, _android_version_);
}, false);

require(['cordova'], function () {});



