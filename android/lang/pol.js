define({
	code: 'pol',
	// general
	LOGIN: 'login',
	LOGOUT: 'wyloguj',
	SEND: 'Wyślij',

    //action bar
    SEARCH_FOR_SETS: 'szukaj zestawów',
	CURRENT_SETS: 'aktualne zestawy',

    //paginator
    NEXT: 'nast.',
    PREV: 'poprz.',

	LOGGING: 'Logowanie',
	REGISTERING: 'Rejestracja',
	SELECT_NATIVE_LANG: 'Wybierz swój język ojczysty',
	SELECT_LEARNING_LANG: 'Wybierz język, którego chcesz się uczyć',

	LANGUAGES: {
		'bel': 'Białoruski',
		'bul': 'Bułgarski',
		'chi': 'Chiński',
		'hrv': 'Chorwacki',
		'cze': 'Czeski',
		'dan': 'Duński',
		'dut': 'Niderlandzki',
		'eng': 'Angielski',
		'est': 'Estoński',
		'fre': 'Francuski',
		'ger': 'Niemiecki',
		'gre': 'Grecki',
		'hun': 'Węgierski',
		'ind': 'Indonezyjski',
		'gle': 'Irlandzki',
		'ice': 'Islandzki',
		'ita': 'Włoski',
		'jpn': 'Japoński',
		'kor': 'Koreański',
		'lit': 'Litewski',
		'lav': 'Łotewski',
		'mac': 'Macedoński',
		'nor': 'Norweski',
		'pol': 'Polski',
		'por': 'Portugalski',
		'rum': 'Rumuński',
		'rus': 'Rosyjski',
		'srp': 'Serbski',
		'slo': 'Słowacki',
		'slv': 'Słoweński',
		'spa': 'Hiszpański',
		'swe': 'Szwedzki',
		'tur': 'Turecki',
		'ukr': 'Ukraiński',
		'vie': 'Wietnamski'
	},

	// validator

	FIELD: 'Pole',
	SHOULD_CONTAINS_MIN: 'powinno zawierać co najmniej',
	SHOULD_CONTAINS_MAX: 'powinno zawierać maksymalnie',
	SHOULD_NOT_CONTAIN_SPECIAL_CHAR: 'nie może zawierać znaku cudzysłowu ( " )',
	CHARS_234: 'znaki',
	CHARS: 'znaków',
	VALUE_TOO_SHORT: 'Wprowadzona wartość jest za krótka',
	INVALID_EMAIL: 'Niepoprawny format email',
	PASSWORDS_NOT_MATCH: 'Hasła się nie zgadzają',
	LANG_SHOULD_NOT_BE_THE_SAME: 'Nie możesz uczyć się swojego natywnego języka!',

	REQUEST_ERROR: 'Sprawdź swoje połączenie sieciowe',
    Server: {
        '001': 'Wystąpił błąd',
        '002': 'Niepoprawne zapytanie',
        '003': 'Brak użytkownika o podanej nazwie',
        '004': 'Niepoprawne hasło',
        '005': 'Nazwa użytkownika i hasło są wymagane',
        '006': 'Użytkownik nie jest zalogowany',
        '007': 'Nazwa i email są wymagane',
        '008': 'Email jest wymagany',
        '009': 'Brak potwierdzonego hasła',
        '010': 'Hasła się nie zgadzają',
        '011': 'Brak lub niepoprawny format email',
        '012': 'Na podany email zarejestrowano już użytkownika',
        '013': 'Użytkownik o tej nazwie już istnieje',
        '014': '',
        '015': 'Nazwa użytkownika może zawierać tylko litery, cyfry oraz znaki \'_\' , \'-\', \' \' \' \'.\'',
        '016': 'Nazwa użytkownika musi zawierać od 2 do 50 znaków',
        '017': 'Hasło musi zawierać od 6 do 50 znaków',
        '018': 'Adres email nie może zawierać więcej niż 254 znaków',
        '019': 'Pola: nazwa użytkownika, język natywny, język nauki i hasło są wymagane',
        '020': 'Nie można dodać wyrażenia',
        '021': 'Brak nazwy użytkownika',
        '022': 'Brak lub niepoprawna wartość pola: origin',
        '023': 'Brak lub niepoprawna wartość pola: translated',
        '024': 'Takie wyrażenie istnieje już w tym zestawie',
        '025': 'Nie można utworzyć grupy',
        '026': 'Musisz podać nazwę użytkownika i nazwę tworzonego zestawu',
        '027': 'Zestaw o podanych parametrach już istnieje',
        '028': 'Nie można pobrać zestawu',
        '029': 'Musisz podać nazwę klasy',
        '030': 'Musisz podać swój język natywny',
        '031': 'Musisz podać oznaczenie języka, którego zamierzasz się uczyć',
        '032': 'Musisz podać poziom klasy"',
        '033': 'Poziom klasy jest niepoprawny. Dostępne wartości to: A1, A2, B1, B2, C1, C2',
        '034': 'Podana wartość nie może zawierać znaku cudzysłowu (")',
        '035': 'Niepoprawna wartość pola: język natywny',
        '036': 'Niepoprawna wartość pola: język nauki',
        '037': 'Niepoprawna wartość pola: appLang',
        '038': 'Niepoprawne dane: nie możesz uczyć się swojego języka natywnego',
        '039': 'Opis zestawu jest zbyt długi, może zawierać maksymalnie 250 znaków',
        '040': 'Niepoprawny format nazwy zestawu. Nazwa powinna być tekstem o długości min 3 i max 50 znaków',
        '041': 'Musisz podać nazwę tworzonego zestawu',
        '042': 'Brak lub niepoprawna kategoria',
        '043': 'Brak lub niepoprawny poziom zestawu',
        '044': 'Niepoprawna kategoria',
        '045': 'Nie można zmienić nazwy zestawu',
        '046': 'Nie możesz ocenić własnego zestawu',
        '047': 'Nie można ocenić zestawu',
        '048': 'Ten zestaw był już oceniany przez Ciebie',
        '049': 'Aby dodać wyrażenie musisz być autorem zestawu',
        '050': 'Już uczysz się tego zestawu',
        '051': 'Niepoprawne dane: brak device uid',
        '052': 'Brak zestawu o podanym id',
        '053': 'Zestaw możesz zgłosić tylko raz na dobę',
        '054': 'Nie masz wystarczającej ilości punktów nauki',
        '055': 'Te urządzenie zostało połączone z innym kontem. Skontaktuj się z nami jeśli chcesz zresetować konto',
        '056': 'Promocyjne punkty zostały już wykorzystane',
        '057': 'Niepoprawny email',
        '058': 'Możesz wysłać maksymalnie 10 wiadomości dziennie',
        '059': '',
        '060': '',
        '061': ''
    },

	CATEGORIES: {
		'001': 'Różne',
		'002': 'Zwierzęta',
		'003': 'Dom',
		'004': 'Szkoła',
		'005': 'Praca',
		'006': 'Biuro',
		'007': 'Rodzina',
		'008': 'Człowiek',
		'009': 'Świat',
		'010': 'Nauka',
		'011': 'Zwroty',
		'012': 'Styl życia',
		'013': 'Hobby',
		'014': 'Kuchnia'
	},

	LEVELS: {
		1: 'początkujacy',
		2: 'średniozaawansowany',
		3: 'zaawansowany'
	},

	// main i group i browse i groupSearch
	CATEGORY: 'kategoria',
	NUMBER_OF_EXPRESSIONS: 'liczba wyrażeń',
	EXPRESSIONS: 'wyrażeń',
	CREATED: 'utworzono',
	DOWNLOADED_DAY: 'pobrano',
	AUTHOR: 'autor',
	LEARN: 'ucz się',
	REPEAT_LEARN: 'powtórz naukę',
	BROWSE: 'przeglądaj',
	MARK: 'oceń',
	REPORT: 'zgłoś',
	UPDATING_SET: 'Aktualizuję zestaw',
	ERROR_OCCURED: 'Wystąpił błąd',
	LOADING_DATA: 'Wczytuje dane',
	EMPTY_SET: 'Do tego zestawu nie dodano jeszcze wyrażeń',
	INFO: 'Info',
	CANNOT_MARK_YOUR_SET: 'Nie możesz ocenić własnego zestawu',
    CANNOT_MARK_RECOMMEND_SET: 'nie możesz oceniać rekomendowanego zestawu',
	CANNOT_REPORT_YOUR_SET: 'Nie możesz zgłosić własnego zestawu',
    CAN_REPORT_ONCE_PER_DAY: 'Zestaw możesz zgłosić raz na dobę',
	CANNOT_MARK_AGAIN: 'Już oceniłeś ten zestaw',
	SET: 'zestaw',
	LEVEL: 'poziom',
	DOWNLOADED: 'pobrań',
	START_LEARN: 'rozpocznij naukę',
	SYNCHRONIZATION_NEEDED: 'Rozpocząłeś naukę zestawu. Aby pobrać wyrażenia musisz się zsynchronizować',
	SYNCHRONIZE: 'Synchronizuj',
	LATER: 'Póżniej',
	NO_SETS_WITH_GIVEN_CRITERIA: 'Brak zestawów spełniających podane kryteria',
    LEARN_POINTS_LACK: 'Brak punktów nauki',
    GO_FOR_LEARN_POINTS_1: 'Aby pozyskać punkty nauki odwiedź',
    GO_FOR_LEARN_POINTS_2: 'i postępuj zgodnie z zamieszczoną tam instrukcją',
    RATES_NUMBER: 'głosów',
    MY_FICHES: 'myFiches',
    FF_RECOMMENDATION: 'rekomendacja',
	//searchGroup
	DOWNLOAD: 'pobierz',
	ALREADY_LEARNING: 'Uczysz się już tego zestawu. ZSYNCHRONIZUJ się jeśli nie ma go na liście',

	// mark
	MARK_0: 'fatalny',
	MARK_1: 'kiepski',
	MARK_2: 'średni',
	MARK_3: 'dobry',
	MARK_4: 'świetny',
	SET_MARKED: 'oceniłeś zestaw',
	GROUP_REPORTED: 'zgłosiłeś zestaw',
	SELECT_MARK: 'Oceń zestaw',
	SELECT_MARK_DETAILS: 'Dotknij ikony gwiazdki aby ocenić zestaw',

	// report
	SELECT_VIOLATION_TYPE: 'Wybierz typ naruszenia',
	SELECT_TYPE: 'Wybierz typ',
    REPORT_1: 'Większość zestawów tworzona jest przez użytkowników',
    REPORT_2: 'Jeśli jakiś zestaw narusza zasady regulaminu',
    REPORT_3: 'zawiera niepoprawne tłumaczenia lub wulgaryzmy',
    REPORT_4: 'możesz to tutaj zgłosić',
    REPORT_5: 'Wybierz typ naruszenia',
    REPORT_6: 'Wulgaryzmy',
    REPORT_7: 'Niepoprawne tłumaczenia',



	// notifications
	USER_REGISTERED_LOGIN: 'konto utworzone, możesz się zalogowć',
	USER_LOGGED_IN: "zalogowałeś się",
	USER_LOGGED_OUT: "wylogowałeś się",
	NEED_TO_LOGIN: "musisz się zalogować",
	DATA_UP_TO_DATE: "wszystkie zestawy są aktualne",
	LEARNING_STOPPED: "zestaw usunięty",
	WANNA_STOP_LEARNING: "Zakończyć naukę zestawu",
	CANNOT_LEARNING_STOPPED: "nie można zakończyć nauki",

	LOGGING_RUNNING: 'Loguje',
	SYNCHRONIZATION_RUNNING: 'synchronizuje',
	SYNCHRONIZATION_COMPLETED: 'Synchronizacja zakończona pomyślnie',
    SYNC_GROUPS_ADDED: 'dodano zestawów',
    SYNC_EXPRESSIONS_ADDED_CHANGED: 'dodano/zmieniono wyrażeń',
    SYNC_GROUPS_REMOVED: 'usunięto zestawów',
	CONNECTING_TO_INTERNET: 'łączę z internetem',

	// main
    MY_FICHES_COM: 'www.myfiches.com',
	NO_SETS_SELECTED: 'Nie wybrałeś jeszcze zestawów do nauki. Odwiedź www.learnitquickly.com aby wygodnie edytować zestawy, następnie wróć do aplikacji i wybierz \'synchronizacja\' z rozwijanego menu. ' +
		'\nMożesz także wyszukać i rozpocząć naukę wybranego zestawu wybierająsz \'szukaj\' z menu.\n Życzymy owocnej nauki! ',
	PROGRESS: 'postęp',
	LAST_SEEN: 'ostatnio',
	NOT_STARTED: 'nie rozpocząłeś',
    MORE: 'więcej',
    NO_SETS_1: 'Obecnie nie uczysz się żadnego zestawu',
    NO_SETS_2: 'Aby rozpocząć naukę możesz wykonać jedną z poniższych czynności',
    NO_SETS_3: 'otwórz menu (strzałka w prawym górnym rogu) i wybierz opcje',
    NO_SETS_4: 'szukaj zestawów',
    NO_SETS_5: 'następnie wybierz insteresujący Cię zestaw i rozpocznij naukę wybierając odpowiednią opcję',
    NO_SETS_6: 'odwiedź stronę ',
    NO_SETS_8: 'aby',
    NO_SETS_9: 'WYGODNIE',
    NO_SETS_10: 'przeglądać, rozpoczynać naukę oraz',
    NO_SETS_11: 'TWORZYĆ WŁASNE',
    NO_SETS_12: 'zestawy',
    NO_SETS_13: 'UWAGA',
    NO_SETS_14: 'Aby zestawy, które wybrałeś na stronie',
    NO_SETS_15: 'pojawiły się w aplikacji, musisz zsynchronizować konto wybierając przycisk',
    NO_SETS_16: 'w prawym górnym rogu okna aplikacji',


	//settings
	SETTINGS: 'ustawienia',
	APPLICATION_LANG: 'Język aplikacji',
	NATIVE_LANG: 'Język ojczysty',
	LEARNING_LANG: 'Język, którego się uczysz',
	NATIVE_LANG_1: 'język ojczysty',
	LEARNING_LANG_1: 'język, którego się uczysz',
	SETTINGS_NETWORK_REQUIRED: 'Zmiana tych ustawień wymaga połączenia z internetem',
	SELECT_LANG: 'Wybierz język',


	//stats
	STATS: 'statystyki',
	GATHERING_STATISTICS: 'Pobieram statystyki',
    STARTED_TO_LEARN: 'Rozpocząłeś naukę',
    CURRENTLY_SETS_LEARN: 'Aktualnie uczysz się',
    WHICH_CONTAINS_1: 'Który zawiera',
    WHICH_CONTAINS: 'Które łącznie zawierają',
    DISPLAYED_FOR_NOW: 'Do tej pory wyświetliłeś',
    DAILY_AVERAGE_DISPLAY: 'Średnio dziennie wyświetlasz',
    LEARNED_FOR_NOW: 'Nauczyłeś się już',
    DAILY_AVERAGE_LEARN: 'Średnio dziennie uczysz się',
    SETS: 'zestawów',
    LEARNING_NOT_STARTED: 'Jeszcze nie rozpocząłeś nauki',
    END_LEARNING: 'zakończ naukę',
    SET_1: 'zestawu',
    SET_2: 'zestawu',


	// learn
	PREPARING_GROUP_LEARNING: 'Przygotowuję zestaw...',
	ENDED: 'Ukończono',
	LEARNING_ENDED: 'Pamiętasz wszystkie wyrażenia z tego zestawu',
	QUESTION: 'Pytanie',
	ANSWER: 'Odpowiedź',
	USAGE_EXAMPLE: 'Przykład użycia',
    SHOW_ANSWER: 'Pokaż odpowiedź',

	// confirm popup
	WANNA_RESTART_LEARNING: 'Chcesz rozpocząć naukę od nowa?',
	WANNA_START_LEARN_1: 'Koszt tego zestawu to',
	LEARN_POINTS: 'punktów nauki',
	LEARN_POINTS_1: 'punkty nauki',
	WANNA_START_LEARN_2: 'Rozpocząć naukę?',
	ARE_U_SURE: 'Na pewno?',
	CANCEL: 'Anuluj',
	OK: 'OK',

	//LEARN POINTS
	TOUCH_IMAGE: 'Dotknij obrazek aby odświeżyć liczbę punktów nauki',
	YOU_HAVE_RIGHT_NOW: 'Posiadasz w tej chwili',
	TO_GET_MORE: 'Aby zdobyć więcej zaloguj się na',



	//LOGIN I REGISTER
	USERNAME: 'nazwa użytkownika',
	PASSWORD: 'hasło',
	REPEAT_PASSWORD: 'powtórz hasło',
	EMAIL: 'email',


	//SORTER
	SORTING: 'Sortowanie',
	SORT: 'Sortuj',
	SORT_BY: 'Sortuj według',
	NAME: 'nazwa',
	MARK_VALUE: 'ocena',
	MARK_COUNTER: 'liczba ocen',
	SELECT_CATEGORY: 'Wybierz kategorię',
	NO_FILTER_FIELD_SELECTED: 'Nie wybrano pola',
	SELECT_FILTER_FIELD: 'Wybierz pole, którego chcesz użyć do filtrowania',
	SELECT_LEVEL: 'Wybierz poziom',
	SORT_DIRECTION: 'Kierunek sortowania',
	ASCENDING: 'rosnąco',
	DESCENDING: 'malejąco',
	FILTERING: 'Filtrowanie',
	FILTER: 'Filtruj',
	SEARCH: 'Szukaj',
	SEARCH_1: 'szukaj',
	DOWNLOADS_NUMBER: 'liczba pobrań',
	RESET_TO_DEFAULTS: 'Przywróć domyślne',
    VALUE: 'Wartość',

	// date time
	AGO: 'temu',
	DAYS: 'dni',
	MONTHS_2_4: 'miesięce',
	MONTHS: 'miesięcy',

	HOURS_2_4: 'godziny',
	HOURS: 'godzin',

	MINUTES_2_4: 'minuty',
	MINUTES: 'minut',

	AN_MOMENT_AGO: 'przed chwilą',
	AN_MINUTE_AGO: 'minutę temu',
	AN_HOUR_AGO: 'godzinę temu',
	A_DAY_AGO: '1 dzień temu',
	A_MONTH_AGO: 'miesiąc temu',
	A_YEAR_AGO: 'ponad rok temu'
});