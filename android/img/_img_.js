var fs = require('fs');

function getFiles(dir,files_){
    files_ = files_ || [];
    if (typeof files_ === 'undefined') files_=[];
    var files = fs.readdirSync(dir);
    for(var i in files){
        if (!files.hasOwnProperty(i)) continue;
        var name = dir+'/'+files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name,files_);
        } else {
            files_.push(name.replace('./', ''));
        }
    }

    return files_;



}

var files = getFiles('.');
var content = 'var _imgList_ = [\n';

for(var i = 0, l = files.length; i < l; i++) {
    if (files[i] !== '_imgList_.js' && files[i] !== '_img_.js') {
        content += ('\'' + files[i] + '\',\n');
    }
}
content = content.substring(0, content.length - 2);
content += ']';

fs.writeFile("_imgList_.js", content, function(err) {
    if(err) {
        console.log(err);
    } else {
        console.log("The file was saved!");
    }
});