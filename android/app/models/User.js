define([
	'App',
	'backbone',
	'platform',
	'logger',
	'config',
	'app/dao',
	'app/models/Settings',
	'lang',
	'underscore'
], function (
	app,
	Backbone,
	platform,
	Logger,
	config,
	dao,
	Settings,
	lang,
	_
){
	var logger = Logger.getOne('models/User');

	var User = Backbone.Model.extend({

		loginUrl: config.url.LOGIN,
		registerUrl: config.url.REGISTER,
		logoutUrl: config.url.LOGOUT,

		defaults: {
			username: '',
			token: '',
			lastSyncTimestamp: 1,
			loggedIn: false,
			created: false, // wskazuje czy użytkownik był już tworzony na urządzeniu
			learningStarted: false, // czas rozpoczęcia nauki w sekundach (pierwsze wejście do sceny learning) - używane w statystykach
			displayedExpressions: 0,
			nativeLang: '',
			learningLang: '',
			deviceUid: '',
			learnPoints: 0
		},

		onLearningStart: function () {
			logger.log('onLearningStart()');
			if (!this.get('learningStarted')) {
			    this.set('learningStarted', Math.round(new Date().getTime() / 1000));
				this.write();
			}
		},

		onLearningEnd: function (displayedExp) {
			logger.log('onLearningEnd()', displayedExp);
			if (displayedExp) {
				this.set('displayedExpressions', this.get('displayedExpressions') + displayedExp);
				this.write();
			}
		},

		incrementDisplayedExppressionsCounter: function () {
			logger.log('incrementDisplayedExppressionsCounter()');
			this.set('displayedExpressions', parseInt(this.get('displayedExpressions'), 10) + 1);
			this.write();
		},

		initialize: function () {
			// todo remove it
			logger.warn('TO REMOVE !!!!!!!!! ');
			window.user = this;

			app.on('clearUserData', this.clearUserData, this);
			app.on('learningStart', this.onLearningStart, this);
			app.on('learningEnd', this.onLearningEnd, this);
		},

		read: function (options) {
			var self = this;
			logger.log("read()", options);
			platform.getUserData(function (userData) {
				if (userData) {
					self.set(userData);
				}
				options.success();
			});
		},

		write: function (options) {
			if (!options) {
			    options = {};
			}
			platform.saveUserData(this.toJSON(), options.success || function () {});
		},

		onLoginSuccess: function (response, callback) {
			logger.log('onLoginSuccess()', response);
			var self = this;
			this.set('loggedIn', true);
			this.set('password', '');
			this.set('learnPoints', response.learnPoints);

			if (!this.get('created') || this.get('created') === null || this.get('created') === 'null') {
				logger.log('THIS IS FIRST LAUNCH !!!!!!!!!!!!!!');
				dao.createInitialTables();
				this.set('created', 1);
			}
			this.write({
				success: function () {
					Settings.onLoginSuccess(response, callback);
				}
			});
		},

		onLoginFailure: function () {
			logger.log('onLoginFailure()', arguments);
		},

		login: function (data, options) {
			logger.log('login()');
			var self = this,
				error = options.error,
				success = options.success;

			options.error = function () {
				self.onLoginFailure(arguments);
				if (error) {
					error.apply(null, arguments);
				}
				self.set('password', '');
			};

			options.success = function (model, response) {
				self.onLoginSuccess(response, function () {
					success.apply(null, arguments);
				});
			};

			this.sync = function (method, model, xhr) {
				model.url = self.loginUrl;
				Backbone.Model.prototype.sync.apply(this, arguments);
			};

			this.save({
				username: data.username,
				password: data.password,
				deviceUid: platform.getDeviceUid()
			}, options);
		},

		register: function (data, options) {
			platform.showSpinner(null, null);
			Backbone.ajax({
				type: 'POST',
				url: config.url.REGISTER,
				success: function () {
					if (options && options.success) {
						options.success();
					}
				}.bind(this),
				error: options.error || function () {},
				data: JSON.stringify(data),
				contentType: 'application/json'
			});
		},

		logout: function (options) {
			this.set('loggedIn', 0);
			this.set('token', '');
			this.write(options);
		},

		synchronize: function (options) {
			platform.showSpinner(null, lang.SYNCHRONIZATION_RUNNING + '...');
			// todo remove
			var lastSyncTimestamp = this.get('lastSyncTimestamp'),
//			var lastSyncTimestamp = 1,
				newSyncTimestamp,
				self = this,
				daoSyncSuccess = function (changed) {
					self.set('lastSyncTimestamp', newSyncTimestamp);
					self.write({
						success: function () {
							options.success(changed);
						}
					});
				};

			if (lastSyncTimestamp === 'null') {
			    lastSyncTimestamp = 1;
			}
			logger.log('synchronize()', lastSyncTimestamp);

			dao.getStatsData(function (statsData) {
				var stats = {
					learningStarted: this.get('learningStarted') || 0,
					learnedTotal: statsData.learnedTotal,
					displayedTotal: this.get('displayedExpressions') || 0
				};

				_.extend(statsData, {learningStarted: this.get('learningStarted')});
				Backbone.ajax({
					url: config.url.SYNCHRONIZE + '/' + (lastSyncTimestamp || 0),
					type: 'POST',
					success: function (syncData) {
						try {
							newSyncTimestamp = JSON.parse(syncData).currentTimestamp;
						} catch (e) {
							logger.error('Cannot parse data');
						}
						//platform.showSpinner(null, lang.SYNCHRONIZATION_RUNNING + '...');
						dao.performSynchronization(syncData, {success: daoSyncSuccess});
					},
					error: function () {
						logger.error('Error while synchronizing');
						if (options.error) {
							options.error('Error while synchronizing');
						}
					},
					data: JSON.stringify(stats),
					contentType: 'application/json'
				})
			}.bind(this));

		},

		clearUserData: function () {
			logger.log('clearUserData()');
			this.set({
				lastSyncTimestamp: 1,
				loggedIn: false,
				token: '',
				created: false,
				username: '',
				learningStarted: 0,
				displayedExpressions: 0,
				lang: ''
			}).write({
				success: function () {
					logger.log('User data reset FINISHED');
				}
			});
		},

        getLearnPoints: function (options) {
            logger.log('getLearnPoints()');
            Backbone.ajax({
                type: 'GET',
                url: config.url.LEARN_POINTS,
                success: function (response) {
                    if (options && options.success) {
						this.set('learnPoints', parseInt(response));
						this.write({
							success: function () {
								options.success(parseInt(response));
							}
						});
                    }
                }.bind(this),
                error: options.error || function () {}
            });
        }

	});

	return new User();
});