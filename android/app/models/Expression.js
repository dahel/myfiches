define([
	'backbone',
	'logger'
], function (
	Backbone,
	Logger
){
	var logger = Logger.getOne('Model:Expression');

	return Backbone.Model.extend({
		defaults: {
			origin: '',
			translated: '',
			usage_example: '-',
			mark: ''
		},

		initialize: function () {
			logger.log('Model:Expression:initialize', this);
		}
	});
});