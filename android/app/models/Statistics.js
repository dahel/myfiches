define([
	'backbone',
	'platform',
	'app/dao',
	'async',
	'logger',
	'app/models/User'
], function (
	Backbone,
	platform,
	dao,
	async,
	Logger,
	User
){
	var logger = Logger.getOne('models/Statistics');

	var Statistics = Backbone.Model.extend({

		defaults: {
			learningStarted: 0,
			learnedTotal: 0,
			displayedTotal: 0,
			numberOfSets: 0,
			totalExpressionsNumber: 0
		},

		initialize: function () {
			window.stats = this;
		},

		sync: function (method, model, options) {
			logger.log('sync()');
			if (method !== 'read') {
			    logger.error('Statistics are read-only');
				return;
			}

			async.parallel({
				learningStarted: function (callback) {
					callback(null, User.get('learningStarted'));
				},
				learnedTotal: function (callback) {
					dao.getLearnedExpressionsNumber(callback);
				},
				displayedTotal: function (callback) {
					callback(null, User.get('displayedExpressions'));
				},
				numberOfSets: function (callback) {
					dao.getNumberOfWordsGroups(callback);
				},
				totalExpressionsNumber: function (callback) {
					dao.getTotalNumberOfExpressions(callback);
				}
			}, function (error, result) {
				logger.log('Statistics gathered!', result);
				options.success(result);
			})
		}
	});

	return new Statistics();
});