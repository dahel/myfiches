define([
	'backbone',
	'logger',
	'lang',
	'underscore',
	'config'
], function (
	Backbone,
	Logger,
	lang,
	_,
	config
){
	var logger = Logger.getOne('Model:SearchWordsGroup');

	return Backbone.Model.extend({

		url: function () {
			return config.url.WORDS_GROUPS + '/' + this.id;
		},

		defaults: {
			name: '',
			author: '',
			length: 0,
			progress: 0,
			created: '',
			markCounter: 0,
			markValue: 0,
			description: '',
			category: '',
			downloaded: 0,
			marked: 0,
			userMarkValue: 0,
			level: 0,
            ff_recommend: 0,
			lp_cost: 99
		},

		initialize: function () {
			logger.log('Model:SearchWordsGroup:initialize', this);
		},


		parse: function (response) {
			return response.body || response;
		},

		startLearning: function (options) {
			logger.log('startLearning()', this.get('id'));
			Backbone.ajax({
				type: 'POST',
				url: config.url.WORDS_GROUPS_LEARNING + '/' + this.id,
				success: function () {
					if (options && options.success) {
						options.success();
					}
				}.bind(this)
			});
		}
	});
});