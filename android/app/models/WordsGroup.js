define([
	'backbone',
	'logger',
	'app/utils/dateTime',
	'lang',
	'underscore',
	'config'
], function (
	Backbone,
	Logger,
	dateTime,
	lang,
	_,
	config
){
	var logger = Logger.getOne('Model:WordsGroup');

	return Backbone.Model.extend({
		defaults: {
			name: '',
			author: '',
			length: 0,
			progress: 0,
			created: '',
			markCounter: 0,
			markValue: 0,
			description: '',
			category: '',
			downloaded: 0,
			marked: 0,
			userMarkValue: 0,
			origin_lang: '',
			translation_lang: '',
			download_date: '',
			level: 0
		},

		urlRoot: config.url.WORDS_GROUP,

		initialize: function () {
			logger.log('Model:WordsGroup:initialize', this);
		},

		sync: function (method, collection, options) {
			logger.log('sync', method, this.toJSON());
			switch (method) {
				case 'read':
					dao.getWordsGroup(this.id, options.success);
					break;
				case 'patch': // for sending user mark
					Backbone.sync.apply(this, arguments);
					break;
				default:
					break;
			}
		},

		parse: function (attrs) {
			// przy zapisywaniu oceny użytkownika serwer nie zwraca całego obiektu
			if (!attrs) {
			    return {};
			}
			var parsed,
				result = {};
			if (attrs.last_time_learn === '0') {
				parsed= lang.NOT_STARTED;
			} else {
				parsed = dateTime.getReadableTimePassed(parseInt(attrs.last_time_learn));
			}

			for (var n in attrs) {
				if (attrs.hasOwnProperty(n)) {
				    if (n === 'last_time_learn') {
				        result[n] = parsed;
				    } else {
						result[n] = attrs[n];
					}
				}
			}

			return result;
		},

		sendMark: function (options) {
			logger.log('sendMark()', options.mark);
			dao.setWordsGroupMarked(this.id);
			this.save({
				userMarkValue: options.mark
			}, _.extend(options, {patch: true}));
		},


		sendReport: function (options) {
			Backbone.ajax({
				type: 'POST',
				url: config.url.REPORT + '/' + this.id,
				success: function () {
					if (options && options.success) {
						options.success();
					}
				}.bind(this),
				data: JSON.stringify({
					reportType: options.reportType
				}),
				contentType: 'application/json'
			});
		},

		stopLearn: function (options) {
			logger.log('stopLearn()', this.id, options);
			var self = this;
			var finalSuccess = function () {
				logger.log('stopLearn:completed');
				dao.deleteWordsGroup(self.id, options.success);
			};

			Backbone.ajax({
				type: 'DELETE',
				url: config.url.WORDS_GROUPS_LEARNING + '/' + this.id,
				success: finalSuccess
			});
		}
	});
});