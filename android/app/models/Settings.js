define([
	'backbone',
	'platform',
	'logger',
	'config'
], function (
	Backbone,
	platform,
	Logger,
	config
){
	var logger = Logger.getOne('models/Settings');

	var Settings = Backbone.Model.extend({

		urlRoot: config.url.SETTINGS,

		defaults: {
			nativeLang: '',
			learningLang: '',
			appLang: '',
			adverts: '',
			learnInReverse: false
		},

		initialize: function () {
			window.settings = this;
			this.on('sync', this.write, this);
		},

		read: function (options) {
			var self = this;
			logger.log("read()", options);
			platform.getSettings(function (settings) {
				if (settings) {
					self.set(settings);
				}
				options.success();
			});
		},

		write: function (options) {
			if (!options) {
			    options = {};
			}
			platform.saveSettings(this.toJSON(), options.success || function () {});
		},

		onLoginSuccess: function (response, callback) {
			logger.log('onLoginSuccess()', response);
			this.set({
				learningLang: response.learningLang,
				nativeLang: response.nativeLang,
				adverts: response.adverts
			}, {silent: true});
			this.write({
				success: callback
			})
		},

		applyChange: function (key, value) {
			logger.log('applyChange()', key, value);
			if (key === 'appLang') { // app lang jest przechowywane lokalnie, może być różne dla klienta browserowego i mobilnego
			    this.set(key, value);
				this.trigger('sync');
			} else {
				var data = {};
				if (!this.has(key)) {
					logger.error('model has no such attribute: ' + key);
				} else {
					if (this.get(key) !== value) {
						platform.showSpinner();
						data[key] = value;
						this.save(data, {
							success: function () {
								platform.hideSpinner();
							},
							error: function () {
								platform.hideSpinner();
							},
							wait: true
						});
					}
				}
			}
		},

		clearUserData: function () {
			logger.log('clearUserData()');
			this.set({
				nativeLang: '',
				learningLang: ''
			}, {silent: true}).write({
				success: function () {
					logger.log('Settings data reset FINISHED');
				}
			});
		}

	});

	return new Settings();
});