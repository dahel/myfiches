define([
	'backbone',
	'logger',
	'lang'
], function (
	Backbone,
	Logger,
	lang
) {

	var logger = Logger.getOne('validator');

	return {
		currentResult: null,

		VALIDATOR_TYPES: {
			username: {
				minLength: {
					value: 2,
					message: lang.FIELD + ' "' + lang.USERNAME + '" ' + lang.SHOULD_CONTAINS_MIN+ ' 2 ' + lang.CHARS_234
				},
				maxLength: {
					value: 50,
					message: lang.FIELD+ ' "' + lang.USERNAME + '" ' + lang.SHOULD_CONTAINS_MAX + ' 50 ' + lang.CHARS
				}
			},

			password: {
				minLength: {
					value: 6,
					message: lang.FIELD + ' "' + lang.PASSWORD + '" ' + lang.SHOULD_CONTAINS_MIN+ ' 6 ' + lang.CHARS
				},
				maxLength: {
					value: 50,
					message: lang.FIELD+ ' "' + lang.PASSWORD + '" ' + lang.SHOULD_CONTAINS_MAX + ' 50 ' + lang.CHARS
				}
			},

			repeatPassword: {
				equalToExtraValue: {
					message: lang.PASSWORDS_NOT_MATCH
				}
			},

			email: {
				pattern: {
					value: /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i,
					message: lang.INVALID_EMAIL
				}
			},

			learningLanguage: {
				notEqualToExtraValue: {
					message: lang.LANG_SHOULD_NOT_BE_THE_SAME
				}
			}
		},

		invalid: function (typeData, fieldName) {
			currentResult = {
				valid: false,
				message: typeData[fieldName].message
			};
			return this;
		},

		validate: function (type, value, extraValue) {
			logger.log('validate()', type, value);
			var typeData;

			if (!this.VALIDATOR_TYPES.hasOwnProperty(type)) {
				logger.error('There is no such type: ', type);
				return this;
			}

			typeData = this.VALIDATOR_TYPES[type];


			if (typeData.hasOwnProperty('minLength') && typeData['minLength'].value > value.length) {
				logger.warn('_validate()', 'value is too short', value);
				return this.invalid(typeData, 'minLength');
			}

			if (typeData.hasOwnProperty('maxLength') && typeData['maxLength'].value < value.length) {
				logger.warn('_validate()', 'value is too long', value);
				return this.invalid(typeData, 'maxLength');
			}

			if (typeData.hasOwnProperty('notContains') && value.indexOf(typeData['notContains'].value)  > -1) {
				logger.warn('_validate()', 'value should not contain special char (")', value);
				return this.invalid(typeData, 'notContains');
			}

			if (typeData.hasOwnProperty('pattern') && !typeData['pattern'].value.test(value)) {
				logger.warn('_validate()', 'invalid format', value);
				return this.invalid(typeData, 'pattern');
			}

			if (typeData.hasOwnProperty('equalToExtraValue') && value !== extraValue) {
				logger.warn('_validate()', 'notEqual', value);
				return this.invalid(typeData, 'equalToExtraValue');
			}

			if (typeData.hasOwnProperty('notEqualToExtraValue') && value === extraValue) {
				logger.warn('_validate()', 'notEqualToExtraValue', value);
				return this.invalid(typeData, 'notEqualToExtraValue');
			}



			logger.log('value is VALID');
			currentResult = {
				valid: true
			};
			return this;

		},

		result: function () {
			return currentResult;
		}
	}

});