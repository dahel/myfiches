define([
	'logger',
	'lang'
],
function (
	Logger,
	lang
) {
	var logger = Logger.getOne('dateTime');

	function response(value, code) {
		return {
			value: value,
			code: code
		};
	}

	function getReadableTimePassed(UTCtime) {
		var time = new Date(UTCtime),
			now = Math.round(new Date() / 1000);

		var diff = (now - time);

		if (diff < 45) {
			return response(0, 1); // few seconds ago
		}

		if (diff < 5400) { // 50min
			return response(Math.round(diff / 60), 2); // an x minutes ago
		}

		if (diff < 79200) { // 22 hours
			return response(Math.round(diff / 3600), 3); // an x hours ago
		}

		if (diff < 2160000) { // 25 days
			return response(Math.round(diff / 86400), 4); // an x days ago
		}

		if (diff < 31104000) { // an x months ago
			return response(Math.round(diff / 1036800), 5); // an x months ago
		}

		return response(0, 6); // over one year ago
	}


	function prepareTranslation(timeData) {
		logger.log('prepareTranslation()', timeData);
		switch (timeData.code) {
			case 1:
				return lang.AN_MOMENT_AGO;
			break;
			case 2:
				if (timeData.value === 1) {
				    return lang.AN_MINUTE_AGO;
				}
				if (timeData.value > 1 && timeData.value < 5) {
				    return timeData.value + ' ' + lang.MINUTES_2_4 + ' ' + lang.AGO;
				}
				return timeData.value + ' ' + lang.MINUTES + ' ' + lang.AGO;
			break;
			case 3:
				if (timeData.value === 1) {
					return lang.AN_HOUR_AGO;
				}
				if (timeData.value > 1 && timeData.value < 5) {
					return timeData.value + ' ' + lang.HOURS_2_4 + ' ' + lang.AGO;
				}
				return timeData.value + ' ' + lang.HOURS + ' ' + lang.AGO;
			break;
			case 4:
				if (timeData.value === 1) {
					return lang.A_DAY_AGO;
				}
				return timeData.value + ' ' + lang.DAYS + ' ' + lang.AGO;
			break;
			case 5:
				if (timeData.value === 1) {
					return lang.A_MONTH_AGO;
				}
				if (timeData.value > 1 && timeData.value < 5) {
					return timeData.value + ' ' + lang.MONTHS_2_4 + ' ' + lang.AGO;
				}
				return timeData.value + ' ' + lang.MONTHS + ' ' + lang.AGO;
			break;
			case 6:
				return lang.A_YEAR_AGO;
		}

	}

	/*
	 * adds padding
	 * @param {string|number} value - value to be padded
	 * @param {string} paddingChar
	 * @param {number} desiredLength - desired total length of parsed value
	 * @return {string} - type is the same as type of value param
	 */
	function addLeftPadding(value, paddingChar, desiredLength) {
		var i,
			l;

		if (typeof value === 'undefined' ||
			typeof paddingChar !== 'string' ||
			typeof desiredLength !== 'number') {
			return logger.error('Missing/icorrect params for addLeftPadding method');
		}

		value += '';

		if (value.length < desiredLength) {
			l = desiredLength - value.length;
			for(i = 0; i < l; i++) {
				value = paddingChar + value;
			}
		}
		return value;
	}

	var formatParser = {
		'YYYY': function (date) {
			return date.getFullYear();
		},
		'MM': function (date) {
			return addLeftPadding(date.getMonth() + 1, '0', 2);
		},
		'DD': function (date) {
			return addLeftPadding(date.getDate(), '0', 2);
		},
		'hh': function (date) {
			return addLeftPadding(date.getHours(), '0', 2);
		},
		mm: function (date) {
			return addLeftPadding(date.getMinutes(), '0', 2);
		},
		ss:  function (date) {
			return addLeftPadding(date.getSeconds(), '0', 2);
		}
	};

	function parseFormat(format, date) {
		logger.log('parseFormat()', format, date);
		if (formatParser.hasOwnProperty(format, date)) {
			return formatParser[format](date)
		} else {
			logger.error('There is no format: ', format);
			return false;
		}
	}

	return {
		getReadableTimePassed: function (time) {
			logger.log('getNumberOfDaysPassed()', time);
			var timeData = getReadableTimePassed(time),
				result = prepareTranslation(timeData);

			logger.log('getNumberOfDaysPassed() result:', result);
			return result;
		},

		/*
		 * @param {number} unix - seconds from 1.01.1970 eg. 1422617594
		 * @param {string} format - output format eg. 'yyyy:mm:dd'
		 * @param {string|undefined} separator - by default is '.'
		 *
		 * available formats:
		 *   yyyy:mm:dd
		 *
		 *   yyyy:mm:dd hh:mm:ss
		 */
		format: function (unix, format, separator) {
			logger.log('format', unix, format);
			var outputParts,
				inputParts,
				outputPartial,
				parsed,
				output = '',
				date,
				i;

			if (typeof separator === 'undefined') {
				separator = '.';
			}

			if (typeof format !== 'string') {
				return logger.error('incorrect parametr type: \'format\' should be string');
			}

			inputParts = format.split(' ');

			date = new Date(unix * 1000);

			for (var j = 0; j < inputParts.length; j++) {
				inputParts[j] = inputParts[j].split(':');
			}

			outputParts = [];

			for(i = 0; i < inputParts.length; i++) {
				outputParts[i] = [];
				for (var k = 0; k < inputParts[i].length; k++) {
					parsed = parseFormat(inputParts[i][k], date);
					if (parsed) {
						outputParts[i].push(parsed);
					} else {
						return '';
					}
				}
			}

			for (var l = 0; l < outputParts.length; l++) {
				outputPartial = outputParts[l].join(separator);
				output += outputPartial;

				if (l < outputParts.length - 1) {
				    output += ' ';
				}
			}

			logger.log('format->output: ', output);
			return output;
		}
	}
});

