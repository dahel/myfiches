define([
	'logger',
    'lang'
],
function (
	Logger,
    lang
) {
	var logger = Logger.getOne('levels');


	var LEVELS = {
        eng: {
            1: lang.LEVELS[1],
            2: lang.LEVELS[2].substr(0, 8) + '.',
            3: lang.LEVELS[3]
        },
        pol: {
            1: lang.LEVELS[1].substr(0, 7) + '.',
            2: lang.LEVELS[2].substr(0, 11) + '.',
            3: lang.LEVELS[3].substr(0, 7) + '.'
        }
    }

	return {
		getLevels: function () {
			return LEVELS[lang.code];
		}
	}
});