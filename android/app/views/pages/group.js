define([
	'App',
	'tpl',
	'app/views/components/confirmPopup',
	'text!templates/pages/group/group.html',
	'app/Page',
	'logger',
	'lang',
	'app/collections/WordsGroups',
	'platform',
	'app/models/User'
],
function (
	app,
	tpl,
	confirmPopup,
	template,
	Page,
	logger,
	lang,
	WordGroupsCollection,
	platform,
	User
) {

	var logger = logger.getOne('page:group');

    function resolveDisableReport(lastTimeReport) {
        if (lastTimeReport === 0 || Math.round(new Date().getTime() / 1000) - lastTimeReport > 86400) {
            return false;
        }
        return true;
    }

	return Page.extend({
		name: 'GROUP',
		className: 'group-page',
		template: tpl.compile(template),

        reportDisabled: false,

		elLearnButton: null,

		model: null,


		events: {
			'click #learn': 'onLearnSelected',
			'click #repeat-learn': 'showRestartPopup',
			'click #browse': 'browseGroup',
			'click #mark': 'onMarkSelected',
			'click #report': 'onReportSelected',
			'click .end-learning .image': 'onEndLearningSelected'
		},

		initialize: function () {
			this._super('initialize');

		},

		render: function (lastTimeReported) {
            this.reportDisabled = resolveDisableReport(lastTimeReported);
			this.el.innerHTML = this.template({
				model: this.prepareModelData(this.model.toJSON()),
                reportDisabled: this.reportDisabled,
				lang: lang,
				isUserOwner: this.model.get('author') === User.get('username')
			});
			this.saveReferences();
		},

		prepareModelData: function (attrs) {
			if (attrs.last_time_learn === '0') {
				attrs.last_time_learn = 'nie rozpoczęto'
			}
			attrs.category = lang.CATEGORIES[attrs.category];
			return attrs;
		},

		generateStyles: function (name) {
			var length = name.length,
				fontSize = 16,
				top = 25;

			if (length < 40 && length > 30) {
				fontSize = 19
				top = 25;
			} else if (length < 31) {
				fontSize = 25;
				top = 20;
			}

			return {
				nameFontSize: fontSize,
				top: top
			}
		},

		saveReferences: function () {
			this.elLearnButton = this.el.querySelector('#learn');
		},

		prepareModel: function (id, callback) {
			this.model = WordGroupsCollection.get(id);
			this.model.fetch({
				success: function () {
                    dao.getLastTimeReport(this.model.id, function (lastTimeReported) {
                        this.updateHeader();
                        this.render(lastTimeReported);
                        callback();
                    }.bind(this));
				}.bind(this)
			});
		},

		updateHeader: function () {
			app.actionBar.updateHeader('group', {
				lang: lang,
				name: this.model.get('name'),
				styles: this.generateStyles(this.model.get('name')),
				model: this.model.toJSON()
			});
		},

		show: function (id) {
			if (app.platform === 'browser') {
				WordGroupsCollection.fetch({
					success: function () {
						this.prepareModel(id, function () {

						}.bind(this));
					}.bind(this)
				})
			} else {
				this.prepareModel(id, function () {

				}.bind(this));
			}
			app.launched();
			this._super('show');
		},

		hide: function () {
			this._super('hide');
		},

		onLearningRestart: function () {
			logger.log('onLearningRestart()');
			platform.showSpinner(null, lang.UPDATING_SET);
//			Loading.show('fullscreen');
			dao.resetLearningResults(this.model.id);
			dao.updateGroupProgress(this.model.id, 0);
			platform.hideSpinner();
//			Loading.hide();
			this.onLearnSelected();

		},

		showRestartPopup: function () {
			logger.log('showRestartPopup()');
			confirmPopup.show({
				message: lang.WANNA_RESTART_LEARNING,
				yesCallback: this.onLearningRestart.bind(this)
			});
		},

		onLearnSelected: function (event) {
			logger.log('onLearnSelected()');
			if (event) {
				event.stopPropagation();
				event.preventDefault();
			}
			app.router.navigate('learn/' + this.model.get('id'), {trigger: true});
		},

		browseGroup: function () {
			logger.log('browseGroup()');
			app.router.navigate('browse/' + this.model.get('id'), {trigger: true});
		},

		onMarkSelected: function () {
			logger.log('onMarkSelected()');
			if (this.model.get('author') === User.get('username')) {
				platform.notify(lang.CANNOT_MARK_YOUR_SET);
				// todo remove
				//app.router.navigate('mark/' + this.model.get('id'), {trigger: true});
			} else if (this.model.get('ff_recommend') === 1 || this.model.get('ff_recommend') === '1') {
                platform.notify(lang.CANNOT_MARK_RECOMMEND_SET);
            } else if (this.model.get('marked')) {
				platform.notify(lang.CANNOT_MARK_AGAIN);
			} else {
				app.router.navigate('mark/' + this.model.get('id'), {trigger: true});
			}
		},

		onReportSelected: function () {
			logger.log('onReportSelected()');
            if (this.reportDisabled) {
                platform.notify(lang.CAN_REPORT_ONCE_PER_DAY);
            } else if (this.model.get('author') === User.get('username')) {
				platform.notify(lang.CANNOT_REPORT_YOUR_SET);
				//app.router.navigate('report/' + this.model.get('id'), {trigger: true});
			} else {
				app.router.navigate('report/' + this.model.get('id'), {trigger: true});
			}
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (confirmPopup.isVisible) {
			    confirmPopup.hide();
			} else {
				history.back();
			}
		},

		endLearning: function () {
			logger.log('endLearning()');
			platform.showSpinner(null, null);
			this.model.stopLearn({
				success: function () {
					platform.hideSpinner(null, null);
					platform.notify(lang.LEARNING_STOPPED);
					app.router.navigate('main', {trigger: true});
				},
				error: function () {
					platform.hideSpinner(null, null);
					platform.notify(lang.CANNOT_LEARNING_STOPPED);
				}
			})
		} ,

		onEndLearningSelected: function () {
			logger.log('onEndLearningSelected()');
			confirmPopup.show({
				message: lang.WANNA_STOP_LEARNING + '?',
				yesCallback: this.endLearning.bind(this)
			});
		}
	});
});