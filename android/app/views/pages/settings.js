define([
	'App',
	'tpl',
	'text!templates/pages/settings/settings.html',
	'app/Page',
	'logger',
	'lang',
	'platform',
	'app/views/components/selectList',
	'app/views/components/langSelectList',
	'jquery',
	'app/models/Settings'
],
function (
	app,
	tpl,
	template,
	Page,
	Logger,
	lang,
	platform,
	SelectList,
	langSelectList,
	$,
	Settings
) {

	var logger = Logger.getOne('page:settings');

	return Page.extend({
		className: 'settings',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,

		appLangSelectList: null,


		events: {
			'click #app-language': 'onAppLanguageSelected',
			'click #native-lang': 'onNativeLanguageSelected',
			'click #learning-lang': 'onLearningLanguageSelected',
			'click #refresh': 'onButtonRefresh'
		},

		initialize: function () {
			this._super('initialize');
			this.appLangSelectList = new SelectList({
				itemTemplate: '<li data-label="{{=it.label}}" data-key="{{=it.key}}"><span class="label">{{=it.label}}</span><span class="lang-icon {{=it.key}}"></span></li>',
				itemsData: [
					{
						label: lang.LANGUAGES['pol'],
						key: 'pol'
					},
					{
						label: lang.LANGUAGES['eng'],
						key: 'eng'
					}
				],
				title: lang.SELECT_LANG
			}).render();
			Settings.on('sync', this.render, this);
		},

		render: function () {
			this.el.innerHTML = this.template({
				lang: lang,
				settings: Settings.toJSON()
			});
			platform.hideSpinner();
		},

		show: function () {
			this.render();
			app.actionBar.updateHeader('settings', {
				lang: lang
			});
			this._super('show');
            Settings.on('change:appLang', function (a) {
				app.launching = true;
                document.location.reload();
            });
			app.launched();
		},

		hide: function () {
			this._super('hide');
		},

		onAppLanguageSelected: function () {
			logger.log('onAppLanguageSelected()');
			this.appLangSelectList.show({
				selectCallback: function (data) {
					Settings.applyChange('appLang', data.key);
				},
                disableItemKey: Settings.get('appLang')
			})
		},

		onNativeLanguageSelected: function () {
			logger.log('onNativeLanguageSelected()');
			langSelectList.show({
				selectCallback: function (data) {
					Settings.applyChange('nativeLang', data.key);
				},
				disableItemKey: Settings.get('learningLang')
			})
		},

		onLearningLanguageSelected: function () {
			logger.log('onLearningLanguageSelected()');
			langSelectList.show({
				selectCallback: function (data) {
					Settings.applyChange('learningLang', data.key);
				},
				disableItemKey: Settings.get('nativeLang')
			})
		},

		onButtonRefresh: function () {
			logger.log('onButtonRefresh()');
			platform.showSpinner();
			Settings.fetch();
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (langSelectList.isVisible) {
				langSelectList.hide();
			} else if (this.appLangSelectList.isVisible) {
				this.appLangSelectList.hide();
			} else {
                app.router.navigate('main', {trigger: true});
			}
		}
	});
});