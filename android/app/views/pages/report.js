define([
	'App',
	'tpl',
	'text!templates/pages/report/report.html',
	'app/collections/WordsGroups',
	'app/models/WordsGroup',
	'app/views/components/infoPopup',
	'app/Page',
	'logger',
	'lang',
	'jquery',
	'platform'
],
function (
	app,
	tpl,
	template,
	WordsGroups,
	WordsGroup,
	InfoPopup,
	Page,
	Logger,
	lang,
	$,
	platform
) {

	var logger = Logger.getOne('page:report');

	return Page.extend({
		className: 'report',
		template: tpl.compile(template),
		groupId: null,

		selection: {
			vulgarism: false,
			invalid_translation: false
		},


		events: {
			'click .checkbox': 'onCheckboxSelect',
			'click #send-footer': 'onSendPressed'
		},

		initialize: function () {
			this._super('initialize');
		},

		render: function () {
			this.el.innerHTML = this.template({
                lang: lang
			});
			//this.saveReferences()
		},


		show: function (args) {
			if (app.platform === 'browser' && !WordsGroups.fetched) {
				WordsGroups.fetch({
					success: this.show.bind(this, args)
				})
			} else {
				this.groupId = args[0];
				this.render();
				this._super('show');
			}
		},

		hide: function () {
			this._super('hide');
            this.selection.vulgarism = false;
            this.selection.invalid_translation = false;
		},

		toggleSelection: function (type) {
			logger.log('toggleSelection()',this.selection[type] !== true);
			this.selection[type] =  this.selection[type] !== true;
		},

		onCheckboxSelect: function (event) {
			logger.log('onCheckboxSelect()', event);
			$(event.currentTarget).toggleClass('selected');
			this.toggleSelection(event.currentTarget.id);
		},

		onSendPressed: function () {
			logger.log('onSendPressed()');
			if (this.selection.vulgarism === false && this.selection.invalid_translation === false) {
				InfoPopup.show({
					templateData: {
						headerMessage: lang.SELECT_TYPE,
						contentMessage: lang.SELECT_VIOLATION_TYPE
					},
					type: InfoPopup.type.INFO
				});
			} else {
				this.sendReport();
			}
		},

		resolveReportType: function () {
			if (this.selection.vulgarism === true && this.selection.invalid_translation === true) {
				return 3;
			}
			if (this.selection.vulgarism === true) {
			    return 1;
			}
			if (this.selection.invalid_translation === true) {
				return 2;
			}
		},

        storeReportEvent: function (groupId) {
            logger.log('storeReportEvent()', groupId);
            dao.storeReportEvent(groupId);
        },

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (InfoPopup.isVisible()) {
				InfoPopup.hide();
				return true;
			}
			history.back();
		},

		sendReport: function () {
			logger.log('sendReport()', this.selectedMark);
			platform.showSpinner(null, null);
            var model = WordsGroups.get(this.groupId);

            if (!model) {
                // znaczy że to search
                model = new WordsGroup({ id: this.groupId});
            }

			model.sendReport({
				reportType: this.resolveReportType(),
				success: function () {
                    this.storeReportEvent(this.groupId);
					platform.hideSpinner();
					platform.notify(lang.GROUP_REPORTED);
					history.back();
                }.bind(this),
				error: function (errorCode) {
					platform.hideSpinner();
                    history.back();
				}
			});
		}
	});
});