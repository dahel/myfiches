define([
	'App',
	'tpl',
	'app/views/components/confirmPopup',
	'text!templates/pages/groupSearch/groupSearch.html',
	'app/Page',
	'logger',
	'lang',
	'app/models/SearchWordsGroup',
    'app/views/components/infoPopup',
	'platform',
	'app/models/User',
	'app/utils/levels',
	'config'
],
function (
	app,
	tpl,
	confirmPopup,
	template,
	Page,
	logger,
	lang,
	SearchWordsGroup,
    infoPopup,
	platform,
	User,
	level,
    config
) {

	var logger = logger.getOne('page:groupSearch');

	return Page.extend({
		className: 'groupSearch',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,


		events: {
			'click #search-browse': 'browseGroup',
			'click #search-download': 'onStartLearnSelected',
			'click #search-report': 'onReportSelected'
		},

		initialize: function () {
			this._super('initialize');

		},

		render: function () {
			this.el.innerHTML = this.template({
				model: this.model.toJSON(),
				lang: lang,
				levels: level.getLevels()
			});
			this.saveReferences();
		},

		generateStyles: function (name) {
			var length = name.length,
				fontSize = 16,
				top = 25;

			if (length < 40 && length > 30) {
				fontSize = 19
				top = 25;
			} else if (length < 31) {
				fontSize = 25;
				top = 20;
			}

			return {
				nameFontSize: fontSize,
				top: top
			}
		},

		saveReferences: function () {
			this.elLearnButton = this.el.querySelector('#learn');
		},

		updateHeader: function (empty) {
			app.actionBar.updateHeader('group', {
				lang: lang,
				name: this.model.get('name'),
				styles: this.generateStyles(this.model.get('name')),
				model: this.model.toJSON()
			})
		},

		show: function (args) {
            logger.log('show()');
			app.actionBar.clearHeader();
			platform.showSpinner();
			this._super('show');
			this.model = new SearchWordsGroup({id: parseInt(args[0], 10)});
			this.model.fetch({
				success: this.onModelFetched.bind(this)
			});
            app.launched();
		},

		onModelFetched: function () {
			logger.log('onModelFetched()', this.model);
			this.updateHeader();
			this.render();

			platform.hideSpinner();
		},

		hide: function () {
			platform.hideSpinner();
			this._super('hide');
		},

		browseGroup: function () {
			logger.log('browseGroup()');
			app.router.navigate('browseSearch/' + this.model.id + '/' + this.model.get('name'), {trigger: true});
		},

		onStartLearnSelected: function () {
			logger.log('onStartLearnSelected()');

			if (this.model.get('isUserLearning')) {
			    platform.notify(lang.ALREADY_LEARNING);
			} else {
				confirmPopup.show({
					message: lang.WANNA_START_LEARN_1 + ': <br/>' +
							' <span style="color: blue">' + this.model.get('lp_cost') + '</span><br/> ' +
							lang.LEARN_POINTS + ' <br/>' +
							lang.WANNA_START_LEARN_2,
					yesCallback: this.startLearning.bind(this)
				});
			}
		},

		startLearning: function () {
			logger.log('startLearning()');
            platform.showSpinner();
            User.getLearnPoints({
                success: function (learnPoints) {
                    if (learnPoints < parseInt(this.model.get('lp_cost'))) {
                        this.showNoEnoughLearnPoints();
                        platform.hideSpinner();
                    } else {
                        this.model.startLearning({
                            success: this.onLearningStarted.bind(this)
                        });
                    }
                }.bind(this),
                error: function () {
                    platform.hideSpinner();
                }
            });
		},

        showNoEnoughLearnPoints: function () {
            infoPopup.show({
                templateData: {
                    headerMessage: lang.LEARN_POINTS_LACK,
                    contentMessage: lang.GO_FOR_LEARN_POINTS_1 + ' ' + '<span class="green-font-dark-color">' + lang.MY_FICHES_COM + '</span>' +
                        '<div>' + lang.GO_FOR_LEARN_POINTS_2 + '</div>'
                },
                type: infoPopup.type.ERROR
            });
        },

		onLearningStarted: function () {
			logger.log('onLearningStarted()');
			User.set('learnPoints', parseInt(User.get('learnPoints'), 10) - parseInt(this.model.get('lp_cost'), 10));
			User.write({
				success: function () {
					platform.hideSpinner();
					this.model.set('isUserLearning', true);
					this.render();
					confirmPopup.show({
						message: lang.SYNCHRONIZATION_NEEDED,
						yesLabel: lang.SYNCHRONIZE,
						noLabel: lang.LATER,
						yesCallback: this.synchronize.bind(this)
					})
				}.bind(this)
			});

		},

		onReportSelected: function () {
			logger.log('onReportSelected()');
			if (this.model.get('isUserOwner')) {
				platform.notify(lang.CANNOT_REPORT_YOUR_SET);
			} else {
				app.router.navigate('report/' + this.model.id, {trigger: true});
			}
		},

		synchronize: function () {
			logger.log('synchronize()');
			app.router.navigate('main', {trigger: true});
			app.trigger('synchronizeNeeded');
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
            if (infoPopup.isVisible()) {
                infoPopup.hide();
                return true;
            }

            if (confirmPopup.isVisible) {
                confirmPopup.hide();
                return true;
            }
            app.router.navigate('search', {trigger: true});
			return true;
		}
	});
});