define([
	'App',
	'tpl',
	'text!templates/pages/browse/browse.html',
	'app/Page',
	'logger',
	'lang',
	'app/collections/GroupExpressions',
	'app/collections/WordsGroups',
	'platform'
],
function (
	app,
	tpl,
	template,
	Page,
	logger,
	lang,
	GroupExpressions,
	WordGroupsCollection,
	platform
) {

	var logger = logger.getOne('page:browse');

	return Page.extend({
		name: 'BROWSE',
		className: 'browse-page',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,


		events: {

		},

		initialize: function () {
			this._super('initialize');

		},

		render: function () {
			this.el.innerHTML = this.template({
				collection: this.collection.toJSON(),
				lang: lang
			});
			platform.hideSpinner();
		},

		isScrolling: false,

		scrollTimeout: null,

		setScrollTimeout: function () {
			if (this.scrollTimeout) {
			    clearTimeout(this.scrollTimeout);
				this.scrollTimeout = null;
			}

			this.scrollTimeout = setTimeout(function () {
				this.isScrolling = false;
			}.bind(this), 500);

		},

		show: function (id) {
			document.body.onscroll = function () {
				this.isScrolling = true;
				this.setScrollTimeout();
			}.bind(this);

			this._super('show');
			this.id = id;
//			platform.showSpinner(null, lang.LOADING_DATA);
			platform.showSpinner(null, null);
			this.collection = new GroupExpressions().setId(id);
			this.collection.fetch({
				type: 'all',
				success: this.render.bind(this)
			});
		},

		hide: function () {
			document.body.onscroll = function () {};
			this._super('hide');
		},

		onKeyBack: function () {
			if (!this.isScrolling) {
				logger.log('onKeyBack()');
				history.back();
			}
		}
	});
});