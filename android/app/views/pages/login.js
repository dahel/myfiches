define([
	'App',
	'tpl',
	'text!templates/pages/login.html',
	'app/Page',
	'logger',
	'app/models/User',
	'lang',
	'app/views/components/footer',
	'app/views/components/langSelectList',
	'app/views/components/input',
	'app/views/components/infoPopup',
	'app/utils/validator',
	'jquery',
	'platform'
],
function (
	app,
	tpl,
	template,
	Page,
	logger,
	User,
	lang,
	Footer,
	langSelectList,
	Input,
	infoPopup,
	Validator,
	$,
	platform
) {

	var logger = logger.getOne('page:main'),

		Mode = {
			LOGIN: 'login',
			REGISTER: 'register'
		},

		currentMode = Mode.LOGIN,

		REGISTER_MODE_CLASS = 'register-mode';

	return Page.extend({
		name: 'welcome',
		className: 'login-page',
		template: tpl.compile(template),
		errorListener: function () {},

		elUsername: null,
		elPassword: null,
		elEmail: null,
		elPasswordRepeat: null,
		elSelectNative: null,
		elSelectLearning: null,

		events: {
			'click #native-lang-input' : 'onNativeLangSelect',
			'click #learning-lang-input' : 'onLearningLangSelect',
			'click #username': 'onUsernameInputSelect',
			'click #password': 'onPasswordInputSelect',
			'click #email': 'onEmailInputSelect',
			'click #password-repeat': 'onPasswordRepeatInputSelect'
		},

		initialize: function () {
			this._super('initialize');
			this.render();
			Footer.on('send', this.onSendPressed, this);
			this.cacheReferences();
			this.addListeners();

			app.actionBar.on('register', function () {
				this.onOptionClick('register');
			}, this);

			app.actionBar.on('login', function () {
				this.onOptionClick('login');
			}, this);
		},

		onUsernameInputSelect: function (event) {
			event.preventDefault();
			Input.show({
				title: lang.USERNAME,
				value: this.elUsername.value,
				callback: function (value) {
					this.elUsername.value = value;
				}.bind(this)
			});
		},

		onPasswordInputSelect: function (event) {
			event.preventDefault();
			Input.show({
				title: lang.PASSWORD,
				value: this.elPassword.value,
				type: 'password',
				callback: function (value) {
					this.elPassword.value = value;
				}.bind(this)
			});
		},

		onEmailInputSelect: function (event) {
			event.preventDefault();
			Input.show({
				title: lang.EMAIL,
				value: this.elEmail.value,
				callback: function (value) {
					this.elEmail.value = value;
				}.bind(this)
			});
		},

		onPasswordRepeatInputSelect: function (event) {
			event.preventDefault();
			Input.show({
				title: lang.REPEAT_PASSWORD,
				value: this.elPasswordRepeat.value,
				type: 'password',
				callback: function (value) {
					this.elPasswordRepeat.value = value;
				}.bind(this)
			});
		},

		cacheReferences: function () {
			this.elUsername = this.el.querySelector('#username');
			this.elPassword = this.el.querySelector('#password');
			this.elEmail = this.el.querySelector('#email');
			this.elPasswordRepeat = this.el.querySelector('#password-repeat');
			this.elSelectNative = this.el.querySelector('#native-lang-input');
			this.elSelectLearning = this.el.querySelector('#learning-lang-input');

			this.elSelectNativeWrapper = this.el.querySelector('#native-lang-input');
			this.elSelectLearningWrapper = this.el.querySelector('#learning-lang-input');
		},

		addListeners: function () {
			this.elUsername.addEventListener('click', this.removeInvalidNotification.bind(this), false);
			this.elPassword.addEventListener('click', this.removeInvalidNotification.bind(this), false);
			this.elEmail.addEventListener('click', this.removeInvalidNotification.bind(this), false);
			this.elPasswordRepeat.addEventListener('click', this.removeInvalidNotification.bind(this), false);
			this.elSelectNativeWrapper.addEventListener('click', this.removeInvalidNotification.bind(this), false);
			this.elSelectLearningWrapper.addEventListener('click', this.removeInvalidNotification.bind(this), false);
		},

		render: function () {
			this.el.innerHTML = this.template({
				userModel: User.toJSON(),
				lang: lang
			});
			this.elErrorMessageWrapper = this.el.querySelector('#error-message-wrapper');
			this.elErrorMessage = this.el.querySelector('#message');
		},

		show: function () {
			this._super('show');
			app.actionBar.updateHeader(
				'login',
				{
					lang: lang,
					showRegisterTab: !User.get('created')
				});
			app.launched();
		},

		hide: function () {
			this.removeInvalidNotification();
			this._super('hide');
		},

		onOptionClick: function (mode) {
			logger.log('onOptionClick()', mode);

			if (mode !== currentMode) {
			    this.switchMode(mode);
			}
		},

		switchMode: function (mode) {
			logger.log('switchMode()', mode);

			if (mode === Mode.LOGIN) {
				$(document.body).removeClass(REGISTER_MODE_CLASS);
			    this.el.className = this.className;
			} else if (mode === Mode.REGISTER) {
				$(document.body).addClass(REGISTER_MODE_CLASS);
			}
			currentMode = mode;
			this.removeInvalidNotification();
			this.clearFields();
		},

		clearFields: function () {
			this.el.querySelector('#username').value = '';
			this.el.querySelector('#email').value = '';
			this.el.querySelector('#password').value = '';
			this.el.querySelector('#password-repeat').value = '';

			this.elSelectNative.innerHTML = lang.NATIVE_LANG_1;
			this.elSelectNative.setAttribute('data-value', '');
			$(this.elSelectNative).removeClass('base-font-dark-color');
			this.elSelectLearning.innerHTML = lang.LEARNING_LANG_1;
			this.elSelectLearning.setAttribute('data-value', '');
			$(this.elSelectLearning).removeClass('base-font-dark-color');
		},

		onSendPressed: function () {
			logger.log('onSendPressed()', currentMode);
			this.validate();
		},

		showErrorMessage: function (message, invalidElement) {
			logger.log('showErrorMessage()', message, invalidElement);
			this.elErrorMessage.innerText = message;
			this.elErrorMessageWrapper.style.display = 'block';
			$(invalidElement).addClass('invalid');
		},

		hideErrorMessage: function () {
			logger.log('hideErrorMessage()');
			this.elErrorMessageWrapper.style.display = 'none';
		},

		removeInvalidNotification: function () {
			var invalidElement = this.el.querySelector('.invalid');
			if (invalidElement) {
			    $(invalidElement).removeClass('invalid');
			}
			this.hideErrorMessage();
		},

		onNativeLangSelect: function (event) {
			logger.log('onNativeLangSelect()');
			event.preventDefault();
			langSelectList.show({
				selectCallback: function (data) {
					$(this.elSelectNative).addClass('base-font-dark-color');
					this.elSelectNative.innerHTML = data.label + '<span class="flag-icon ' + data.key + '"></span>';
					this.elSelectNative.setAttribute('data-value', data.key);
				}.bind(this),
				disableItemKey: this.elSelectLearning.getAttribute('data-value')
			});
		},

		onLearningLangSelect: function () {
			logger.log('onLearningLangSelect()');
			langSelectList.show({
				selectCallback: function (data) {
					$(this.elSelectLearning).addClass('base-font-dark-color');
					this.elSelectLearning.innerHTML = data.label + '<span class="flag-icon ' + data.key + '"></span>';
					this.elSelectLearning.setAttribute('data-value', data.key);
				}.bind(this),
				disableItemKey: this.elSelectNative.getAttribute('data-value')
			});
		},

		validate: function () {
			logger.log('validate()');

			if (!Validator.validate('username', this.elUsername.value).result().valid) {
			    logger.log('validate(). Invalid input:username', this.elUsername, Validator.result());
				this.showErrorMessage(Validator.result().message, this.elUsername);
				return;
			}

			if (currentMode === Mode.REGISTER) {
				if (!Validator.validate('email', this.elEmail.value).result().valid) {
					logger.log('validate(). Invalid input:email', this.elEmail, Validator.result());
					this.showErrorMessage(Validator.result().message, this.elEmail);
					return;
				}
			}

			if (!Validator.validate('password', this.elPassword .value).result().valid) {
				logger.log('validate(). Invalid input:password', this.elPassword , Validator.result());
				this.showErrorMessage(Validator.result().message, this.elPassword);
				return;
			}

			if (currentMode === Mode.REGISTER) {
				if (!Validator.validate('repeatPassword', this.elPasswordRepeat.value, password.value).result().valid) {
					logger.log('validate(). Invalid input:repeatPassword', this.elPasswordRepeat, Validator.result());
					this.showErrorMessage(Validator.result().message, this.elPasswordRepeat);
					return;
				}
			}

			if (currentMode === Mode.REGISTER) {
				if (!this.elSelectNative.getAttribute('data-value')) {
					this.showErrorMessage(lang.SELECT_LANG, this.elSelectNativeWrapper);
					return;
				}
			}

			if (currentMode === Mode.REGISTER) {
				if (!this.elSelectLearning.getAttribute('data-value')) {
					this.showErrorMessage(lang.SELECT_LANG, this.elSelectLearningWrapper);
					return;
				}
			}

			if (currentMode === Mode.LOGIN) {
			    this.login(this.elUsername.value, this.elPassword.value);
			} else if (currentMode === Mode.REGISTER) {
				//this.register('asdfasdf', 'asddf@wp.pl', 'asdf3535', 'asdf3535', 'eng', 'pol');
				this.register(this.elUsername.value, this.elEmail.value, this.elPassword.value, this.elPasswordRepeat.value, this.elSelectNative.getAttribute('data-value'), this.elSelectLearning.getAttribute('data-value'));
			}

		},

		register: function (username, email, password, repeatPassword, nativeLang, learningLang) {
			logger.log('register()', arguments);
			var self = this;
			User.register({
				username: username,
				password: password,
				password_repeat: repeatPassword,
				email: email,
				nativeLanguage: nativeLang,
				learningLanguage: learningLang
			}, {
				success: function () {
					self.switchMode(Mode.LOGIN);
					platform.hideSpinner();
					platform.notify(lang.USER_REGISTERED_LOGIN);
				},
				error: function () {
					platform.hideSpinner();
				}
			})
		},

		login: function (username, password) {
			logger.log('login()', arguments);
			var self = this;
			platform.showSpinner();
			User.login({
				username: username,
				password: password
			}, {
				success: function () {
					self.clearFields();
					platform.notify(lang.USER_LOGGED_IN);
					app.router.navigate('main', {trigger: true});
					platform.hideSpinner();
				},
				error: function () {
					platform.hideSpinner();
				}
			});
		},

		onKeyBack: function () {
			logger.log('onKeyBack()', langSelectList.isVisible, Input.isVisible, infoPopup.isVisible());
			if (langSelectList.isVisible) {
			    langSelectList.hide();
				return;
			}

			if (Input.isVisible) {
				Input.hide();
				return;
			}

			if (infoPopup.isVisible()) {
				infoPopup.hide();
				return;
			}

			logger.log('onKeyBack() Exiting from app');
			navigator.app.exitApp();
		}
	});
});