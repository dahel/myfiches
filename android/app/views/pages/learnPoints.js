define([
	'App',
	'tpl',
	'text!templates/pages/learnPoints/learnPoints.html',
	'app/Page',
	'app/models/User',
	'logger',
	'lang',
	'platform'
],
function (
	app,
	tpl,
	template,
	Page,
	User,
	Logger,
	lang,
	platform
) {

	var logger = Logger.getOne('page:learnPoints');

	return Page.extend({
		className: 'learnPoints-page',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,


		events: {
			'click .icon': 'refreshLearnPoints'
		},

		initialize: function () {
			this._super('initialize');
		},

		render: function (learnPoints) {
			this.el.innerHTML = this.template({
				lang: lang,
				learnPoints: learnPoints
			});
		},

		show: function () {
			app.actionBar.updateHeader('stats', {
				lang: {
					STATS: lang.LEARN_POINTS_1
				}
			});
			this.render(User.get('learnPoints'));
			this._super('show');
			app.launched();
		},

		hide: function () {
			this._super('hide');
		},

		refreshLearnPoints: function () {
			logger.log('refreshLearnPoints()');
			platform.showSpinner();
			User.getLearnPoints({
				success: function (learnPoints) {
					platform.hideSpinner();
					this.render(learnPoints);
				}.bind(this),
				error: function () {
					platform.hideSpinner();
				}
			})
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
            app.router.navigate('main', {trigger: true});
		}
	});
});