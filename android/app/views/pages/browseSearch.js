define([
	'App',
	'tpl',
	'text!templates/pages/browse/browse.html',
	'app/Page',
	'logger',
	'lang',
	'app/collections/SearchGroupExpressions',
	'platform'
],
function (
	app,
	tpl,
	template,
	Page,
	logger,
	lang,
	SearchGroupExpressions,
	platform
) {

	var logger = logger.getOne('page:browseSearch');

	return Page.extend({
		className: 'browseSearch',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,


		events: {

		},

		isScrolling: false,

		scrollTimeout: null,

		setScrollTimeout: function () {
			if (this.scrollTimeout) {
				clearTimeout(this.scrollTimeout);
				this.scrollTimeout = null;
			}

			this.scrollTimeout = setTimeout(function () {
				this.isScrolling = false;
			}.bind(this), 500);

		},

		initialize: function () {
			this._super('initialize');

		},

		render: function () {
//

			this.el.innerHTML = this.template({
				collection: this.collection.toJSON(),
				lang: lang
			});
			platform.hideSpinner();
		},

		generateStyles: function (name) {
			var length = name.length,
				fontSize = 16,
				top = 25;

			if (length < 40 && length > 30) {
				fontSize = 19
				top = 25;
			} else if (length < 31) {
				fontSize = 25;
				top = 20;
			}

			return {
				nameFontSize: fontSize,
				top: top
			}
		},

		show: function (args) {
			document.body.onscroll = function () {
				this.isScrolling = true;
				this.setScrollTimeout();
			}.bind(this);
			var id = args[0],
				modelName = args[1];
			this._super('show');
			platform.showSpinner(null, null);
			app.actionBar.updateHeader('browse', {
				lang: lang,
				name: modelName,
				styles: this.generateStyles(modelName)
			});

//			platform.showSpinner(null, lang.LOADING_DATA);
			this.collection = new SearchGroupExpressions().setId(id);
			this.collection.fetch({
				success: this.render.bind(this),
				data: {
					itemsPerPage: 100,
					offset: 0
				},
				reset: true
			});
		},

		hide: function () {
			document.body.onscroll = function () {};
			this._super('hide');
		},

		onKeyBack: function () {
			if (!this.isScrolling) {
				logger.log('onKeyBack()');
				history.back();
			}
		}
	});
});