define([
	'App',
	'tpl',
	'text!templates/pages/main/main.html',
	'text!templates/pages/main/mainList.html',
	'app/Page',
	'logger',
	'app/models/User',
	'app/models/Settings',
    'app/views/components/infoPopup',
	'lang',
	'platform',
	'app/collections/WordsGroups',
	'app/utils/dateTime',
	'jquery',
	'app/views/components/loading',
	'app/views/components/paginator'
],
function (
	app,
	tpl,
	template,
	mainListTemplate,
	Page,
	logger,
	UserModel,
	Settings,
    infoPopup,
	lang,
	platform,
	WordsGroups,
	dateTime,
	$,
	Loading,
	Paginator

) {

	var logger = logger.getOne('page:main');

	return Page.extend({
		name: 'MAIN',
		className: 'main-page',
		paginator: null,
		template: tpl.compile(template),
		listTemplate: tpl.compile(mainListTemplate),

		events: {
			'click #log-action': 'onLogAction',
			'click #logout': 'logUserOut',
			'click .words-group': 'onOptionsSelected',

			'click .options': 'onOptionsSelected'
//			'click .learn': 'onLearnSelected'
		},

		paginatorOptions: {
			itemsPerPage: 20,
			page: 1
		},

		initialize: function () {
			this._super('initialize');

			this.paginator = new Paginator();
			this.paginator.on('change:page', this.changePage, this);

			app.actionBar.on('synchronize', this.onSyncClick, this);
			app.on('synchronizeNeeded', this.onSyncClick, this);

			this.render();
		},

		render: function () {
			this.el.innerHTML = this.template({
				userModel: UserModel.toJSON(),
				lang: lang
			});
			this.el.querySelector('#paginator-container').appendChild(this.paginator.el);
			this.saveReferences();
		},

		saveReferences: function () {
			this.elLeftMenuDim = this.el.querySelector('.floating-menu-dim');
			this.elLeftMenuContainer = this.el.querySelector('.floating-menu-container');
			this.elCloseMenuOption = this.el.querySelector('.button-back');
			this.elListWrapper = this.el.querySelector('#list-wrapper');
		},

		updatePaginator: function () {
			logger.log('updatePaginator()');
			this.paginator.update(WordsGroups);
		},

		changePage: function (page) {
			logger.log('changePage', page);
			this.paginatorOptions.page = page;
			this.updateList();
		},

		fetchCollection: function () {
			logger.log('fetchColection', this.searchOptions);
			this.collection.fetch({
				data: this.searchOptions,
				reset: true
			});
		},

		show: function () {
			if (!UserModel.get('loggedIn')) {
				logger.log('User not logged in. Navigating to login scene..');
				app.router.navigate('login', {trigger: true});
			} else {
				this.updateList();
				app.actionBar.updateHeader('main');
				this._super('show');
			}
			app.launched();
		},

		hide: function () {
			this._super('hide');
		},

		updateList: function (callback) {
			logger.log('updateList()');
			WordsGroups.fetch({
				success: function () {
					this.renderList();
					this.updatePaginator();
					if (callback) {
					    callback();
					}
				}.bind(this),
				page: this.paginatorOptions.page,
				itemsPerPage: this.paginatorOptions.itemsPerPage,
				reset: true
			})
		},

		renderList: function () {
			logger.log('renderList()', WordsGroups.toJSON());
			
			this.elListWrapper.innerHTML = this.listTemplate({
				collection: WordsGroups.toJSON(),
				settings: Settings.toJSON(),
				lang: lang
			});
		},

		logUserOut: function () {
			logger.log('logUserOut()');
			UserModel.logout({
				success: function () {
					app.router.navigate('login', {trigger: true});
					platform.notify(lang.USER_LOGGED_OUT)
				}
			});
		},

		showLoginPage: function () {
			logger.log('showLoginPage()', app);
			app.router.navigate('login', {trigger: true});
		},

		onSyncClick: function () {
			UserModel.synchronize({
				success: function (syncData) {

                    if (syncData.addedGroups || syncData.addedChangedExpressions || syncData.removedGroups) {
                        this.updateList(function () {
                            Loading.hide();
                            this.onSnchronizationPerformed(syncData);
//                            platform.notify(lang.SYNCHRONIZATION_COMPLETED);
                        }.bind(this))
                    } else {
                        logger.log('onSyncClick(): Nothing changed, data up to date, no need to update list.');
                        platform.notify(lang.DATA_UP_TO_DATE);
                    }
					platform.hideSpinner();
//					Loading.hide();
				}.bind(this)
			})
		},

        onSnchronizationPerformed: function (data) {
            logger.log('onSnchronizationPerformed()', data);
            infoPopup.show({
                templateData: {
                    headerMessage: lang.SYNCHRONIZATION_COMPLETED,
                    contentMessage: (function () {
                        return '<div>' +
                            '<p> ' + lang.SYNC_GROUPS_ADDED + ': <span class="sync-completed-popup-bold">' + data.addedGroups + '</span></p>' +
                            '<p> ' + lang.SYNC_EXPRESSIONS_ADDED_CHANGED + ': <span class="sync-completed-popup-bold">' + data.addedChangedExpressions + '</span></p>' +
                            '<p> ' + lang.SYNC_GROUPS_REMOVED + ': <span class="sync-completed-popup-bold">' + data.removedGroups + '</span></p>' +
                            '</div>'
                    }())
                } ,
                type: infoPopup.type.INFO
            });
        },

		onOptionsSelected: function (event) {
			var groupId = event.currentTarget.getAttribute('data-id');
			if (!groupId) {
				groupId = event.target.parentNode.getAttribute('data-id');
			}
			logger.log('onOptionsSelected()', groupId);
			event.stopPropagation();
			app.router.navigate('group/' + groupId, {trigger: true});
		},

//		onLearnSelected: function (event) {
//			var groupId = event.target.parentNode.getAttribute('data-id');
//			logger.log('onLearnSelected()', groupId);
//		},

		onKeyBack: function () {
			logger.log('onKeyBack() Exiting from app');
			navigator.app.exitApp();
		}
	});
});