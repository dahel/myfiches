define([
	'App',
	'tpl',
	'text!templates/pages/stats/stats.html',
	'app/Page',
	'logger',
	'lang',
	'app/models/Statistics',
	'platform',
    'app/utils/dateTime'
],
function (
	app,
	tpl,
	template,
	Page,
	Logger,
	lang,
	Statistics,
	platform,
    dateTime
) {

	var logger = Logger.getOne('page:stats');

	function addPadding(val, paddingChar, finalLength) {
		val += '';
		for (var i = 0; i < finalLength - val.length; i++) {
		    val = paddingChar + val;
		}
		return val;
	}

	return Page.extend({
		className: 'stats-page',
		template: tpl.compile(template),

		elLearnButton: null,

		model: null,


		events: {

		},

		initialize: function () {
			this._super('initialize');

		},

		render: function () {
			this.el.innerHTML = this.template({
				lang: lang,
				stats: this.generateStats()
			});
		},

		generateStats: function () {
			var stats =  _.extend(
				Statistics.toJSON(),
				this.generateDailyStats(),
				{learningStarted: this.parseLearningStarted()}
			);

			logger.log('generateStats()', stats);
			return stats;
		},

		parseLearningStarted: function () {
            var parsed;
            if (!Statistics.get('learningStarted')) {
                return 0;
            }
            parsed = dateTime.format(Statistics.get('learningStarted'), 'DD:MM:YYYY');

			logger.log('parseLearningStarted()', parsed);
			return parsed;
		},

		generateDailyStats: function () {
			var secondsPassed = Math.round(new Date().getTime() / 1000) - Statistics.get('learningStarted');
			var days = Math.floor(secondsPassed / (60 * 60 * 24)) || 1,
				dailyStats;

			dailyStats =  {
				days: days,
				dailyDisplay: Math.round(Statistics.get('displayedTotal') / days),
				dailyLearn: Math.round(Statistics.get('learnedTotal') / days)
			};

			logger.log('generateDailyStats()', dailyStats);
			return dailyStats;
		},

		show: function () {
			app.actionBar.updateHeader('stats', {
				lang: lang
			});
			platform.showSpinner(null, lang.GATHERING_STATISTICS + '...');
//			Loading.show();
			Statistics.fetch({
				success: function () {
					this.render();
					this._super('show');
					platform.hideSpinner();
//					Loading.hide();
				}.bind(this)
			});
		},

		hide: function () {
			this._super('hide');
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
            app.router.navigate('main', {trigger: true});
		}
	});
});