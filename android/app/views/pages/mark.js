define([
	'App',
	'tpl',
	'text!templates/pages/mark/mark.html',
	'app/collections/WordsGroups',
	'app/views/components/infoPopup',
	'app/Page',
	'logger',
	'lang',
	'platform'
],
function (
	app,
	tpl,
	template,
	WordsGroups,
	infoPopup,
	Page,
	Logger,
	lang,
	platform
) {

	var logger = Logger.getOne('page:mark');

	return Page.extend({
		className: 'mark-page',
		template: tpl.compile(template),
		groupId: null,
		selectedMark: 0,

		elStars: [],
		elDescription: null,

		events: {
			'click .star': 'onStarSelected',
			'click #send-footer': 'onMarkSend'
		},

		initialize: function () {
			this._super('initialize');
		},

		render: function () {
			this.el.innerHTML = this.template({
                lang: lang
			});
			this.saveReferences();
		},

		saveReferences: function () {
			this.elStars = this.el.querySelectorAll('.star');
			this.elDescription = this.el.querySelector('#description');
		},

		show: function (args) {
			this.selectedMark = 0;
			if (app.platform === 'browser' && !WordsGroups.fetched) {
			    WordsGroups.fetch({
					success: this.show.bind(this, args)
				})
			} else {
				this.groupId = args[0];
				this.render();
				this._super('show');
			}
		},

		hide: function () {
			this._super('hide');

		},

		clearStars: function () {
			logger.log('clearStars()');
			for (var i = 0; i < this.elStars.length; i++) {
			    this.elStars[i].className = 'star';
			}
		},

		highlightStars: function (index) {
			for (var i = index; i >= 0; i--) {
			    this.elStars[i].className = 'star highlighted';
			}
		},

		updateDescription: function (index) {
			logger.log('updateDescription', index);
			this.elDescription.innerHTML = lang['MARK_' + index];
		},

		onStarSelected: function (event) {
			logger.log('onStarSelected()', event);
			var index = event.target.getAttribute('data-index');
			this.updateDescription(index);
			this.elDescription.className = 'mark-' + index;
			this.clearStars();
			this.highlightStars(index);
			this.selectedMark = parseInt(index) + 1;
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (infoPopup.isVisible()) {
			    infoPopup.hide();
			} else {
				history.back();
			}
		},

		onMarkSend: function () {
			logger.log('onMarkSend()', this.selectedMark);
			if (!this.selectedMark) {
				infoPopup.show({
					templateData: {
						headerMessage: lang.SELECT_MARK,
						contentMessage: lang.SELECT_MARK_DETAILS
					},
					type: infoPopup.type.INFO
				});
			} else {
				platform.showSpinner();
				WordsGroups.get(this.groupId).sendMark({
					mark: this.selectedMark,
					success: function () {
						platform.hideSpinner();
						platform.notify(lang.SET_MARKED);
						history.back();
					},
					error: function () {
						platform.hideSpinner();
						history.back();
					}
				});
			}

		}
	});
});