define([
	'App',
	'tpl',
	'text!templates/pages/search/search.html',
	'app/Page',
	'logger',
	'lang',
	'app/collections/SearchWordsGroups',
	'app/views/components/paginator',
	'app/views/components/sorter',
	'platform',
	'app/utils/levels',
	'underscore'
],
function (
	app,
	tpl,
	template,
	Page,
	Logger,
	lang,
	SearchWordsGroups,
	Paginator,
	Sorter,
	platform,
	level,
	_
) {

	var logger = Logger.getOne('page:search');

	var scrollHandler;

	return Page.extend({
		className: 'search-page',
		template: tpl.compile(template),
		paginator: null,

		searchOptions: {
			offset: 0,
			itemsPerPage: 30,
			page: 1,
			sortBy: 'markValue',
			sortDirection: 'desc',
			category: '',
			author: ''
		},

		events: {
			'click .options': 'showGroupDetails',
			'click .start-learn': 'onStartLearnSelected'
		},

		initialize: function () {
			this._super('initialize');
			app.actionBar.on('search:options', this.onSearchOptionsSelected, this);
			this.collection = new SearchWordsGroups();
			this.collection.on('sync', this.render, this);

			this.paginator = new Paginator();
			this.paginator.on('change:page', this.changePage, this);

			this.sorter = new Sorter();
			this.sorter.on('sort', this.applySorterData, this);
			this.sorter.on('reset', this.resetSearchOptions, this);
		},

		resetSearchOptions: function () {
			logger.log('resetSearchOptions()');
			this.searchOptions = {
				offset: 0,
				itemsPerPage: 30,
				page: 1,
				sortBy: 'markValue',
				sortDirection: 'desc',
				category: '',
				author: ''
			}
		},

		render: function () {
			logger.log('render()');
			this.el.innerHTML = this.template({
				collection: this.collection.toJSON(),
                page: this.searchOptions.page,
                itemsPerPage: this.searchOptions.itemsPerPage,
				lang: lang,
				levels: level.getLevels()
			});
			this.el.querySelector('#paginator-container').appendChild(this.paginator.el);
			this.updatePaginator();
			window.scrollTo(0, 0);
			platform.hideSpinner();
		},

		applySorterData: function (sorterData) {
			logger.log('applySorterData()', sorterData);
			_.extend(this.searchOptions, sorterData);
			this.fetchCollection();
		},

		updatePaginator: function () {
			logger.log('updatePaginator()');
			this.paginator.update(this.collection);
		},

		changePage: function (page) {
			logger.log('changePage', page);
			this.searchOptions.page = page;
			this.fetchCollection();
		},

		fetchCollection: function () {
			logger.log('fetchColection', this.searchOptions);
			platform.showSpinner(null, null);
			this.collection.fetch({
				data: this.searchOptions,
				reset: true
			});
		},

		show: function () {
			logger.log('show()');
            var reset = arguments[0][0];
            if (reset) {
				this.sorter.reset();
            }
			app.actionBar.updateHeader('search', {
				lang: lang
			});

//			Loading.show('page');
			if (reset) {
				this.fetchCollection();
			}

			this._super('show');
            app.launched();
		},

		remove: function () {
			window.removeEventListener('scroll', scrollHandler, false);
		},

		hide: function () {
			window.removeEventListener('scroll', scrollHandler, false);
			this._super('hide');
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			
			if (this.sorter.isVisible) {
			    this.sorter.onKeyBack();
			} else {
				app.router.navigate('main', {trigger: true});
			}
		},

		onSearchOptionsSelected: function () {
			logger.log('onSearchOptionsSelected()');
			this.sorter.show();
		},

		showGroupDetails: function () {
			var id = event.target.parentNode.getAttribute('data-id');
			logger.log('showGroupDetails()', id);
			app.router.navigate('groupSearch/' + id, {trigger: true});
		},

		onStartLearnSelected: function (event) {
			var id = event.target.parentNode.getAttribute('data-id');
			logger.log('onStartLearnSelected()', id);
		}
	});
});