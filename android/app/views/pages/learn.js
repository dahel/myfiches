define([
	'App',
	'tpl',
	'app/collections/GroupExpressions',
	'text!templates/pages/learn/learn.html',
	'app/Page',
	'logger',
	'lang',
	'app/collections/WordsGroups',
	'app/views/components/infoPopup',
	'platform',
	'app/models/User',
	'app/models/Settings',
	'jquery'
],
function (
	app,
	tpl,
	GroupExpressions,
	template,
	Page,
	logger,
	lang,
	WordsGroups,
	infoPopup,
	platform,
	User,
	Settings,
	$
) {

	var logger = logger.getOne('page:learn');

	function onUserTap() {}

	return Page.extend({
		name: 'LEARN',
		className: 'learn-page',
		template: tpl.compile(template),
		groupModel: null,
		mode: 'question', // can be also answer

		currentWordModel: null,

		collection: null,

		elQuestion: null,
		elAnswer: null,
		elUsageExample: null,
		elUsageExampleWrapper: null,
		elLearnedWordsCounter: null,
		elProgressBar: null,

		initialWordsNumber: 0,

//		displayedExpressionsCounter: 0,

		progress: 0,

		events: {
			'click .button': 'onButtonSelected',
			'click .switch-lang': 'switchLang'
//			'click #show-answer': 'showAnswer',
//			'click #buttons-container': 'showAnswer'
		},

		initialize: function () {
			this._super('initialize');
		},

		render: function () {
			this.groupModel = WordsGroups.get(this.modelId);
			this.currentWordModel = this.collection.at(0);
			this.calculateProgress();
			this.el.innerHTML = this.template({
				groupModel: this.groupModel.toJSON(),
				learned: this.getLearnedWordsNumber(),
				learnInReverse: Settings.get('learnInReverse'),
				lang: lang
			});
			this.saveReferences();
			this.updateView(this.currentWordModel.toJSON());
			platform.hideSpinner();
		},

		getLearnedWordsNumber: function () {
			return parseInt(this.groupModel.get('length'), 10) - this.collection.length;
		},

		saveReferences: function () {
			this.elQuestion = this.el.querySelector('#question-value');
			this.elAnswer = this.el.querySelector('#answer-value');
			this.elUsageExample = this.el.querySelector('#usage-example-value');
			this.elUsageExampleWrapper = this.el.querySelector('#usage-example');
			this.elLearnedWordsCounter = this.el.querySelector('#learned-words-counter');
			this.elProgressBar = this.el.querySelector('#progress');
		},

		show: function (args) {
			var self = this;
			this._super('show');
//			this.displayedExpressionsCounter = 1;
			platform.showSpinner(null, lang.PREPARING_GROUP_LEARNING + '...');
//			Loading.show();

			dao.notifyLearningStart(args[0]);
			app.actionBar.updateHeader('learn');
			this.createCollection(args[0]);
			this.addListeners();
			app.trigger('learningStart');

			onUserTap = function () {
				if (self.mode === 'question') {
				    self.showAnswer();
				}
			};

			document.body.addEventListener('click', onUserTap, false);
		},

		switchLang: function (event) {
			logger.log('switchLang()');
			event.stopPropagation();
			platform.showSpinner();
			Settings.set('learnInReverse', !Settings.get('learnInReverse'));
			Settings.write({
				success: function () {
					this.render();
				}.bind(this)
			});
		},

		clearFields: function () {
			this.elQuestion.innerHTML = '';
			this.elAnswer.innerHTML = '';
			this.elUsageExample.innerHTML = '';
		},

		hide: function () {
			this._super('hide');
			this.mode = 'question';
			$(this.el).removeClass('answer-mode');
			this.removeListeners();
//			app.trigger('learningEnd', this.displayedExpressionsCounter);
			document.body.removeEventListener('click', onUserTap, false);
			this.clearFields();
			dao.notifyLearningStart(this.groupModel.get('id'));
		},

		addListeners: function ( ){

		},

		removeListeners: function ( ) {

		},


		createCollection: function (id) {
			logger.log('createCollection()', id);
			var self = this;
			this.modelId = id;
			this.collection = new GroupExpressions().setId(id);
			this.collection.fetch({
				type: 'learning',
				success: self.onCollectionFetched.bind(this)
			});
		},

		onCollectionFetched: function () {
			logger.log('onCollectionFetched()',this.collection);
//			Loading.hide();
			if (app.platform === 'android') {
			    this.render();
			} else {
				WordsGroups.fetch({
					success: this.render.bind(this)
				})
			}
		},

		showAnswer: function () {
			logger.log('showAnswer()');
			if (this.mode = 'question') {
				$(this.el).addClass('answer-mode');
				this.mode = 'answer';
			}
		},


		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (infoPopup.isVisible()) {
				infoPopup.hide();
			} else {
				history.back();
			}
		},

		saveWordMark: function (wordMark) {
			logger.log('saveWordMark()');
			this.currentWordModel.set('mark', wordMark);
			dao.saveWordMark(this.modelId, this.currentWordModel.toJSON());
		},

		calculateProgress: function () {
			var progress = Math.round(
				(this.getLearnedWordsNumber() * 100) / parseInt(this.groupModel.get('length'), 10)
			);
			this.progress = progress;
		},

		adjustFontSize: function (text) {
			if (text.length > 80) {
			    return '18px';
			} else if (text.length > 60) {
				return '20px';
			} else  if (text.length > 40) {
				return '22px';
			} else if (text.length > 20) {
				return '24px'
			} else {
				return '30px';
			}
		},

		updateView: function (model) {
			logger.log('updateView', model);
			var question, answer;
			if (Settings.get('learnInReverse')) {
			    question = model.translated;
				answer = model.origin;
			} else {
				question = model.origin;
				answer = model.translated;
			}
			this.elQuestion.style.fontSize = this.adjustFontSize(question);
			this.elQuestion.innerHTML =question;
			this.elAnswer.style.fontSize = this.adjustFontSize(answer);
			this.elAnswer.innerHTML = answer;
			if (model.usage_example) {
				this.elUsageExampleWrapper.style.visibility = 'visible';
				this.elUsageExample.innerHTML = model.usage_example;
			} else {
				this.elUsageExampleWrapper.style.visibility = 'hidden';
			}
						//this.elUsageExample.innerHTML = 'Ala loves her cat very mush. All she want to do is to carry him very caretully aslkd ajksdfj ;alskdfja df ajsf ';
			this.elLearnedWordsCounter.innerHTML = this.getLearnedWordsNumber();
			this.updateProgressBar();
			User.incrementDisplayedExppressionsCounter();
		},

		updateProgressBar: function () {
			var progress = this.progress - 2;

			if (progress < 0) {
			    progress = 0;
			}
			this.elProgressBar.style.width = progress + '%';
		},

		showNextWord: function () {
			logger.log('showNextWord()');
			if (this.collection.length) {
				$(this.el).removeClass('answer-mode');
			    this.currentWordModel = this.collection.at(0);
				this.updateView(this.currentWordModel.toJSON());
//				User.incrementDisplayedExppressionsCounter();
//				this.displayedExpressionsCounter++;
			} else {
				this.updateProgressBar();
				this.showLearningEndPopup();
			}
			this.mode = 'question';
		},

		showLearningEndPopup: function () {
			logger.log('showLearningEndPopup()');
			infoPopup.show({
				templateData: {
					headerMessage: lang.ENDED,
					contentMessage: lang.LEARNING_ENDED
				} ,
				type: infoPopup.type.INFO,
				callback: function () {
					history.back()
				}
			});
		},

		onButtonSelected: function (event) {
			var mark = parseInt(event.target.parentNode.getAttribute('data-value'), 10);
			event.stopPropagation();
			logger.log('onButtonSelected()', mark);
			this.saveWordMark(mark);
			this.collection.remove(this.currentWordModel);
			if (mark === 2) {
				this.calculateProgress();
				dao.updateGroupProgress(this.modelId, this.progress);
			} else {
				this.collection.add(this.currentWordModel);
			}
			this.showNextWord(mark);
		}
	});
});