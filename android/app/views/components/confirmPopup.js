define([
	'App',
	'backbone',
	'tpl',
	'text!templates/components/confirmPopup.html',
	'lang'
],
function (
	app,
	Backbone,
	tpl,
	template,
	lang
) {

	var ConfirmPopup =  Backbone.View.extend({
		className: 'confirm-popup-container',

		type: {
			INFO: 'info',
			ERROR: 'error'
		},

		isInDOM: false,

		elPopupBody: null,

        isVisible: false,

		template: tpl.compile(template),

		initialize: function () {

		},

		yesCallback: function () {},
		noCallback: function () {},

		events: {
			'click .button-ok': 'onOkButton',
			'click .button-cancel': 'onCancelButton'

		},

		show: function (data) {
			/*
			* ConfirmPopup.show({
					 message: app.lang.Server['001'],
					 yesLabel: 'rozpocznij',
					 noLabel: 'anuluj',
					 yesCallback: function () {},
					 noCallback: function () {}
				 });
			* */

				this.el.innerHTML = this.template({
				header: lang.ARE_U_SURE,
				message: data.message,
				yesLabel: data.yesLabel || lang.OK,
				noLabel: data.noLabel || lang.CANCEL,
				lang: lang
			});
			this.el.style.display = '';

			if (!this.isInDOM) {
			    document.body.appendChild(this.el);
			}

			if (data.yesCallback) {
			    this.yesCallback = data.yesCallback;
			}

			if (data.noCallback) {
				this.noCallback = data.noCallback;
			}

			this.positionToCenter();
            this.isVisible = true;
		},

		hide: function () {
			this.el.style.display = 'none';
            this.isVisible = false;
		},

		positionToCenter: function () {
			this.elPopupBody = this.el.querySelector('.confirm-popup');



			var windowWidth = window.innerWidth,
				windowHeight = window.innerHeight,
				popupWidth = this.elPopupBody.offsetWidth,
				popupHeight = this.elPopupBody.offsetHeight,
				left,
				top;


			left = (windowWidth - popupWidth) / 2;
			top = (windowHeight - popupHeight) / 2;
			this.elPopupBody.style.left = left + 'px';
			this.elPopupBody.style.top = top + 'px';
		},

		onOkButton: function (event) {
			event.stopPropagation();
			this.hide();
			this.yesCallback();
		},

		onCancelButton: function () {
			this.hide();
			this.noCallback();
		}
	});

	return new ConfirmPopup();
});