define([
	'backbone',
	'tpl',
	'text!templates/components/input.html',
	'logger'
], function (
	Backbone,
	tpl,
	template,
	Logger
) {

	var logger = Logger.getOne('logger');

	var Input = Backbone.View.extend({

		id: 'input-component',

		elTitle: null,

		elInput: null,

		isVisible: false,

		types: {
			password: 'password',
			text: 'text'
		},

		events: {
			'click #button-ok': 'onButtonOk'
		},

		initialize: function () {
			this.el.innerHTML = tpl.compile(template)();
			this.elTitle = this.el.querySelector('#input-title');
			this.elInput = this.el.querySelector('input');
		},

		show: function (options) {
			logger.log('show()');
			if (this.isVisible) {
				logger.error('input component is already visible!');
				return;
			}
			this.isVisible = true;
			this.elTitle.innerHTML = options.title;
			this.elInput.type = options.type || 'text';
			this.elInput.value = options.value || '';
			this.callback = options.callback || function () {};
			document.body.appendChild(this.el);

			this.elInput.focus();
		},

		hide: function () {
			logger.log('hide()');
			try {
				document.body.removeChild(this.el);
			} catch (e) {}
			this.isVisible = false;
		},

		onButtonOk: function () {
			logger.log('onButtonOk()', this.elInput.value);
			this.hide();
			this.callback(this.elInput.value);
		}
	});

	return new Input();

})