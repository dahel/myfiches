define([
	'backbone',
	'lang',
	'tpl',
	'text!templates/components/actionBar/actionBar.html',
	'text!templates/components/actionBar/headers/main.html',
	'text!templates/components/actionBar/headers/login.html',
	'text!templates/components/actionBar/headers/group.html',
	'text!templates/components/actionBar/headers/browse.html',
	'text!templates/components/actionBar/headers/settings.html',
	'text!templates/components/actionBar/headers/stats.html',
	'text!templates/components/actionBar/headers/search.html',
	'logger',
	'jquery'
],
function (
	Backbone,
	lang,
	tpl,
	template,
	mainHeaderTemplate,
	loginHeaderTemplate,
	groupHeaderTemplate,
	browseHeaderTemplate,
	settingsHeaderTemplate,
	statsHeaderTemplate,
	searchHeaderTemplate,
	logger,
	$
) {

	var logger = logger.getOne('actionBar');

	return function (app) {


		var ActionBar =  Backbone.View.extend({
			el: '.action-bar',

			isMenuVisible: false,

			elLeftMenuDim: null,
			elLeftMenuContainer: null,
			elCloseMenuOption: null,
			elCustonHeaderArea: null,
			elActionBarWrapper: null,

			template: tpl.compile(template),

			headerTemplates: {
				main: tpl.compile(mainHeaderTemplate),
				login: tpl.compile(loginHeaderTemplate),
				group: tpl.compile(groupHeaderTemplate),
				browse: tpl.compile(browseHeaderTemplate),
				settings: tpl.compile(settingsHeaderTemplate),
				stats: tpl.compile(statsHeaderTemplate),
				search: tpl.compile(searchHeaderTemplate)
			},

			events: {
				'click .show-menu': 'showMenu',
				'click .button-back': 'hideMenu',
				'click .floating-menu-dim': 'hideMenu',
				'click .action-button': 'onActionButtonPressed',
				'click #logout': 'logout',
				'click #user-stats': 'showUserStats',
				'click #settings': 'showSettings',
				'click #search': 'showSearch',
				'click #learn-points': 'showLearnPoints',
				'click #current-sets': 'showMain',
				'click #search-options': 'onSearchOptionsSelected'
			},

			render: function () {
                var User = require('app/models/User');
                this.el.innerHTML = this.template({
                    lang: lang,
                    username: User.get('username')
                });
                this.saveReferences();
			},

			saveReferences: function () {
				this.elLeftMenuDim = this.el.querySelector('.floating-menu-dim');
				this.elLeftMenuContainer = this.el.querySelector('.floating-menu-container');
				this.elCloseMenuOption = this.el.querySelector('.button-back');
				this.elCustonHeaderArea = this.el.querySelector('#custom-header-area');
				this.elActionBarWrapper = document.querySelector('.action-bar-wrapper');
			},

			initialize: function () {
				logger.log('Initialize()');
				this.render();
				app.router.on('route', this.onSceneChange, this);
                require('app/models/User').on('change:loggedIn', function (User) {
                    if (User.get('loggedIn')) {
                        this.render();
                    }
                }, this);
			},

			showMenu: function () {
				if (!this.isMenuVisible) {
					logger.log('showMenu()');
					$(this.elLeftMenuContainer).addClass('active');
					this.isMenuVisible = true;
					this.selectActiveItem();
				}
			},

			selectActiveItem: function () {
				var currentActive = this.elLeftMenuContainer.querySelector('.item-' + Backbone.history.fragment.split('/')[0]);
				var previousActive = this.elLeftMenuContainer.querySelector('.active');
				if (currentActive && previousActive && (currentActive === previousActive)) {
					return;
				}

				if (previousActive) {
					previousActive.classList.remove('active');
				}
				if (currentActive) {
					currentActive.classList.add('active');
				}
			},

			hideMenu: function (callback) {
				logger.log('hideMenu()');
				if (this.isMenuVisible) {
					if (app.lowPerformanceDevice) {
						$(this.elLeftMenuContainer).removeClass('active');
					} else {
						$(this.elCloseMenuOption).addClass('pre-hide');
						setTimeout(function () {
							$(this.elLeftMenuContainer).removeClass('active');
							setTimeout(function () {
								$(this.elCloseMenuOption).removeClass('pre-hide');
								if (typeof callback === 'function') {
								    callback();
								}
							}.bind(this), 200);
						}.bind(this), 150);
					}
				}
				this.isMenuVisible = false;
//			this.elLeftMenuContainer.className = '';
			},

			updateHeader: function (templateName, data) {
				logger.log('updateHeader()', templateName, data);
				if (!this.headerTemplates.hasOwnProperty(templateName)) {
				    this.hide();
					return;
				}
				this.show();
				this.elCustonHeaderArea.innerHTML =
					this.headerTemplates[templateName](data);
			},

			clearHeader: function () {
				this.elCustonHeaderArea.innerHTML = '';
			},

			hide: function () {
				this.elActionBarWrapper.className = 'action-bar-wrapper hidden';
			},

			show: function () {
				this.elActionBarWrapper.className = 'action-bar-wrapper';
			},

			onSceneChange: function (href) {
				//var sceneName = Backbone.history.fragment.split('/')[0];
				logger.log('onSceneChange()');
				this.hideMenu();
			},

			onActionButtonPressed: function (event) {
				var action = event.currentTarget.getAttribute('data-action');
				logger.log('onActionButtonPressed()', action);
				if (!action) {
				    logger.error('No action for button with action-button class !!');
				}
				this.trigger(action);
			},

			logout: function () {
				logger.log('logout()');
				require('app/models/User').logout({
					success: function () {
						require('platform').notify(lang.USER_LOGGED_OUT);
						app.router.navigate('login', {trigger: true});
					}
				});
			},

			showUserStats: function () {
				logger.log('showUserStats()');
				this.hideMenu(function () {
					if (Backbone.history.fragment === 'stats') {
						return;
					}
					app.router.navigate('stats', {trigger: true});
				});
			},

			showSettings: function () {
				logger.log('showSettings()');
				this.hideMenu(function () {
					if (Backbone.history.fragment === 'settings') {
						return;
					}
					app.router.navigate('settings', {trigger: true});
				});
			},

			showSearch: function () {
				logger.log('showSearch()');
				this.hideMenu(function () {
					if (Backbone.history.fragment.split('/')[0] === 'search') {
						return;
					}
					app.router.navigate('search/reset', {trigger: true});
				});
			},

			showLearnPoints: function () {
				logger.log('showLearnPoints()');
				this.hideMenu(function () {
					if (Backbone.history.fragment === 'learnPoints') {
						return;
					}
					app.router.navigate('learnPoints', {trigger: true});
				});
			},

			showMain: function () {
				logger.log('showLearnPoints()');
				this.hideMenu(function () {
					if (Backbone.history.fragment === 'main') {
						return;
					}
					app.router.navigate('main', {trigger: true});
				});
			},

			onSearchOptionsSelected: function () {
				logger.log('onSearchOptionsSelected()');
				this.trigger('search:options');
				this.hideMenu();
			}
		});

		return new ActionBar();
	};
});