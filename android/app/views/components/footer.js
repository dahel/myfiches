define([
	'backbone',
	'lang',
	'logger'
],
function (
	Backbone,
	lang,
	logger
) {

	var logger = logger.getOne('Footer');

	var Footer =  Backbone.View.extend({
		el: '.common-footer',

		events: {
			'click #footer-button-send': 'onSendPressed'
		},

		render: function () {
			this.el.innerHTML = '<div id="footer-button-send">' + lang.SEND + '</div>';
		},

		initialize: function () {
			this.render();
		},

		onSendPressed: function () {
			logger.log('onSendPressed()');
			this.trigger('send');
		}
	});

	return new Footer();
});