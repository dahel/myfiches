define([
	'backbone',
	'logger',
	'jquery'
], function (
	Backbone,
	Logger,
	$
) {

	var logger = Logger.getOne('loading'),

		LOADING_WIDTH = 40,

		LOADING_HEIGHT = 40,

		wrapper,
		isVisible = false,
		initialized = false,
		initialize = function () {
			wrapper = document.createElement('div');

			var	dim = document.createElement('div'),
				loadingImg = document.createElement('div');



			wrapper.id = 'loading-wrapper';

			loadingImg.innerHTML =
				'<div class="spinner">' +
					'<div class="double-bounce1"></div>' +
					'<div class="double-bounce2"></div>' +
					'<div class="double-bounce3"></div>' +
					'</div>'

			dim.className = 'global-popup-dim';
			loadingImg.id = 'loading-image';

			loadingImg.style.width = LOADING_WIDTH + 'px';
			loadingImg.style.height = LOADING_HEIGHT + 'px';
			loadingImg.style.top = ((window.innerHeight - LOADING_HEIGHT) / 2) + 'px';
			loadingImg.style.left = ((window.innerWidth - LOADING_WIDTH) / 2) + 'px';


			wrapper.appendChild(dim);
			wrapper.appendChild(loadingImg);
		};

	return {

		/*
		* @param {String} mode - 'page' (action bar visible) or 'fullscreen'
		* */
		show: function (mode) {
			logger.log('show()', mode);
			if (!initialized) {
			    initialize();
			}

			if (!mode || mode === 'page') {
				$(wrapper).removeClass('fullscreen');
				$(wrapper).addClass('page');
			} else {
				$(wrapper).addClass('fullscreen');
				$(wrapper).removeClass('page');
			}

			if (!isVisible) {
				document.body.appendChild(wrapper);
			}
			isVisible = true;
		},

		hide: function () {
			logger.log('hide()');
			if (isVisible) {
				try {
					document.body.removeChild(wrapper);
				} catch(e){}

				isVisible = false;
			}

		}
	}
});