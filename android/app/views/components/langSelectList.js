define([
	'app/views/components/selectList',
	'text!templates/components/langSelectList/langSelectList.html',
	'lang'
], function (
	SelectList,
	itemTemplate,
	lang
) {

	function prepareItemsData() {
		var itemsData = [],
			item,
            alphabet = '*!@_.()#^&%-=+01234567989aąbcćdeęfghijklłmnńoóprsśtuwvyzźż';

		for (var langCode in lang.LANGUAGES) {
			if (lang.LANGUAGES.hasOwnProperty(langCode)) {
				item = {};
				item.label = lang.LANGUAGES[langCode];
				item.key = langCode;
				itemsData.push(item);
			}
		}

        itemsData.sort(function (a, b) {
            var compare = function (index) {
                var index_a = alphabet.indexOf(a.label[index].toLowerCase()),
                    index_b = alphabet.indexOf(b.label[index].toLowerCase());

                if (index_a == index_b) {
                    return compare(++index);
                } else {
                    return index_a - index_b;
                }
            };
            return compare(0);
        });
		return itemsData;
	}
	return new SelectList({
		itemTemplate: itemTemplate,

		itemsData: prepareItemsData(),

		title: lang.SELECT_LANG
	}).render();
});