define([
	'App',
	'lang',
	'tpl',
	'text!templates/components/sorter.html',
	'app/views/components/selectList',
	'app/views/components/infoPopup',
	'app/views/components/input',
	'logger'
], function (
	app,
	lang,
	tpl,
	sorterTemplate,
	SelectList,
	infoPopup,
	Input,
	Logger
) {

	var logger = Logger.getOne('sorter');

	return Backbone.View.extend({
		id: 'sorter',

		template: tpl.template(sorterTemplate),

		isVisible: false,

		selectList: null,
		input: null,

		elSortBy: null,
		elSortDirection: null,
		elFilterName: null,
		elFilterValue: null,

		sortFields: [
			{
				key: 'markValue',
				label: lang.MARK_VALUE
			},
			{
				key: 'length',
				label: lang.NUMBER_OF_EXPRESSIONS
			},
			{
				key: 'downloaded',
				label: lang.DOWNLOADS_NUMBER
			}
		],

		sortDirections: [
			{
				key: 'asc',
				label: lang.ASCENDING
			},
			{
				key: 'desc',
				label: lang.DESCENDING
			}
		],
		
		filterFields: [
			{
				key: 'category',
				label: lang.CATEGORY
			},
			{
				key: 'author',
				label: lang.AUTHOR
			},
			{
				key: 'level',
				label: lang.LEVEL
			}
		],

		categoriesFields: [],

		levelsFields: [],

		sortData: {
			sortBy: 'markValue',
			sortDirection: 'desc'
		},

		filter: {
			name: null,
			value: null
		},

		events: {
			'click #sorter-sort-by': 'selectSortByField',
			'click #sorter-sort-direction': 'selectSortDirection',
			'click #sorter-filter-by': 'selectFilterField',
			'click #sorter-filter-value': 'selectFilterValue',
			'click #search-action': 'onSearchActionSelected',
			'click #reset-to-default-action': 'onResetActionSelected',
			'click .global-popup-dim': 'hide'
		},

		onResetActionSelected: function () {
			logger.log('onResetActionSelected()');
			this.reset();
			this.onSearchActionSelected();
		},

		reset: function () {
			this.filter.name = null;
			this.filter.value = null;
			this.sortData.sortBy = 'markValue';
			this.sortData.sortDirection = 'desc';
			this.render();
			this.trigger('reset');
		},

		render: function () {
			this.el.innerHTML = this.template({
				lang: lang
			});

			this.saveReferences();
		},

		saveReferences: function () {
			this.elSortBy = this.el.querySelector('#sorter-sort-by .setting-value');
			this.elSortDirection = this.el.querySelector('#sorter-sort-direction .setting-value');
			this.elFilterName = this.el.querySelector('#sorter-filter-by .setting-value');
			this.elFilterValue = this.el.querySelector('#sorter-filter-value .setting-value');
		},

		initialize: function () {
			this.el.style.display = 'none';
			this.render();
			document.body.appendChild(this.el);

			this.selectList = new SelectList({
				itemTemplate: '<li data-label="{{=it.label}}" data-key="{{=it.key}}">{{=it.label}}</li>'
			});

			this.generateCategoriesFields();
			this.generateLevelsFields();
		},

		generateCategoriesFields: function () {
			logger.log('generateCategoriesFields()');
			for (var categoryCode in lang.CATEGORIES) {
				if (lang.CATEGORIES.hasOwnProperty(categoryCode)) {
				    this.categoriesFields.push({
						key: categoryCode,
						label: lang.CATEGORIES[categoryCode]
					});
				}
			}
		},

		generateLevelsFields: function () {
			logger.log('generateLevelsFields()');
			for (var levelCode in lang.LEVELS) {
				if (lang.LEVELS.hasOwnProperty(levelCode)) {
					this.levelsFields.push({
						key: levelCode,
						label: lang.LEVELS[levelCode]
					});
				}
			}
		},

		show: function () {
			logger.log('show()');
			this.el.style.display = 'block';
			this.positionToCenter();
			this.isVisible = true;
			app.disableScrolling();
		},

		hide: function () {
			logger.log('hide()');
			this.el.style.display = 'none';
			this.isVisible = false;
			app.enableScrolling();
		},

		positionToCenter: function () {
			this.elPopupBody = this.el.querySelector('#sorter-popup-wrapper');

			var windowWidth = window.innerWidth,
				windowHeight = window.innerHeight,
				popupWidth = this.elPopupBody.offsetWidth,
				popupHeight = this.elPopupBody.offsetHeight,
				left,
				top;

            var scrollTop = window.scrollY;


			left = (windowWidth - popupWidth) / 2;
			top = ((windowHeight - popupHeight) / 2) + scrollTop;

			this.elPopupBody.style.left = left + 'px';
			this.elPopupBody.style.top = top + 'px';
		},

		setSortByField: function (data) {
			logger.log('setSortByField()', data);
			this.sortData.sortBy = data.key;
			this.elSortBy.innerHTML = data.label;
		},

		selectSortByField: function () {
			logger.log('selectSortByField()');
			this.selectList.update({
				title: lang.SORT_BY,
				itemsData: this.sortFields
			}).show({
				selectCallback: this.setSortByField.bind(this),
				disableItemKey: this.sortData.sortBy
			});
		},

		setSortDirection: function (data) {
			logger.log('setSortDirection()', data);
			this.sortData.sortDirection = data.key;
			this.elSortDirection.innerHTML = data.label;
		},

		selectSortDirection: function () {
			logger.log('selectSortDirection()');
			this.selectList.update({
				title: lang.SORT_DIRECTION,
				itemsData: this.sortDirections
			}).show({
				selectCallback: this.setSortDirection.bind(this),
				disableItemKey: this.sortData.sortDirection
			});
		},

		setFilterField: function (data) {
			logger.log('setFilterField()');
			if (this.filter.name !== data.key) {
				this.elFilterValue.innerHTML = '';
				this.filter.value = '';
			}
			this.filter.name = data.key;
			this.elFilterName.innerHTML = data.label;
		},

		selectFilterField: function () {
			logger.log('selectFilterField()');
			this.selectList.update({
				title: lang.FIELD,
				itemsData: this.filterFields
			}).show({
				selectCallback: this.setFilterField.bind(this),
				disableItemKey: this.filter.name
			});
		},

		selectFilterValue: function () {
			logger.log('selectFilterValue()');
			if (this.filter.name === 'author') {
				Input.show({
					title: 'Input author name',
					value: this.elFilterValue.innerHTML,
					type: 'text',
					callback: function (value) {
						this.elFilterValue.innerHTML = value;
						this.filter.value = value;
					}.bind(this)
				});
			} else if (this.filter.name === 'category') {
				this.selectList.update({
					title: lang.SELECT_CATEGORY,
					itemsData: this.categoriesFields
				}).show({
					selectCallback: function (data) {
						this.elFilterValue.innerHTML = data.label;
						this.filter.value = data.key;
					}.bind(this),
					disableItemKey: this.filter.name
				});
			} else if (this.filter.name === 'level') {
				this.selectList.update({
					title: lang.SELECT_LEVEL,
					itemsData: this.levelsFields
				}).show({
					selectCallback: function (data) {
						this.elFilterValue.innerHTML = data.label;
						this.filter.value = data.key;
					}.bind(this),
					disableItemKey: this.filter.name
				});
			} else {
				infoPopup.show({
					templateData: {
						headerMessage: lang.NO_FILTER_FIELD_SELECTED,
						contentMessage: lang.SELECT_FILTER_FIELD
					} ,
					type: infoPopup.type.ERROR
				});
			}
		},

		onSearchActionSelected: function () {
			logger.log('onSortActionSelected()', this.sortData);
			var data = {
				sortBy: this.sortData.sortBy,
				sortDirection: this.sortData.sortDirection
			};

			if (this.filter.value) {
				if (this.filter.name === 'category') {
					data.category = this.filter.value;
				} else if (this.filter.name === 'author') {
					data.author = this.filter.value;
				} else if (this.filter.name === 'level') {
					data.level = this.filter.value;
				}
			}

			this.hide();
			this.trigger('sort', data);
		},

		triggerSortAction: function () {
			logger.log('triggerSortAction()');

		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (infoPopup.isVisible()) {
			    infoPopup.hide();
				return;
			}
			if (this.selectList.isVisible) {
			    this.selectList.hide();
			} else {
				this.hide();
			}
		}
	})
})