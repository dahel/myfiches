define([
	'backbone',
	'tpl',
	'text!templates/components/infoPopup.html',
	'lang'
],
function (
	Backbone,
	tpl,
	template,
	lang
) {

    function calculateHeaderFontSize(string) {
        var strLength = string.length;
        if (strLength < 25) {
            return 24
        }

        if (strLength < 35) {
            return 20
        }

        if (strLength < 45) {
            return 18
        }

        return 16
    }
	var InfoPopup =  Backbone.View.extend({
		className: 'info-popup-container',

		type: {
			INFO: 'info',
			ERROR: 'error'
		},

		isInDOM: false,

		visible: false,

		elPopupBody: null,

		template: tpl.compile(template),

		initialize: function () {

		},

		callback: function () {

		},

		events: {
			'click .button-ok': 'hide'
		},

		show: function (data) {
			/*
			* InfoPopup.show({
					 type: InfoPopup.types.ERROR,
					 headerMessage: app.lang.Server['001'],
					 contentMessage: app.lang.Server[errorCode]
				 });
			* */
			var templateData = this.prepareTemplateData(data.templateData);
			this.el.innerHTML = this.template({
                templateData: templateData,
                headerFontSize: calculateHeaderFontSize(templateData.headerMessage)
            });
			this.el.style.display = '';
			this.el.className = this.className + ' ' + data.type;

			if (!this.isInDOM) {
			    document.body.appendChild(this.el);
			}

			if (data.callback) {
			    this.callback = data.callback;
			}

			this.positionToCenter();
			this.visible = true;
		},

		hide: function () {
			this.el.style.display = 'none';
			this.callback();
			this.visible = false;
		},

		isVisible: function () {
			return this.visible;
		},

		prepareTemplateData: function (data) {
			var templateData = {};

			if (typeof data === 'string') {
			    templateData.headerMessage = lang.ERROR_OCCURED;
				templateData.contentMessage = data;
				return templateData;
			} else {
				return data;
			}
		},

		positionToCenter: function () {
			this.elPopupBody = this.el.querySelector('.info-popup');

			var windowWidth = window.innerWidth,
				windowHeight = window.innerHeight,
				popupWidth = this.elPopupBody.offsetWidth,
				popupHeight = this.elPopupBody.offsetHeight,
				left,
				top;


			left = (windowWidth - popupWidth) / 2;
			top = (windowHeight - popupHeight) / 2;
			this.elPopupBody.style.left = left + 'px';
			this.elPopupBody.style.top = top + 'px';
		}
	});

	return new InfoPopup();
});