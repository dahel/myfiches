define([
	'backbone',
	'text!templates/components/paginator.html',
	'logger',
    'lang',
	'tpl'
],
function (
	Backbone,
	template,
	Logger,
    lang,
	tpl
) {

	var logger = Logger.getOne('Paginator');

	return  Backbone.View.extend({
		className: 'paginator',

		totalLength: null,

		numberOfPages: null,

		currentPage: null,

		itemsPerPage: null,

		maxPageLinks: 5,

		links: [],

		template: tpl.compile(template),


		initialize: function (options) {

		},

		update: function (collection) {
			if (typeof collection.totalLength === 'undefined') {
				console.error('no totalLength for paginator !!!');
				return;
			}

			if (typeof collection.page === 'undefined') {
				console.error('no page for paginator !!!');
				return;
			}

			if (typeof collection.itemsPerPage === 'undefined') {
				console.error('no itemsPerPage for paginator !!!');
				return;
			}

			this.totalLength = collection.totalLength;
			this.currentPage = collection.page;
			this.itemsPerPage = collection.itemsPerPage;

			this.render();
		},


		events: {
			'click .page-link': 'changePage',
			'click .next-page': 'nextPage',
			'click .previous-page': 'previousPage'
		},

		calculate: function () {
			this.numberOfPages = Math.ceil(this.totalLength / (this.itemsPerPage));
			this.paginate = this.itemsPerPage < this.totalLength;

			if (this.paginate) {
				this.links = this.getPageLinks();
			}

		},

		getPageLinks: function () {
			logger.log('getPagesLinks()');
			var link = 0,
				links = [];

			if (this.currentPage < 4 || this.numberOfPages <= this.maxPageLinks) {
				for (link = 0; link < this.maxPageLinks; link++) {
					if (link + 1 > this.numberOfPages) {
						break;
					}
					links.push(link + 1)
				}
			} else {
				link = this.currentPage;

				if (link + 3 >= this.numberOfPages) {
					//debugger;
					var j = this.numberOfPages - this.maxPageLinks;

					//debugger;
					for (var i = j + 1; i < this.numberOfPages + 1; i++) {
						links.push(i);
					}
				} else {
					for (var i = 1; i < this.maxPageLinks / 2; i++) {

						links.push(link - i);
						links.push(link + i);

						if (links.length === Math.floor(this.maxPageLinks / 2)) {
							links.push(link)
						}
					}
				}
			}

			return links.sort(function (a, b) {
				return a - b;
			});
		},

		render: function () {
			this.calculate();
			this.el.innerHTML = this.template({
				links: this.links,
				currentPage: this.currentPage,
				numberOfPages: this.numberOfPages,
				paginate: this.paginate,
                lang: lang
			})
		},

		show: function (data) {

		},

		hide: function () {

		},

		changePage: function (event) {
			logger.log('changePage()', event);
			var page;

			if (event.target.className.indexOf('current-page') > -1) {
				return;
			} else {
				page = event.target.getAttribute('data-page');
			}
			this.trigger('change:page', parseInt(page, 10));
		},

		nextPage: function (event) {
			logger.log('nextPage()', event);
			if (this.currentPage < this.numberOfPages) {
				this.trigger('change:page', this.currentPage + 1);
			}
		},

		previousPage: function (event) {
			logger.log('previousPage()', event);
			if (this.currentPage > 1) {
				this.trigger('change:page', this.currentPage - 1);
			}
		}
	});
});