define([
	'backbone',
	'App',
	'logger',
	'text!templates/components/selectListTemplate.html',
	'tpl'
], function (
	Backbone,
	app,
	Logger,
	selectListTemplate,
	tpl
) {
	var logger = Logger.getOne('SelectList');

	return Backbone.View.extend({
		className: 'select-list',

		elList: null,

		elTitle: null,

		elPopup: null,

		itemTemplate: null,

		template: tpl.compile(selectListTemplate),

		itemsData: null,

		title: null,

		itemHeight: 30,

		isVisible: false,

		nativeScroll: false,

		startPosition: 0,

		currentScroll: 0,

		selectCallback: null,

		events: {
			'click li': 'onSelection',
//			'touchmove .list': 'onTouchMove',
//			'touchstart .list': 'onTouchStart',
//			'touchend .list': 'onTouchEnd',
			'click .global-popup-dim': 'hide'
		},

		onTouchStart: function (event) {
			if (this.nativeScroll) {
			    return;
			}
			this.initialTopPosition = Math.abs(parseInt(this.elList.style.top, 10) || 0);
			logger.log('onTouchStart() this.initialTopPosition:', this.initialTopPosition);
		},

		onTouchEnd: function (event) {
			if (this.nativeScroll) {
				return;
			}
			this.startPosition = 0;
			logger.log('onTouchEnd()', event);
		},

		onTouchMove: function (event) {
			if (this.nativeScroll) {
				return;
			}
			logger.log('onTouchMove', event.originalEvent.touches[0].clientY);
			var moveValue,
				direction;
			if (this.startPosition === 0) {
			    this.startPosition = event.originalEvent.touches[0].clientY;
				return;
			}

			moveValue = event.originalEvent.touches[0].clientY - this.startPosition;

			if (moveValue !== 0) {
				direction = moveValue > 0 ? 'up' : 'down';
				this['scroll' + direction](moveValue);
			} else {
				logger.log('moveValue is 0, no scrolling..');
			}

		},

		scrollup: function (value) {
			var scrollValue = this.initialTopPosition - Math.abs(value);
			logger.log('scrollup', scrollValue);

			if (this.initialTopPosition === 0) {
			    logger.log('top = 0, no scroll applied');
				return;
			}

			if (scrollValue <= 0) {
			    scrollValue = 0;
			}

			this.elList.style.top = '-' + scrollValue + 'px';

		},

		scrolldown: function (value) {
			var scrollValue = Math.abs(value) + this.initialTopPosition;
			logger.log('scrolldown', scrollValue);

			if (this.initialTopPosition >= this.scrollLength) {
			    logger.log('scrolled to down, no scroll applied');
				return;
			}

			if (scrollValue  >= this.scrollLength) {
				scrollValue = this.scrollLength;
			}
			this.elList.style.top = '-' + scrollValue + 'px';

		},


		initialize: function (options) {
			window.list = this;
			if (!options.itemTemplate) {
			    logger.warn("you have to pass template")
			}

			if (!options.itemsData) {
			    logger.warn("you have to pass itemsData")
			}

			if (!options.title) {
			    logger.warn('cannot resolve title!')
			}

			this.el.style.display = 'none';
			this.itemTemplate = tpl.compile(options.itemTemplate);
			this.itemsData = options.itemsData;
			this.title = options.title;
			return this;
		},

		render: function () {
			this.el.innerHTML = this.template();

			this.saveReferences();

			var html = '';

			for (var i = 0; i < this.itemsData.length; i++) {
			    html += this.itemTemplate(this.itemsData[i]);
			}

			this.elList.innerHTML = html;
			this.elTitle.innerHTML = this.title;
			return this;
		},

		update: function (options) {
			if (!options.itemTemplate) {
				logger.warn("you have to pass template")
			} else {
				this.itemTemplate = options.itemTemplate;
			}

			if (!options.itemsData) {
				logger.warn("you have to pass itemsData")
			} else {
				this.itemsData = options.itemsData;
			}

			if (!options.title) {
				logger.warn('cannot resolve title!')
			} else {
				this.title = options.title;
			}

			return this.render();
		},

		applyNativeScroll: function () {
			logger.log('applyNativeScroll()');

			this.elList.style.height = (this.elPopup.offsetHeight - this.elTitle.offsetHeight) + 'px';
            if ((this.elPopup.offsetHeight - this.elTitle.offsetHeight) < 300) {
                this.elList.style.overflowY = 'hidden';
            } else {
                this.elList.style.overflowY = 'scroll';
            }

		},

		saveReferences: function () {
			this.elList = this.el.querySelector('.list');
			this.elPopup = this.el.querySelector('.select-list-content');
			this.elTitle = this.el.querySelector('.select-list-title');
		},

		show: function (options) {
			logger.log('show()', options);

			if (!options.selectCallback) {
			    logger.error('no select callback!');
			} else {
				this.selectCallback = options.selectCallback;
			}

			if (!this.isVisible) {
				document.body.appendChild(this.el);
				this.el.style.display = 'block';
				this.isVisible = true;
			} else {
				logger.warn('you trying to show already visible element');
			}

			this.applyNativeScroll();
//			if (parseInt(('' + app.androidVersion)[0], 10) < 3) {
//				this.applyNativeScroll();
//				this.nativeScroll = true;
//			}

			this.positionToCenter();
			this.calculateScrollLength();

			if (options.disableItemKey) {
			    this.disableItemByKey(options.disableItemKey)
			}
			return this;
		},

		disableItemByKey: function (key) {
			logger.log('disableItemByKey', key);
			this.disabled = this.el.querySelector('li[data-key=' + key + ']');
			if (this.disabled) {
				this.disabled.className = 'disabled';
			}
		},

		hide: function () {
			logger.log('hide()');
			if (this.isVisible) {
				document.body.removeChild(this.el);
				if (this.disabled) {
					this.disabled.className = '';
					this.disabled = null;
				}

				this.el.style.display = 'none';
				this.isVisible = false;
			} else {
				logger.warn('you trying to hide already hidden element');
			}
			return this;
		},

		calculateScrollLength: function () {
			this.scrollLength = this.elList.offsetHeight + this.elTitle.offsetHeight - this.elPopup.offsetHeight;
		},

		positionToCenter: function () {
			var windowWidth = window.innerWidth,
				windowHeight = window.innerHeight,
				popupWidth = this.elPopup.offsetWidth,
				popupHeight = this.elPopup.offsetHeight,
				left,
				top;

//			debugger;
			left = (windowWidth - popupWidth) / 2;
			top = (windowHeight - popupHeight) / 2;
			this.elPopup.style.left = left + 'px';
			this.elPopup.style.top = top + 'px';
		},

		onSelection: function (event) {
			logger.log('onSelection', event.target);
			var target = event.target,
				data = {};

			if (!target.hasAttribute('data-key')) {
				target = target.parentNode;
			}

			if (target.className.indexOf('disabled') > -1) {
			    logger.warn('Selected disable item');
				return;
			}

			data.label = target.getAttribute('data-label');
			data.key = target.getAttribute('data-key');

			if (!data.label) {
			    logger.warn('Cannot resolve label!')
			}

			if (!data.label) {
				logger.warn('Cannot resolve label!')
			}

			this.hide();
			this.selectCallback(data);
		}
	});
});