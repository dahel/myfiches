define([
	'backbone'
],
function (
	Backbone
) {

	return Backbone.View.extend({

		initialize: function () {
		},

		show: function () {
			this.beforeShow();
			this.el.style.display = '';
			this.afterShow();
		},

		hide: function () {
			this.beforeHide();
			this.el.style.display = 'none';
			this.afterHide();
		},

		beforeHide: function () {
		},

		afterHide: function () {
		},

		beforeShow: function () {
		},

		afterShow: function () {
		}
	})
});