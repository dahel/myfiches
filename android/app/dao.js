define([
	'underscore',
	'logger',
	'platform',
	'app/utils/dateTime',
	'async'
], function (
	_,
	Logger,
	platform,
	dateTime,
	async
) {
	var logger = Logger.getOne('DAO');

	var DATABASE_NAME 			= 'liq_database_03',
		DATABASE_VERSION 		= '0.3',
		DATABASE_DESCRIPTION 	= 'Learn it quickly app database';

	var TABLE_WORDS_GROUPS_NAME = 'words_groups',
		TABLE_WORDS_GROUPS 		= 	{
			C_ID				:	'id',
			C_NAME				:	'name',
			C_AUTHOR			:	'author',
			C_LENGTH			:	'length',
			C_PROGRESS			:	'progress',
			C_CREATED			:	'created',
			C_MARK_COUNTER		:	'mark_counter',
			C_MARK_VALUE		:	'mark_value',
			C_DESCRIPTION		:	'description',
			C_CATEGORY			:	'category',
			C_LAST_TIME_LEARN	:	'last_time_learn',
			C_DOWNLOADED		:	'downloaded',
			C_DOWNLOAD_DATE		:	'download_date',
			C_FF_RECOMMEND	    :	'ff_recommend',
			C_ORIGIN_LANG		:	'origin_lang',
			C_TRANSLATION_LANG	:	'translation_lang',
			C_MARKED			: 	'marked',
			C_LEVEL				: 	'level'
		},
		CREATE_TABLE_WORDS_GROUPS =
			'CREATE TABLE if not exists ' +
			TABLE_WORDS_GROUPS_NAME + ' (' +
			TABLE_WORDS_GROUPS.C_ID + ' INTEGER PRIMARY KEY, ' +
			TABLE_WORDS_GROUPS.C_NAME + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_AUTHOR + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_LENGTH + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_PROGRESS + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_CREATED + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_MARK_COUNTER + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_MARK_VALUE + ' FLOAT, ' +
			TABLE_WORDS_GROUPS.C_DESCRIPTION + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_CATEGORY + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_LAST_TIME_LEARN + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_ORIGIN_LANG + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_TRANSLATION_LANG + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_MARKED + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_LEVEL + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_FF_RECOMMEND + ' INTEGER, ' +
			TABLE_WORDS_GROUPS.C_DOWNLOAD_DATE + ' TEXT, ' +
			TABLE_WORDS_GROUPS.C_DOWNLOADED + ' INTEGER );';


	var TABLE_WORDS_GROUP_NAME = 'words_group_',
		TABLE_WORDS_GROUP 		= 	{
			C_ID			:	'id',
			C_ORIGIN		:	'origin',
			C_TRANSLATED	:	'translated',
			C_USAGE_EXAMPLE:	'usage_example',
			C_MARK			:	'mark'
		},
		getWordsGroupTableQuery = function (id) {
			return 'create table if not exists ' +
					TABLE_WORDS_GROUP_NAME + id + ' (' +
					TABLE_WORDS_GROUP.C_ID + ' INTEGER PRIMARY KEY, ' +
					TABLE_WORDS_GROUP.C_ORIGIN + ' TEXT, ' +
					TABLE_WORDS_GROUP.C_TRANSLATED + ' TEXT, ' +
					TABLE_WORDS_GROUP.C_USAGE_EXAMPLE + ' TEXT, ' +
					TABLE_WORDS_GROUP.C_MARK + ' INT);';
		};

	// tabela do przechowywania id'ków wyrażeń, których użytkownik już się nauczył
	var TABLE_LEARNED_EXPRESSIONS_NAME = 'learned_expressions',
		TABLE_LEARNED_EXPRESSIONS = {
			C_ID: 'id' // id to połączone id zestawu z id wyrażenia
		},
		CREATE_LEARNED_EXPRESSIONS =
			'create table if not exists ' +
				TABLE_LEARNED_EXPRESSIONS_NAME + ' (' +
				TABLE_LEARNED_EXPRESSIONS.C_ID + ' INTEGER PRIMARY KEY);';

    var TABLE_REPORTS_NAME = 'reports',
        TABLE_REPORTS	= 	{
            C_ID			:	'id',
            C_GROUP_ID		:	'group_id',
            C_LAST_REPORTED	:	'last_reported'
        },
        CREATE_TABLE_REPORTS =
            'create table if not exists ' +
                TABLE_REPORTS_NAME + ' (' +
                TABLE_REPORTS.C_ID + ' INTEGER PRIMARY KEY, ' +
                TABLE_REPORTS.C_GROUP_ID + ' INTEGER, ' +
                TABLE_REPORTS.C_LAST_REPORTED + ' INTEGER );';

	/*
	* **************************************************************************************
	* ******************************* GETTERS **********************************************
	* **************************************************************************************
	*/

	function getLearningGroupsIds(callback) {
		logger.log('getLearningGroupsIds()');
		var resultData = [],
			query;

		query = 'select id from ' +
				TABLE_WORDS_GROUPS_NAME;

		runQuery(query, function (result) {
			var i,
				length = result.rows.length;
			for (i = 0; i < length; i++) {
				resultData.push(result.rows.item(i).id);
			}
			callback(resultData);
		});
	}

	function getWordsGroups(options) {
		logger.log('getWordsGroups()', options);
		var items = options.itemsPerPage || 50,
			page = options.page || 1,
			offset = (page - 1) * items;

		getNumberOfWordsGroups(function (error, count) {
			var wordsGroups = [],
				query =
					'select * from ' +
						TABLE_WORDS_GROUPS_NAME +
						' order by ' +
						TABLE_WORDS_GROUPS.C_LAST_TIME_LEARN +
						' desc' +
						' limit ' + items +
						' offset ' + offset;

			runQuery(query, function (result) {
				var i,
					length = result.rows.length;
				for (i = 0; i < length; i++) {
					wordsGroups.push(result.rows.item(i));
				}
				options.success({
					itemsPerPage: items,
					page: page,
					count: count,
					collection: wordsGroups
				});
			});
		});


	}

    function getLastTimeReport(groupId, callback) {
        logger.log('getLastTimeReport', groupId);
        var query = 'select ' + TABLE_REPORTS.C_LAST_REPORTED +' from ' + TABLE_REPORTS_NAME +' where ' + TABLE_REPORTS.C_GROUP_ID + '=' + groupId;
        runQuery(query, function (result) {
            var lt;
            if (result.rows.length) {
                lt = result.rows.item(0).last_reported;
            } else {
                lt = 0;
            }
            callback(lt);
            logger.log('getLastTimeReport, result:', lt);
        });
    }

	function getWordsGroup(id, callback) {
		logger.log('getWordsGroup()', id);
		var wordsGroups = [],
			query =
				'select * from ' +
				TABLE_WORDS_GROUPS_NAME +
				' where id=' + id;

		runQuery(query, function (result) {
			callback(result.rows.item(0));
		});
	}

	function getGroupExpressionsForLearning(id, callback) {
		logger.log('getGroupExpressionsForLearning()');
		var expressions = [],
			query = 'select * from ' +
			TABLE_WORDS_GROUP_NAME + id +
			' where mark != 2 ' +
			' order by mark ASC';

		runQuery(query, function (result) {
			var i,
				length = result.rows.length;
			for (i = 0; i < length; i++) {
				expressions.push(result.rows.item(i));
			}
			callback(expressions);
		})
	}

	function getGroupExpressions(id, callback) {
		logger.log('getGroupExpressions()');
		var expressions = [],
			query = 'select * from ' +
				TABLE_WORDS_GROUP_NAME + id;

		runQuery(query, function (result) {
			var i,
				length = result.rows.length;
			for (i = 0; i < length; i++) {
				expressions.push(result.rows.item(i));
			}
			callback(expressions);
		})
	}

	// pobiera liczbę zapamiętanych wyrażeń
	function getLearnedExpressionsNumber(callback) {
		logger.log('getLearnedExpressionsNumber');
		var query =
			'select count(*) from ' +
			TABLE_LEARNED_EXPRESSIONS_NAME;

		runQuery(query, function (result) {
			callback(null, result.rows.item(0)['count(*)']);
		});
	}

	function getStatsData(callback) {
		logger.log('getStatsData()');
		async.parallel({
			learnedTotal: getLearnedExpressionsNumber
		}, function (error, result) {
			logger.log('getStatsData():result', result);
			callback(result);
		});
	}

	function getNumberOfWordsGroups(callback) {
		logger.log('getNumberOfWordsGroups');
		var query =
			'select count(*) from ' +
				TABLE_WORDS_GROUPS_NAME;

		runQuery(query, function (result) {
			callback(null, result.rows.item(0)['count(*)']);
		});
	}

	function getTotalNumberOfExpressions(callback) {
		logger.log('getTotalNumberOfExpressions');
		var query =
			'select sum(length) from ' +
				TABLE_WORDS_GROUPS_NAME;

		runQuery(query, function (result) {
			callback(null, result.rows.item(0)['sum(length)']);
		});
	}


	/*
	 * **************************************************************************************
	 * ******************************* SETTERS **********************************************
	 * **************************************************************************************
	 */

	function removeWord(groupId, wordId) {
		var query =
			'delete from ' +
			TABLE_WORDS_GROUP_NAME + groupId +
			' where id=' +
			wordId;

		runQuery(query);
	}

	function insertWord(groupId, wordData) {
		var query =
			'insert or replace into ' +
			TABLE_WORDS_GROUP_NAME + groupId +
			' (' +
			TABLE_WORDS_GROUP.C_ID + ', ' +
			TABLE_WORDS_GROUP.C_ORIGIN + ', ' +
			TABLE_WORDS_GROUP.C_TRANSLATED + ', ' +
			TABLE_WORDS_GROUP.C_USAGE_EXAMPLE + ', ' +
			TABLE_WORDS_GROUP.C_MARK + ')' +
			' values (' +
			'"' + wordData.id + '", ' +
			'"' + wordData.origin + '", ' +
			'"' + wordData.translated + '", ' +
			'"' + wordData.usage_example + '", ' +
			+ wordData.mark + ')';

		runQuery(query);
	}

	function notifyLearningStart(groupId) {
		logger.log('notifyLearningStart()', groupId);
		var query =
			'update ' +
			TABLE_WORDS_GROUPS_NAME +
			' set ' +
			TABLE_WORDS_GROUPS.C_LAST_TIME_LEARN
			+ ' = '
			+ Math.round(new Date().getTime() / 1000) +
			' where id=' +
			groupId;
		runQuery(query);
	}

	// for saving while learning
	function saveWordMark(groupId, wordData) {
		logger.log('saveWordMark()', groupId, wordData);
		removeWord(groupId, wordData.id);
		insertWord(groupId, wordData);

		if (wordData.mark === 2) { // jeśli użytkownik nauczył się słówka zapiszmy jego id aby później móc zsumować postęp
			saveWordStatData(groupId, wordData);
		}
	}

	function updateGroupProgress(groupId, progress) {
		logger.log('updateGroupProgress()', groupId, progress);
		var query =
			'update ' +
			TABLE_WORDS_GROUPS_NAME +
			' set progress=' +
			+ progress +
			' where id=' +
			groupId;
		runQuery(query);
	}

	function resetLearningResults(groupId) {
		logger.log('resetLearningResults()', groupId);
		var query =
			'update ' +
			TABLE_WORDS_GROUP_NAME + groupId +
			' set mark=0';

		runQuery(query);
	}

	// kiedy użytkownik oceni zestaw
	function setWordsGroupMarked(groupId) {
		logger.log('setWordsGroupMarked()', groupId);
		var query =
			'update ' +
			TABLE_WORDS_GROUPS_NAME +
			' set ' +
			TABLE_WORDS_GROUPS.C_MARKED +
			' = 1 ' +
			'where id =' + groupId;
		runQuery(query);
	}

	// zapisuje mark dla danego wyrażenia, żeby uniknąć duplikatów jako id
	// stosuje się groupId + '' + wordId
	function saveWordStatData(groupId, wordData) {
		logger.log('saveWordStatData', groupId, wordData);
		var id = groupId + '' + wordData.id;
		var query =
			'insert or replace into ' +
				TABLE_LEARNED_EXPRESSIONS_NAME +
				'(' +
				TABLE_LEARNED_EXPRESSIONS.C_ID + ') ' +
				'values (' +
				id + ')';
		runQuery(query);
	}


	function openDatabase() {
		return platform.openDatabase(DATABASE_NAME, DATABASE_VERSION, DATABASE_DESCRIPTION);
	}

	function runQuery(query, callback) {
		logger.log('runQuery()', query);
		var db = openDatabase();
		db.transaction(
			function (tx) {
				tx.executeSql(
					query,
					[],
					function (tx, result) {
						logger.log('query result', result);
						if (callback) {
							callback(result);
						}
					},
					function onError(transaction, error) {
						logger.error('runQuery() ERROR', error);
                        logger.error('query: ', query);
					}
				);
			},
			function onError(transaction, error) {
				logger.error('runQuery() ERROR', error);
			}
		);
	}

	function createWordsGroupTable(id) {
		logger.log('createWordsGroupTable()', id);
		runQuery(getWordsGroupTableQuery(id));
	}

	function removeWordsGroupsFromMainTable(id) {
		logger.log('deleteWordsGroup()', id);
		var query =
			'delete from ' +
				TABLE_WORDS_GROUPS_NAME +
				' where ' +
				TABLE_WORDS_GROUP.C_ID + ' = ' + id;

		runQuery(query);
	}

	function removeGroupExpressionsTable(id) {
		logger.log('removeGroupExpressionsTable()', id);
		var query =
			'drop table ' +
			TABLE_WORDS_GROUP_NAME + id;
		runQuery(query);
	}

	function deleteWordsGroup(id, callback) {
		logger.log('deleteWordsGroup()', id);
		removeWordsGroupsFromMainTable(id);
		removeGroupExpressionsTable(id);
		if (callback) {
		    callback();
		}
	}

	function createInitialTables() {
		logger.log('createInitialTables()');
		runQuery(CREATE_TABLE_WORDS_GROUPS);
		runQuery(CREATE_LEARNED_EXPRESSIONS);
        runQuery(CREATE_TABLE_REPORTS);
	}

	/*
	 * **************************************************************************************
	 * ******************************* SYNCHRONIZATION **********************************************
	 * **************************************************************************************
	 */

	/*
	* Usuwa grupy, których użytkownik przstał się uczyć,
	* @param {array} learningIds tablica stringów-idków wszytkich zestawów których użytkownik
	* 	aktualnie się uczy
	* */
	function syncRemovedGroups (learningIds, callback) {
		logger.log('syncRemovedGroups()', learningIds);
		getLearningGroupsIds(function (currentGroupsIds) {
			var i,
				length = currentGroupsIds.length,
				removedCount = 0;

			for (i = 0; i < length; i++) {
			    if (learningIds.indexOf(currentGroupsIds[i] + '') < 0) {
					logger.warn('Will remove group from currently being lerned ', currentGroupsIds[i]);
					deleteWordsGroup(currentGroupsIds[i]);
                    removedCount++;
			    }
			}
			callback(null, removedCount);
		});
	}

	/*
	*	Dodaje zestaw do bazy
	**/
	function addWordsGroup(groupData) {
		logger.log('addWordsGroup()', groupData);
		var query =
				'insert into ' + TABLE_WORDS_GROUPS_NAME + '(' +
				TABLE_WORDS_GROUPS.C_ID + ', ' +
				TABLE_WORDS_GROUPS.C_NAME + ', ' +
				TABLE_WORDS_GROUPS.C_AUTHOR + ', ' +
				TABLE_WORDS_GROUPS.C_LENGTH + ', ' +
				TABLE_WORDS_GROUPS.C_PROGRESS + ', ' +
				TABLE_WORDS_GROUPS.C_CREATED + ', ' +
				TABLE_WORDS_GROUPS.C_MARK_COUNTER + ', ' +
				TABLE_WORDS_GROUPS.C_MARK_VALUE + ', ' +
				TABLE_WORDS_GROUPS.C_DESCRIPTION + ', ' +
				TABLE_WORDS_GROUPS.C_CATEGORY + ', ' +
				TABLE_WORDS_GROUPS.C_ORIGIN_LANG + ', ' +
				TABLE_WORDS_GROUPS.C_TRANSLATION_LANG + ', ' +
				TABLE_WORDS_GROUPS.C_LAST_TIME_LEARN + ', ' +
				TABLE_WORDS_GROUPS.C_MARKED + ', ' +
				TABLE_WORDS_GROUPS.C_LEVEL + ', ' +
				TABLE_WORDS_GROUPS.C_DOWNLOAD_DATE + ', ' +
				TABLE_WORDS_GROUPS.C_FF_RECOMMEND + ', ' +
				TABLE_WORDS_GROUPS.C_DOWNLOADED + ')' + ' values (' +
				groupData.id + ', ' +
				'"' + groupData.name + '", ' +
				'"' + groupData.author + '", ' +
				groupData.length + ', ' +
				'0, ' +
				'"' + groupData.created + '", ' +
				groupData.markCounter + ', ' +
				groupData.markValue + ', ' +
				'"' + groupData.description + '", ' +
				'"' + groupData.category + '", ' +
				'"' + groupData.origin_lang + '", ' +
				'"' + groupData.translation_lang + '", ' +
				'"0", ' +
				'0, ' +
				parseInt(groupData.level, 10) + ', ' +
				dateTime.format(Math.round(new Date().getTime() / 1000), 'YYYY:MM:DD', '-') + ', ' +
                groupData.ff_recommend + ', ' +
				groupData.downloaded + ');';

		runQuery(query);
	}

	function addStartedGroups(groupsData, callback) {
		logger.log('addStartedGroups()', groupsData);
		var i,
			length = groupsData.length,
            performData = {
                addedGroups: length,
                addedExpressions: 0
            };

		for (i = 0; i < length; i++) {
            performData.addedExpressions += groupsData[i].words.length;
		    addWordsGroup(groupsData[i]);
			createWordsGroupTable(groupsData[i].id);
			addGroupExpressions(groupsData[i]);
		}
		callback(null, performData);
	}

	function addExpression(groupId, expression) {
		logger.log('addExpression()', groupId, expression);
		var query =
			'insert or replace into ' +
			TABLE_WORDS_GROUP_NAME + groupId +
			'(' +
			TABLE_WORDS_GROUP.C_ID + ', ' +
			TABLE_WORDS_GROUP.C_ORIGIN + ', ' +
			TABLE_WORDS_GROUP.C_TRANSLATED + ', ' +
			TABLE_WORDS_GROUP.C_USAGE_EXAMPLE+ ', ' +
			TABLE_WORDS_GROUP.C_MARK + ') ' +
			'values (' +
			expression.id + ', ' +
			'"' + expression.origin + '", ' +
			'"' + expression.translated + '", ' +
			'"' + expression.usage_example + '", ' +
			'0)';

			runQuery(query);
	}

	function updateWordsGroup(groupData) {
		logger.log('updateWordsGroup()', groupData);
		var query =
			'update ' + TABLE_WORDS_GROUPS_NAME + ' set ' +
				TABLE_WORDS_GROUPS.C_NAME + ' = "' + groupData.name + '", ' +
				TABLE_WORDS_GROUPS.C_LENGTH + ' = ' + groupData.length + ', ' +
				TABLE_WORDS_GROUPS.C_MARK_COUNTER + ' = ' + groupData.markCounter + ', ' +
				TABLE_WORDS_GROUPS.C_MARK_VALUE + ' = ' + groupData.markValue + ', ' +
				TABLE_WORDS_GROUPS.C_DESCRIPTION + ' = "' + groupData.description + '", ' +
				TABLE_WORDS_GROUPS.C_CATEGORY + ' = "' + groupData.category + '", ' +
				TABLE_WORDS_GROUPS.C_DOWNLOADED + ' =' + groupData.downloaded + ' ' +
				' where id=' + groupData.id + ';';

		runQuery(query);
	}

	function recalculateGroupProgress(groupData) {
		logger.log('recalculateGroupProgress()', groupData);
		var query =
			'select * from ' +
			TABLE_WORDS_GROUP_NAME + groupData.id +
			' where ' +
			TABLE_WORDS_GROUP.C_MARK +
			' = 2';

		runQuery(query, function (result) {
			var progress = Math.floor((result.rows.length * 100) / groupData.length);
			updateGroupProgress(groupData.id, progress);
		});

	}

	function updateOrInsertGroupExpressions(groupData) {
		logger.log('updateOrInsertGroupExpressions()', groupData);
		for (var i = 0; i < groupData.words.length; i++) {
		    addExpression(groupData.id, groupData.words[i]);
		}

		// todo
		console.error('recalculate progress !!!');
		recalculateGroupProgress(groupData);
	}

	function addGroupExpressions(groupData) {
		logger.log('addGroupExpressions()', groupData);
		var length = groupData.words.length;
		for (var i = 0; i < length; i++) {
		    addExpression(groupData.id, groupData.words[i]);
		}
	}

    function storeReportEvent(groupId) {
        logger.log('storeReportEvent()', groupId);
        var query = 'insert into ' +
                    TABLE_REPORTS_NAME + '(' +
                    TABLE_REPORTS.C_GROUP_ID + ', ' +
                    TABLE_REPORTS.C_LAST_REPORTED + ') values (' +
                    groupId + ', ' +
                    Math.round(new Date().getTime() / 1000) + ')';
        runQuery(query);
    }

	function syncChangedGroups(groupsData, callback) {
		logger.log('syncChangedGroups()', groupsData);
		var changedCount = 0;
		for (var i = 0; i < groupsData.length; i++) {
		    updateWordsGroup(groupsData[i]);
			updateOrInsertGroupExpressions(groupsData[i]);
            changedCount += groupsData[i].words.length
		}
		callback(null, changedCount);
	}


	function performSynchronization(syncData, options) {
		logger.log('performSynchronization()', syncData);
		var parsedData;
		try {
			parsedData = JSON.parse(syncData);
		} catch (e) {
			logger.error('Cannot parse data');
		}

		async.series([
			function (callback) {
				addStartedGroups(parsedData.started, callback);
			},
			function (callback) {
				syncChangedGroups(parsedData.changed, callback);
			},
			function (callback) {
				syncRemovedGroups(parsedData.learningIds, callback);
			}
		], function (error, changedArray) {
			logger.log('performSynchronization()', 'COMPLETED!!!', arguments);
			var syncResult = {
                addedGroups: changedArray[0].addedGroups,
                addedChangedExpressions: changedArray[0].addedExpressions + changedArray[1],
                removedGroups: changedArray[2]
            };
			options.success(syncResult);
		});
	}

	function deleteAllWordsGroups(callback) {
		getLearningGroupsIds(function (ids) {
			for (var i = 0; i < ids.length; i++) {
				if (i === ids.length - 1) {
					deleteWordsGroup(ids[i], callback);
				} else {
					deleteWordsGroup(ids[i]);
				}
			}
		})
	}

	function deleteWordsGroupsTable() {
		logger.log('deleteWordsGroupsTable()');
		var query = 'drop table ' + TABLE_WORDS_GROUPS_NAME;
		runQuery(query);
	}

    function deleteReportsTable() {
        logger.log('deleteReportsTable()');
        var query = 'drop table ' + TABLE_REPORTS_NAME;
        runQuery(query);
    }

	function deleteStatsTable() {
		logger.log('deleteStatsTable()');
		var query = 'drop table ' + TABLE_LEARNED_EXPRESSIONS_NAME;
		runQuery(query);
	}

	function deleteAllTables() {
		logger.log('deleteAllTables()');
		deleteAllWordsGroups();
		deleteStatsTable();
		deleteWordsGroupsTable();
        deleteReportsTable();
	}

	function clearAllUserData() {
		logger.log('clearAllUserData()');
		deleteAllTables();
	}

	var dao = {
		createInitialTables: createInitialTables,
		performSynchronization: performSynchronization,
		getWordsGroups: getWordsGroups,
		getGroupExpressionsForLearning: getGroupExpressionsForLearning,
		saveWordMark: saveWordMark,
		updateGroupProgress: updateGroupProgress,
		resetLearningResults: resetLearningResults,
		getGroupExpressions: getGroupExpressions,
		getWordsGroup: getWordsGroup,
		notifyLearningStart: notifyLearningStart,
		setWordsGroupMarked: setWordsGroupMarked,
		getNumberOfWordsGroups: getNumberOfWordsGroups,
		getLearnedExpressionsNumber: getLearnedExpressionsNumber,
		getTotalNumberOfExpressions: getTotalNumberOfExpressions,
		deleteWordsGroup: deleteWordsGroup,
        storeReportEvent: storeReportEvent,
        getLastTimeReport: getLastTimeReport,



		// todo remove these few rows below
		getLearningGroupsIds: getLearningGroupsIds,
		deleteAllWordsGroups: deleteAllWordsGroups,
		removeWordsGroupsFromMainTable: removeWordsGroupsFromMainTable,
		removeGroupExpressionsTable: removeGroupExpressionsTable,
		clearAllUserData: clearAllUserData,
		getStatsData: getStatsData,
		runQuery: runQuery
	};

	window.dao = dao;
	return dao;
});