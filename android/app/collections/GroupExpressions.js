define([
	'backbone',
	'app/models/Expression',
	'logger',
	'app/dao'
], function (
	Backbone,
	Expression,
	Logger,
	dao
){
	var logger = Logger.getOne('Collection:GroupExpression');

	return Backbone.Collection.extend({

		model: Expression,

		initialize: function () {
			logger.log('initialize');
			return this;
		},

		sync: function (method, collection, options) {
			logger.log('sync', method);
			switch (method) {
				case 'read':
					if (options.type === 'all') {
						dao.getGroupExpressions(this.id, options.success);
					} else if (options.type === 'learning') {
						dao.getGroupExpressionsForLearning(this.id, options.success);
					}
					break;
				default:
					break;
			}
		},

		setId: function (id) {
			this.id = id;
			return this;
		}
	});
});