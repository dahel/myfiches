define([
	'backbone',
	'app/models/WordsGroup',
	'logger',
	'app/dao'
], function (
	Backbone,
	WordsGroup,
	Logger,
	dao
){
	var logger = Logger.getOne('Collection:WordsGroups');

	var WordsGroups = Backbone.Collection.extend({

		model: WordsGroup,

		fetched: false,

		itemsPerPage: 0,

		count: 0,

		page: 0,

		initialize: function () {

			//todo remove it
			window.WordsGroups = this;
			logger.log('initialize');
		},


		// options = {
		// 		itemsPerPage: 10,
		//		page: 2
		// }
		sync: function (method, collection, options) {
			logger.log('sync', method);
			switch (method) {
				case 'read':
					dao.getWordsGroups(options);
					this.fetched = true;
					break;
				default:
					break;
			}
		},

		parse: function (response) {
			this.itemsPerPage = response.itemsPerPage;
			this.page = response.page;
			this.totalLength = response.count;
			return response.collection;
		}
	});

	return new WordsGroups();
});