define([
	'backbone',
	'app/models/Expression',
	'logger',
	'config'
], function (
	Backbone,
	Expression,
	Logger,
	config
){
	var logger = Logger.getOne('Collection:GroupExpression');

	return Backbone.Collection.extend({

		model: Expression,

		url: function () {
			return config.url.getExpressionsUrl(this.id)
		},

		initialize: function () {
			logger.log('initialize');
			return this;
		},

		parse: function(response) {
			return response.body.collection;
		},

		setId: function (id) {
			this.id = id;
			return this;
		}
	});
});