define([
	'backbone',
	'logger',
	'app/models/SearchWordsGroup',
	'config'
], function (
	Backbone,
	Logger,
	SearchWordsGroup,
	config
){
	var logger = Logger.getOne('Collection:SearchWordsGroups');

	return Backbone.Collection.extend({

		url: config.url.WORDS_GROUPS,

		model: SearchWordsGroup,

		itemsPerPage: 0,

		count: 0,

		page: 0,

		initialize: function () {

			//todo remove it
			window.search = this;
			logger.log('initialize');
		},

		parse: function (response) {
			this.itemsPerPage = response.body.itemsPerPage;
			this.page = response.body.page;
			this.totalLength = response.body.count;
			return response.body.collection;
		}
	});
});