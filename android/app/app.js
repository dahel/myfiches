define([
	'backbone',
	'router',
	'logger',
	'fastclick',
	'lang',
	'app/views/components/infoPopup',
	'app/views/components/actionBar',
	'app/views/components/loading',
	'jquery',
	'async',
	'app/models/Settings',
	'libs/polyfills'
],
function (
	Backbone,
	Router,
	Logger,
	fastclick,
	lang,
	InfoPopup,
	ActionBar,
	Loading,
	$,
	async,
	Settings
) {

	var logger = Logger.getOne('app');

	return _.extend({}, Backbone.Events, {
		actionBar: null,
		router: null,
		requestManager: this,

		launching: true,

		platform: 'android',

		lowPerformanceDevice: false,

		androidVersion: 0,

		launched: function (callback) {
			logger.log('launched', this.launching);
			if (this.launching) {
				setTimeout(function () {
                    document.body.classList.remove('loading-splash-screen');
					document.body.querySelector('#content').style.display = 'block';
					try {
						document.body.removeChild(document.body.querySelector('.splash-screen'));
					} catch (e){}

					if (callback) {
					    callback();
					}
				}, 200);
				this.launching = false;
			}
		},

		ready: function () {
			logger.log('ready()');
		},

		initialize: function (platform) {
			window.app = this;
			logger.log('initialize()');
			this.callSyncStuff(platform);
			this.callAsyncStuff();
		},

		callSyncStuff: function (platform) {
			this.platform = platform;
			Backbone.View.prototype._super = function (name) {
				this.constructor.__super__[name].apply(this, arguments);
			};
			this.attachFastclick();
			this.handleKeyBack();

			if (platform === 'android') {
			    this.readAndroidVersion();
				// for android only portrait
				screen.lockOrientation("portrait-primary");
			} else {
				this.androidVersion = 1000;
				//this.applyLowPerformanceClass();
			}
		},

		applyLowPerformanceClass: function () {
			logger.log('applyLowPerformanceClass()');
			$(document.body).addClass('low-performance-device');
			this.lowPerformanceDevice = true;
		},

		callAsyncStuff: function () {
			async.series([
				this.initRequestManager.bind(this),
				this.readUserData.bind(this)
			], this.start.bind(this));

		},

		start: function () {
			if (!parseInt(Settings.get('adverts'))) {
				logger.warn('TODO - need to disable items. for now just adding no-adverts class to body element');
				$('body').addClass('no-adverts');
			}
			this.router = new Router();

			if (this.platform === 'browser') {
				this.router.on('page:hide', this.requestManager.abortAll, this.requestManager);

			}

			this.actionBar = new ActionBar(this);
			this.router.initializePages(function () {
				Backbone.history.start();
			});
		},

		initRequestManager: function (callback) {
			var self = this;
			require(['app/RequestManager'], function (RequestManager) {
				RequestManager.initialize(this);
				RequestManager.setServerErrorHandler(function (serverErrorCode) {
					logger.error('ServerErrorHandler()', lang.Server[serverErrorCode]);
					InfoPopup.show({
						templateData: lang.Server[serverErrorCode],
						type: InfoPopup.type.ERROR
					});
				});
				RequestManager.setRequestErrorHandler(function () {
					logger.error('RequestErrorHandler()');
					InfoPopup.show({
						templateData: lang.REQUEST_ERROR,
						type: InfoPopup.type.ERROR
					});
				});
				RequestManager.on('error', this.handleRequestError, this);
				RequestManager.on('unauthorized', function () {
					self.router.navigate('login', {trigger: true});
				}, this);

				self.requestManager = RequestManager;
				callback(null, null);
			});
		},

		readUserData: function (callback) {
			require(['app/models/User'], function (UserModel) {
				UserModel.read({
					success: function () {
						callback(null, null);
					}
				})
			})
		},

		handleRequestError: function (errorCode) {
			logger.warn('handleRequestError(errorCode)', errorCode, lang.Server[errorCode]);
		},

		readAndroidVersion: function () {
			this.androidVersion = parseInt(device.version.split('.').join(''));

			if (parseInt(('' + this.androidVersion)[0], 10) < 3) {
				logger.log('--------------- LOW PERFORMANCE DEVICE ---------------');
				logger.log('--------------- ANDROID VERSION: ' + this.androidVersion + ' ---------------');
				logger.log('--------------- LOW PERFORMANCE DEVICE ---------------');
				this.applyLowPerformanceClass();

			}

			logger.log('readAndroidVersion()', this.androidVersion);
		},

		attachFastclick: function () {
			fastclick.attach(document.body);
		},

		handleKeyBack: function () {
			var self = this;
			document.addEventListener("backbutton", function(e){
				logger.log('backbutton');
				self.onKeyBack();
			}, false);

			if (this.platform === 'browser') {
				document.addEventListener("keydown", function(e){
					if (e.keyCode === 8) {
						e.preventDefault();
						logger.log('backbutton');
						self.onKeyBack();
					}
				}, false);
			}
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			if (this.actionBar.isMenuVisible) {
				this.actionBar.hideMenu();
			} else if (InfoPopup.isVisible()) {
				InfoPopup.hide();
			} else {
				this.requestManager.abortAll();
				Loading.hide();
				this.router.onKeyBack();
			}
		},

		clearAllUserData: function () {
			logger.log('clearAllUserData()');
			dao.clearAllUserData();
			this.trigger('clearUserData');

			if (this.platform === 'android') {
			    logger.log('CLEARING SHARING PREFERENCES...');
				sharedpreferences.clear();
			}
		},

		disableScrolling: function () {
			logger.log('disableScrolling()');
			$(document.body).addClass('scroll-disabled');
		},

		enableScrolling: function () {
			logger.log('enableScrolling()');
			$(document.body).removeClass('scroll-disabled');
		}
	})
});