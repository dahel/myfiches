define([
	'backbone',
	'underscore',
	'app/models/User',
	'logger',
	'platform',
	'lang',
	'app/views/components/loading'
],
function (
	Backbone,
	_,
	User,
	logger,
	platform,
	lang,
	Loading
) {

	var logger = logger.getOne('RequestManager'),
		serverErrorHandler;


	return _.extend({}, Backbone.Events, {

		loading: null,

		app: null,

		initialize: function (app) {
			this.app = app;
			this.applyCustomAjax();
		},

		setServerErrorHandler: function (handler) {
			serverErrorHandler = handler;
		},

		setRequestErrorHandler: function (handler) {
			requestErrorHandler = handler;
		},

		token: 0,

		requests: {},

		getToken: function () {
			return ++this.token;
		},

		addRequest: function (token, xhr, onAbort) {
			this.requests[token] = {
				request: xhr,
				onAbort: onAbort
			};
		},

		removeRequest: function (token) {
			if (this.requests.hasOwnProperty(token)) {
			    delete this.requests[token];
			}
		},

		abortAll: function () {
			logger.log('abortAll()');
			for(var request in this.requests) {
				if (this.requests.hasOwnProperty(request)) {
					if (this.requests[request].onAbort) {
						this.requests[request].onAbort();
					}
					this.requests[request].request.abort();
					delete this.requests[request];
				}
			}
		},

		createHeaders: function () {
			return {
				'Access-Token': User.get('token'),
				'User-Name': User.get('username'),
				'Android-Client': 'true'
			}
		},

		applyCustomAjax: function () {
			var self = this;

			function getMessageCode(response) {
				var errorCode = '001';
				var responseText = response.responseText;

				try {
				if (responseText) {
					responseText = JSON.parse(responseText);
				}
					errorCode = responseText.body.code;
				} catch (e) {}

				return errorCode
			}

			Backbone.ajax = function () {
				logger.log('Backbone.ajax()', arguments[0].url);

				//platform.showSpinner(null, lang.CONNECTING_TO_INTERNET + '...');
				var xhr,
					params = arguments[0],
					token = self.getToken(),
					success = params.success,
					error = params.error || function () {};

				params.complete = function () {
					self.removeRequest(token);
				};

				params.success = function (response) {
					var args = [].slice.call(arguments, 0);
//					platform.hideSpinner();
					//Loading.hide();
					success.apply(null, args);
				};

				params.error = function (response) {
//					platform.hideSpinner();
					Loading.hide();
					// unauthorized usage, need to login
					if (response.status === 401) {
						logger.warn('Unauthorized usage, need to login');
						self.trigger('unauthorized');
						platform.notify(lang.NEED_TO_LOGIN);
					} else if (response.status !== 200 && response.status !== 0) {
						var args = [].slice.call(arguments, 0);
						error.apply(null, arguments);
						serverErrorHandler(getMessageCode(response));
					} else if (response.status === 0 && response.statusText !== 'abort') {
						requestErrorHandler();
					}
				};

				params.headers = self.createHeaders();

				xhr = $.ajax(params);
				self.addRequest(token, xhr, params.onAbort);
				return xhr;
			};
		}
	});
});