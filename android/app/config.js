define([], function () {

		//var baseUrl = "http://192.168.0.13/fast_fiche/rest.php/";
//		var baseUrl = "http://192.168.0.13/myfiches/fast_fiche/rest.php/";
//		var baseUrl = "http://www.htmlcenter.pl/fast_fiche/rest.php/";
		var baseUrl = "http://www.myfiches.com/fast_fiche/rest.php/";

	return {
		SHARED_PREFERENCES_FILENAME: 'liq_shared_preferences_file',

		url: {
			WORDS_GROUP: baseUrl + 'wordsGroup',
			WORDS_GROUPS: baseUrl + "wordsGroups",
			CATEGORIES: baseUrl + "categories",
			WORDS_GROUPS_LEARNING: baseUrl + "learningWordsGroups",
			WORDS_GROUPS_COMMON: baseUrl + "common",
			WORDS_GROUPS_USER: baseUrl + "userWordsGroups",
			SYNCHRONIZE: baseUrl + "synchronize",

			SETTINGS: baseUrl + 'settings',
			LOGIN: baseUrl + 'login',
			REGISTER: baseUrl + 'register',
			LOGOUT: baseUrl + 'logout',

			REPORT: baseUrl + 'report',
			LEARN_POINTS: baseUrl + 'learnPoints',

			getExpressionsUrl: function (id) {
				return this.WORDS_GROUPS + '/' + id + '/words'
			}
		},

        LEARN_POINTS_COST_PER_SET: 100
	}
});