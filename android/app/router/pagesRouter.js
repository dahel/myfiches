define([
	'backbone',
	'logger'
],
function (
	Backbone,
	Logger
) {

	var logger = Logger.getOne('PagesRouter');

	var pagesNames = ['search'];

	var pages = {

		},

		currentPageName = null;

	function switchPage(name, args, router) {
		logger.log('switchPage()');
		if (!pages.hasOwnProperty(name)) {
		    logger.error('There is no such page:', name);
		} else {
			$(document.body).addClass(name);

			if (currentPageName) {
			    pages[currentPageName].hide();
				$(document.body).removeClass(currentPageName);
                router.trigger('page:hide')
			}

			pages[name].show(args);
			currentPageName = name;
		}
	}

	function showPage(name, args, router) {
		logger.log('showPage()');
		if (!pages.hasOwnProperty(name)) {
		    require(['app/views/pages/' + name], function (Page) {
				pages[name] = new Page();
				pages[name].el.style.display = 'none';
				document.body.querySelector('#pages-wrapper').appendChild(pages[name].el);

				switchPage(name, args, router);
			})
		} else {
			switchPage(name, args, router);
		}
	}

	return Backbone.Router.extend({
		show: function (name) {
			logger.log('show', name);
			showPage(name, Array.prototype.slice.call(arguments, 1), this)
		},

		onKeyBack: function () {
			logger.log('onKeyBack()');
			pages[currentPageName].onKeyBack();
		},

		initializePages: function (callback) {
			logger.log('initializePages()');
			var current = 0,
				loadNext = function () {
					if (pagesNames[current]) {
						logger.log('LOADING PAGE: ', pagesNames[current]);
						require(['app/views/pages/' + pagesNames[current]], function (Page) {
							pages[pagesNames[current]] = new Page();
							pages[pagesNames[current]].el.style.display = 'none';
							document.body.querySelector('#pages-wrapper').appendChild(pages[pagesNames[current]].el);
							current++;
							loadNext();
						});

					} else {
						callback();
					}
				};
			loadNext();
		}
	});
});