define([
    'backbone',
	'app/router/pagesRouter'
    ],
function (
    Backbone,
	PagesRouter
) {
	return PagesRouter.extend({
        routes: {
            'main': 'showMainPage',
            'login': 'showLoginPage',
			'group/:id': 'showGroupPage',
			'learn/:id': 'showGroupLearn',
			'browse/:id': 'showGroupBrowse',
			'mark/:id': 'showGroupMark',
			'report/:id': 'showGroupReport',
			'stats': 'showStats',
			'settings': 'showSettings',
			'search': 'showSearch',
			'search/:reset': 'showSearch',
			'browseSearch/:id/:name': 'showBrowseSearch',
			'groupSearch/:id': 'showGroupSearch',
			'learnPoints': 'showLearnPoints',
			'': 'goToMainPage'
        },

        initialize: function() {
        },

		showMainPage: function () {
			this.show('main');
		},

		showGroupPage: function (id) {
			this.show('group', id);
		},

		showLoginPage: function () {
			this.show('login')
		},

		showGroupLearn: function (id) {
			this.show('learn', id);
		},

		showGroupBrowse: function (id) {
			this.show('browse', id);
		},

		showGroupMark: function (id) {
			this.show('mark', id);
		},

		showGroupReport: function (id) {
			this.show('report', id);
		},

		showStats: function () {
			this.show('stats');
		},

		showSettings: function () {
			this.show('settings');
		},

		showSearch: function (reset) {
			this.show('search', reset);
		},

		showBrowseSearch: function (id, name) {
			this.show('browseSearch', id, name);
		},

		showGroupSearch: function (id) {
			this.show('groupSearch', id);
		},

		showLearnPoints: function () {
			this.show('learnPoints');
		},

		goToMainPage: function () {
			this.navigate('main', {trigger: true});
		}
    });
});