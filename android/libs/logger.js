define([], function () {

	var maxPrefixLength = 45;

	var last = 0,
		passed = 0;

	function addPadding(text) {
		var prefix = new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1") + ' [' + text + '] ';

		while (prefix.length < maxPrefixLength) {
			prefix += '-';
		}

		return prefix + '> ';
	}

	function log() {

//		var args = Array.prototype.slice.call(arguments);
//		args.splice(1, 0, new Date().getTime() - last);
//		args.unshift(addPadding(args.shift()));
////		debugger;
//		console.log.apply(console, args);
//		last = new Date().getTime();
	}

	function error () {
		//var args = Array.prototype.slice.call(arguments);
		//args.unshift(addPadding(args.shift()));
		//console.error.apply(console, args);
	}

	function warning () {
		//var args = Array.prototype.slice.call(arguments);
		//args.unshift(addPadding(args.shift()));
		//console.warn.apply(console, args);
	}

	function Logger(name) {
		this.name = name;
		this.log = function () {
			var args = Array.prototype.slice.call(arguments);
			args.unshift(this.name);
			log.apply(null, args)
		};
		this.error = function () {
			var args = Array.prototype.slice.call(arguments);
			args.unshift(this.name);
			error.apply(null, args)
		};
		this.warn = function () {
			var args = Array.prototype.slice.call(arguments);
			args.unshift(this.name);
			warning.apply(null, args)
		}
	}

	return {
		getOne: function (name) {
			return new Logger(name);
		}
	}
});