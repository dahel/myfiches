(function (global) {
	if (!Function.prototype.bind) {
		Function.prototype.bind = function(oThis) {
			if (typeof this !== 'function') {
				// closest thing possible to the ECMAScript 5
				// internal IsCallable function
				throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
			}

			var aArgs   = Array.prototype.slice.call(arguments, 1),
				fToBind = this,
				fNOP    = function() {},
				fBound  = function() {
					return fToBind.apply(this instanceof fNOP && oThis
						? this
						: oThis,
						aArgs.concat(Array.prototype.slice.call(arguments)));
				};

			fNOP.prototype = this.prototype;
			fBound.prototype = new fNOP();

			return fBound;
		};
	}

	var $target;

	function onUserInteractionStart(event) {
//	console.log('################### start ###################');
		$target = $(event.target);

		if ($target.attr('notap')) {
		    return;
		}
//		console.log('################### target ###################', $target);
		if (!$target.hasClass('tapped')) {
			$target.addClass('tapped');
			if (event.target.getAttribute('tapped-delay')) {
				setTimeout(function () {
					$(event.target).removeClass('tapped');
				}, parseInt(event.target.getAttribute('tapped-delay')))
			}
		}
	}

	function userInteractionEnd(event) {
		if ($target) {
			$target.removeClass('tapped');
		}
//		if (!event.target.getAttribute('tapped-delay')) {
//			$(event.target).removeClass('tapped');
//		}
	}

	if (!navigator.userAgent.match(/Android/)) {
		global.onmousedown = onUserInteractionStart;

		global.onmouseup = userInteractionEnd;
	}

	global.ontouchstart = onUserInteractionStart;

	global.ontouchend = userInteractionEnd;

	global.ontouchcancel = userInteractionEnd

}(this));